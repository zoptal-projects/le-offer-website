import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { TermsService } from '../terms/terms.service';
import { Configuration } from '../app.constants';
import { HomeService } from '../home/home.service';
import { Meta } from '@angular/platform-browser';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';
declare var $: any;
@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _serviceHome: HomeService,
    private _meta: Meta,
    private _conf: Configuration,
    private _router: Router,
    private _service: TermsService,
    private route: ActivatedRoute,
    private location: Location,
    private _lang: LanguageService) { }

  headerOpen: string;
  token: any;
  type = 5;
  about: any;
  offset = 0;
  limit = 40;
  newsDetails: any;
  data: any;
  aboutUs: any;
  languageCode: any;

  ngOnInit() {

    // this.aboutUs = this._lang.engAbout;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.aboutUs = this._lang.engAbout1;
                this.languageCode = "fr";
                break;
        case 2: this.aboutUs = this._lang.engAbout;
                this.languageCode = "en";
                break;
        case 3: this.aboutUs = this._lang.engAbout2;
                this.languageCode = "ar";
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.aboutUs = this._lang.engAbout;
      this.languageCode = "en";
    }
    // this._router.navigate([this.languageCode,'about']);
    this.location.replaceState(`${this.languageCode}/about`);
    this.seo();
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    this.aboutList();
    this.newsList();
  }

  seo() {
    let list = {
      type: 4
    }
    this._serviceHome.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.data = res.data;
          if (this.data) {
            this._meta.addTag({ name: 'title', content: this.data.title })
            this._meta.addTag({ name: 'description', content: this.data.description })
            this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
          }
        }
      });
  }

  aboutList() {
    this._service.webContent(this.languageCode, this.type)
      .subscribe((res) => {
        if (res.code == 200) {
          this.about = res.data[0].configData;
        }
      });
  }

  newsList() {
    this._service.newsContent(this.languageCode, this.offset, this.limit)
      .subscribe((res) => {
        if (res.code == 200) {
          this.newsDetails = res.data;
        }
      });
  }
  goToHowWorks(){
    this._router.navigate([`${this.languageCode}/howitworks`])
  }

}
