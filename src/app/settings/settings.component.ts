import { Component, OnInit, NgZone, EventEmitter } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { RouterLinkActive } from '@angular/router';
import { MissionService } from '../app.service';
import { SettingsService } from './settings.service';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";
import { HomeService } from '../home/home.service';
import { Cloudinary } from '@cloudinary/angular-5.x';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import{ Location } from '@angular/common';
import * as  cloudinary1 from 'cloudinary';
import { Ng2ImgMaxService } from 'ng2-img-max';

declare var $: any;
declare var google: any;
declare var FB: any;
declare var moment: any;

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  cloudinaryData:any;
  userLocation :string;
  headerOpen: string;
  headerRefresh: string;
  token: any;
  userName: any;
  email: any;
  profilePicUrl: any;
  phone_Error = false;
  email_Error = false;
  registerSave = false;
  editData: any;
  errorMsg: any;
  languageCode:any;
  loaderButton = true;
  cloudinaryImage: any;
  follow_count = 0;

  constructor(
    private _missionService: MissionService,
    private _conf: Configuration,
    private _router: Router,
    private _zone: NgZone,
    private location:Location,
    private _service: SettingsService,
    public _auth: AuthService,
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private Cloudinary: Cloudinary,
    private imgCompressService: Ng2ImgMaxService,
    private homeService : HomeService) {

    _missionService.missionheaderRefresh$.subscribe(
      headerRefresh => {
        this.ngOnInit();
      });
      this.homeService.getCloudinaryData(this._conf.getItem('authToken')).subscribe((res)=>{
        ////console.log("the home cloudinary data", res);
        this.cloudinaryData = res.response;
      });
      cloudinary1.config({
        cloud_name: this._conf.cloudinaryData.cloud_name,
        api_key:this._conf.cloudinaryData.api_key,
        api_secret:this._conf.cloudinaryData.api_secret,
        upload_preset:this._conf.cloudinaryData.upload_preset
      });
  }
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    // cloudName: this.cloudinaryData.cloudName,
    uploadPreset: 'iirvrtog',
    autoUpload: true
  });

  // uploader: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);
  uploader: FileUploader;



  sellingOffer: any;
  offset = 0;
  limit = 40;
  internetModal = false;


  buyingOffer: any;
  loginUsername: any;
  watchOffer: any;


  offerListLength = false;
  allListProfile = false;
  following: any;
  followingErr: any;
  followerLength: any;
  followingLength: any;
  postLength: any;
  tileFollowers: any;
  loaderButtonPopup = false;
  loaderButtonProfile = false;
  settings: any;
  emptyStar = [];
  noPostContent = 0;
  tabName:string = '';

  sub: any;
  accessToken: any;
  payPal = "https://www.paypal.me/";
  imageUploader = (item: any, response: string, status: number, headers: any): any => {
    let cloudinaryImage = JSON.parse(response);
    console.log("cloudinaryImage",cloudinaryImage)
    let list = {
      img: cloudinaryImage.secure_url,
      cloudinaryPublicId: cloudinaryImage.public_id,
      containerHeight: cloudinaryImage.height,
      containerWidth: cloudinaryImage.width
    }
    $(".yeloProfileImage").attr("src",  cloudinaryImage.secure_url);
    $(".imgCloudinary").hide();
    this.editProfile.profilePicUrl = cloudinaryImage.secure_url;
    this.uploader.uploadAll();
  };

  ngOnInit() {

    // this.settings = this._lang.engSettings;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.settings = this._lang.engSettings1;
                this.languageCode = 'fr';
                break;
        case 2: this.settings = this._lang.engSettings;
                this.languageCode = 'en';
                break;
        case 3: this.settings = this._lang.engSettings2;
                this.languageCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.languageCode = 'en';
      this.settings = this._lang.engSettings;
    }
    $(window).on('popstate', function () {
      $('.modal').modal('hide');
    });

    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    if (!this.token) {
      // this._router.navigate([this.languageCode]);
      this._router.navigate(['']);             
    }

    this.userName = this._conf.getItem('username');
    this.email = this._conf.getItem('email');
    this.profilePicUrl = this._conf.getItem('profilePicUrl');

    setTimeout(() => {
      this.allListProfile = true;
    }, 500);
    this.allListProcess(0);
    this.getFollowing();
    setTimeout(()=>{
      this.myeditProfile();
    },1000)
    this.getFollowers();
    this.emptyFields();
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${
        this.Cloudinary.config().cloud_name
        }/auto/upload`,
    autoUpload: true,
    isHTML5: true,
    removeAfterUpload: true,
    headers: [{ name: 'X-Requested-With', value: 'XMLHttpRequest' },],
      // }
    };
    this.uploader = new FileUploader(uploaderOptions);
    // this.uploader.onAfterAddingFile = (fileItem:any) => {
    //   fileItem.withCredentials = false;
    //   fileItem.upload_preset = this.Cloudinary.config().upload_preset;
    //   if(fileItem.file.rawFile.type === "image/png" ||fileItem.file.rawFile.type === "image/jpg" ){
    //       this.imgCompressService.resizeImage(fileItem.file.rawFile, 600, 800).subscribe(
    //       result => {
    //       const newImage = new File([result], result.name);
    //       this.uploader.clearQueue();
    //       this.uploader.addToQueue([newImage]);
    //       // this.uploader.uploadAll();
    //     },
    //     error => //console.log("error image",error)
    //   );
    //  }
    //   ////console.log(fileItem); // fileItem is the file object
    // };
    // this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
    //   form.append('upload_preset', this.Cloudinary.config().upload_preset);
    // };
    // this._router.navigate([this.languageCode,'settings'])
    this.location.replaceState(`${this.languageCode}/settings`);
  }
  
  public emptyStars = [
    { star: false },
    { star: false },
    { star: false },
    { star: false },
    { star: false }
  ]
  EditProfile(){
    ////console.log("the edit profile:", this.editProfile)
    var input = document.getElementById('location');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      let lati = place.geometry.location.lat();
      let lngi = place.geometry.location.lng();
      var place = $(".place").val(place.formatted_address);
      var lat = $(".lat").val(lati);
      var lng = $(".lng").val(lngi);
    });

    $("#mobileSignin").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry: "ma",
      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
}
  emptyFields() {
    this.emptyStar = [];
    for (var i = 0; i < this.emptyStars.length; i++) {
      this.emptyStar.push(this.emptyStars[i]);
    }
  }

  starActive(val: number) {
    for (var i = 0; i < val; i++) {
      this.emptyStar[i].star = true;
    }
  }
  onScroll(){
    this.offset+= 10;
    this.limit += 10;
    this.allListProcess(this.noPostContent);
  }
  allListProcess(val) {
    switch(val){
      case 0: (this.languageCode == 'en')?
      this.tabName = 'Settings' :
      this.languageCode == "fr" ?  this.tabName = 'Réglages' : this.tabName = "الإعدادات";
              break;

      case 1: 
      // this.tabName = 'Sold';
      (this.languageCode == 'en')?
      this.tabName = 'Sold' : 
      this.languageCode == "fr" ? this.tabName = 'Vendu' : this.tabName = "تم البيع"
              break;

      case 2: 
      // this.tabName = 'Favourites';
      (this.languageCode == 'en')?
      this.tabName = 'Favourites' : 
      this.languageCode == "fr" ? this.tabName = 'Favoris' : this.tabName = "المفضلة"
              break;

      case 3: 
      // this.tabName = 'Exchange';
      (this.languageCode == 'en')?
      this.tabName = 'Exchange' : this.languageCode == "fr" ? this.tabName = 'Échange':
      this.tabName = "تبادل"
              break;
    }
    this.noPostContent = val;
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 3000);
    this._service.sellingPostList(list, val)
      .subscribe((res) => {
        if (res.code == 200) {
          this.loaderButton = false;
          this.sellingOffer = res.data;
          if (val == 3) {
            this.timeFunction();
          }
          if (res.data && res.data.length > 0) {
            this.offerListLength = true;
            this.postLength = res.data.length;
          } else {
            this.offerListLength = false;
            this.postLength = 0;
          }
        } else {
          this.offerListLength = false;
          this.postLength = 0;
          this.loaderButton = false;
        }
      });
  }


  timeFunction() {
    var len = this.sellingOffer;
    for (var i = 0; i < len.length; i++) {
      var date = moment(this.sellingOffer[i].acceptedOn).valueOf();
      var post = date;
      var poston = Number(post);
      var postdate = new Date(poston);
      var currentdate = new Date();
      var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
      var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);

      if (1 < diffDays) {
        this.sellingOffer[i].acceptedDate = diffDays + " d";
      } else if (1 <= diffHrs) {
        this.sellingOffer[i].acceptedDate = diffHrs + " h";
      } else {
        this.sellingOffer[i].acceptedDate = diffMins + " m";
      }
    }
  }

  buildTrusts() {
    $("#buildTrustPopup").modal("show");
  }

  getFollowers() {
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    // this.tileFollowers = "Follower";
    this.languageCode == 'en' ? this.tileFollowers = "Follower" : 
    this.languageCode == "fr" ? this.tileFollowers = "Disciple" : this.tileFollowers = "تابع"
    this.following = [];
    this.loaderButtonPopup = true;
    setTimeout(() => {
      this.loaderButtonPopup = false;
    }, 3000);
    this._service.followerPostList(list)
      .subscribe((res) => {
        this.loaderButtonPopup = false;
        if (res.code == 200) {
          this.followingErr = false;
          this.following = res.followers;
          if (res.followers && res.followers.length) {
            this.followerLength = res.followers.length;
          } else {
            this.followerLength = 0;
          }
        } else {
          this.followerLength = 0;
          this.followingErr = res.message;
        }
      });

  }


  getFollowing() {
    let list = {
      token: this.token,
      offset: this.offset,
      limit: this.limit
    }
    // this.tileFollowers = "Following";
    this.languageCode == 'en' ? this.tileFollowers = "Following" : 
    this.languageCode == "fr" ? this.tileFollowers = "Suivant" : this.tileFollowers = "التالية";
    this.following = [];
    this.loaderButtonPopup = true;
    setTimeout(() => {
      this.loaderButtonPopup = false;
    }, 3000);
    this._service.followingPostList(list)
      .subscribe((res) => {
        this.loaderButtonPopup = false;
        if (res.code == 200) {
          this.followingErr = false;
          this.following = res.result;
          if (res.result && res.result.length) {
            this.followingLength = res.result.length;
          } else {
            this.followingLength = 0;
          }
        } else {
          this.followingLength = 0;
          this.followingErr = res.message;
        }
      });
  }


  followerButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        userNameToFollow: val
      }
      this.following[i].loaderFlag = true;
      setTimeout(() => {
        this.following[i].loaderFlag = false;
      }, 3000);
      this.following[i].userFollowRequestStatus = 1;
      this._service.followersPostListButton(list)
        .subscribe((res) => {
          this.following[i].loaderFlag = false;

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  unFollowButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        unfollowUserName: val
      }
      this.following[i].loaderFlag = true;
      setTimeout(() => {
        this.following[i].loaderFlag = false;
      }, 3000);
      this.following[i].userFollowRequestStatus = null;
      this._service.unfollowPostListButton(list)
        .subscribe((res) => {
          this.following[i].loaderFlag = false;
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }



  editProfile = {
    profilePicUrl: '',
    fullName: '',
    username: '',
    bio: '',
    websiteUrl: '',
    email: '',
    phoneNumber: '',
    place: '',
    latitude: '',
    longitude: '',
    facebookVerified: '',
    googleVerified: '',
    paypalUrl:'',
    location:''
  }

  myeditProfile() {
    let list = {
      token: this.token,
    }
    this.errorMsg = false;
    this._service.editProfile(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.editProfile = res.data;
          var geocoder = new google.maps.Geocoder;
          var latlng = {lat: parseFloat(this.editProfile.latitude), lng: parseFloat(this.editProfile.longitude)};
          geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
              if (results[0]) {
                this.userLocation = results[0].formatted_address.toString();
                $("#location").val(this.userLocation)
                
              } else {
                ////console.log("the google map geocoderNo results found")
              }
            } else {
              ////console.log("the google map geocoderNo results found")
            }
    });
          if (res.data.avgRating) {
            this.starActive(res.data.avgRating);
          }
        } else {
          this.errorMsg = res.message;
        }
      });
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.saveProfile();
    }
  }


  saveProfile() {
    let flag = $("#mobileSignin").intlTelInput("getSelectedCountryData");
    var place = $(".place").val();
    var lat = $(".lat").val();
    var lng = $(".lng").val();
    var image = $(".settingImg").attr("src");
    if (image != '../assets/images/default-thumnail.png') {
      ////console.log("not image", image);
      this.editProfile.profilePicUrl = image;
    }
    this.editProfile.place = place;
    this.editProfile.latitude = lat;
    this.editProfile.longitude = lng;
    // if (this.editProfile.phoneNumber.charAt(0) != '+') {
    //   this.editProfile.phoneNumber = "+" + flag.dialCode + this.editProfile.phoneNumber;
    // }
    this.loaderButtonProfile = true;
    setTimeout(() => {
      this.loaderButtonProfile = false;
    }, 3000);
    if(!this.errorMsg){
    // ////console.log(this.editProfile);
    this._service.saveProfilePost(this.editProfile)
      .subscribe((res) => {
        if (res.code == 200) {
          this.loaderButtonProfile = false;
          this._conf.setItem('authToken', res.token);
          this._conf.setItem('profilePicUrl', image);
          this._conf.setItem('username', this.editProfile.username);
          // this.editData = res.data; 
          (this.languageCode == 'en') ? 
          this.errorMsg = "Success" : 
          this.languageCode == "fr" ? this.errorMsg = "Succès" : this.errorMsg = "نجاح";
          this._conf.setItem('phone', this.phoneNumber);
          ////console.log("the phone number saved:", this._conf.getItem('phone'));
          this.phoneNumber = "+" + flag.dialCode + this.phoneNumber;
          this.phonenumberCheck(this.phoneNumber);
          this._missionService.confirmheaderRefresh(this.headerRefresh);
          setTimeout(() => {
            this.errorMsg = false;
            $(".modal").modal("hide");
            $("#editProfile1").modal('hide');
            setTimeout(()=>{
              this._router.navigate([this.languageCode,'otpVerification']);
            },1000);
            // $("#verified").modal('show');
            // $('.errorMsgs').text('Changes Are Saved')
          }, 3000);
        } else {
          this.errorMsg = res.message;
          setTimeout(() => {
            this.errorMsg = false;
          }, 3000);
        }
      });
    }
  }

  // fileUploader(event) {
  //   this.errorMsg = false;
  //   $(".imgCloudinary").show();
  //   setTimeout(() => {
  //     this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
  //       this.cloudinaryImage = JSON.parse(response);
  //       $(".settingImg").attr("src", this.cloudinaryImage.url);
  //       $(".imgCloudinary").hide();
  //       ////console.log(this.cloudinaryImage.url);
  //       this.uploader.uploadAll();
  //       return { item, response, status, headers };
  //     };
  //   }, 200);
  // }

  fileUploader(event:EventEmitter<File[]>) {
    console.log("event, vent", event)
    this.uploader.clearQueue();
    const file: File = event[0];
    this.imgCompressService.resizeImage(file, 600, 800).subscribe(
      result => {
        const newImage = new File([result], result.name);
        //console.log("result", result);
      this.uploader.onBeforeUploadItem = (fileItem: any): any => {
      //console.log("compress image result fileItem",fileItem )
      fileItem.withCredentials = false;
      fileItem.secure = true;
    }
    this.uploader.onAfterAddingFile = (fileItem:any) => {
      fileItem.withCredentials = false;
      fileItem.upload_preset = this.Cloudinary.config().upload_preset;
      this.uploader.cancelAll();

    };
    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      this.uploader.cancelAll();
      form.append('upload_preset', this.Cloudinary.config().upload_preset);
    };
    this.uploader.onSuccessItem  = this.imageUploader;

        this.uploader.addToQueue([newImage]);
        this.uploader.uploadAll();
    },
      error => console.log(error));
  }

  changePassword() {
    $(".modal").modal("hide");
    this._router.navigate([this.languageCode,'password']);
  }

  emailClick() {
    this._router.navigate([this.languageCode,'emailVerify']);
  }

  itemDetails(list) {
    list.productName = list.productName.split(' ').join('-')
    this._router.navigate(["./"+this.languageCode,list.productName, list.postId]);
  }
  signInWithGoogle(): void {
    this._auth.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => 
      {
        // this.fullName = x.name;
        // this.email = x.email;
        this.onGoogle();
      });
 
  }

  signInWithFB(): void {
    // window.location.reload();
    this._auth.signIn(FacebookLoginProvider.PROVIDER_ID).then(x =>
       {
        // this.fullName = x.name;
        // this.email = x.email;
         this.facebookVerify(x);
        });
    // this._auth.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  facebookVerify(value) {
        let list = {
          token: this.token,
          accessToken: value.authToken
        }
        ////console.log("the list :", list)
        this._service.facebookPost(list)
          .subscribe((res) => {
            if (res.code == 200) {
              this.editProfile.facebookVerified == "1";
              var currentLocation = window.location;
              this._conf.setItem("pathname", currentLocation.pathname);
              // this._router.navigate([this.languageCode]);
              this._router.navigate(['']);              
              window.location.replace('');  
            } else {
              $("#verified").modal("show");
              $(".errorMsgs").text(res.message);
            }
          });
    //   }
    // });
  }

  onGoogle() {
      this._auth.authState.subscribe((user) => {        
        ////console.log("test", user);
        if (user.provider == 'GOOGLE') {
          this._zone.run(() => {
          var googleUser = user;
      let list = {
        googleId: googleUser.id,
        accessToken: googleUser.idToken,
        token: this.token,
      }
      this._service.googlePost(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.editProfile.googleVerified == "1";
            var currentLocation = window.location;
            this._conf.setItem("pathname", currentLocation.pathname);
            // this._router.navigate([this.languageCode]);
            this._router.navigate(['']);             
            window.location.replace('');
          } else {
            $("#verified").modal("show");
            $(".errorMsgs").text(res.message);
          }
        });
      });
    }
    });
  }
  payPalList() {
    let list = {
      paypalUrl: this.payPal,
      token: this.token,
    }
    this.loaderButtonProfile = true;
    this._service.paypalPost(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.editProfile.paypalUrl == "1";
          $(".modal").modal("hide");
          this.loaderButtonProfile = false;
        }
      });
  }
  emailValidation(value) {
    this.errorMsg = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.errorMsg = false;
        this.registerSave = true;
        // $("#editProfile1").modal('hide');
        this.emailClick();
      } else {
        (this.languageCode == 'en') ? 
        this.errorMsg = "email is incorrect" : 
        this.languageCode == "fr" ? this.errorMsg = "l'email est incorrect" :
        this.errorMsg = "البريد الإلكتروني مفقود";
        this.registerSave = false;
      }
    }
  }
  mobileValidation(value) {
    this.errorMsg = false;
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    let flag = $("#mobileSignin").intlTelInput("getSelectedCountryData");
    this.phoneNumber = value;
    if (value.match(regexPhone)) {
      this.errorMsg = false;
      this.registerSave = true;
      // this._conf.setItem('phone', this.phoneNumber);
      let flag = $("#mobileSignin").intlTelInput("getSelectedCountryData");
      this._conf.setItem('isoCode',flag.iso2);
      // $("#editProfile1").modal('hide');
      // setTimeout(()=>{
      //   this._router.navigate(['./otpVerification']);
      // },1000);
    } else {
      var val = value.replace(/([^+0-9]+)/gi, '');
      this.phone_Error = true;
      (this.languageCode == 'en') ? 
      this.errorMsg = "phone number is incorrect": 
      this.languageCode == "fr" ? this.errorMsg = 'le numéro de téléphone est incorrect' :
      this.errorMsg = "رقم الهاتف مفقود";
      this.registerSave = false;
    }
  }
  phoneNumber:any;
  phonenumberCheck(val) {
    this.mobileValidation(val);
    let flag = $("#mobileSignin").intlTelInput("getSelectedCountryData");
    let list = {
      phoneNumber: val
    }
    ////console.log("the hefbvffv dial code:",flag)
    // this.phoneNumber = list.phoneNumber;
    this.homeService.phoneNumberCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.errorMsg = res.message;
          // this.phonenumber = "";
          setTimeout(() => {
            this.errorMsg = false;
          }, 3000);
        } else {
          this.errorMsg = false;
            let otpBody ={
              token:this._conf.getItem('authToken'),
              phoneNumber: "+" + flag.dialCode + this._conf.getItem('phone')
            }
            ////console.log("the opt send from settings:",otpBody )
            this._service.getOtp(otpBody).subscribe((res)=>{
              if(res.code ==200){
              $("#editProfile1").modal('hide');
              setTimeout(()=>{
                this._router.navigate([this.languageCode,'otpVerification']);
              },1000);
            }
            },error=>{
                ////console.log("the error is:", error)
            })
        }
      });
  }
  // format price based on dirham currency (DH)
  getDirhamCurrencyPrice(list){
    let price = parseInt(list.price).toFixed(2);
    price = this._conf.getDirhamCurrencyPrice(list);
    // let price = list.price.toFixed(2);
    // if(list.currency == 'DH' || list.currency == 'Dirham'|| list.currency == 'dirham' || list.currency == 'dh'){
    //     price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //     price = price.replace('.',',') ; 
    // }
    // if(list.currency == 'DH' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'Dirham' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'dirham' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'dh' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // list.currency == 'DH' ? price.replace(',',' ').replace('.',',')  : '';
    return price;
  
  }
    // for performance purpose us trackBy for *ngFor directive
  trackByIndex(index, item) {
    return index;
  }

}



