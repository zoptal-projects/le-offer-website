import { Component, OnInit, HostListener } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import {Location} from '@angular/common';
import { HomeService } from '../home/home.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { LanguageService } from '../app.language';
import { DeviceDetectorService } from 'ngx-device-detector';
import {BreakpointObserver} from '@angular/cdk/layout';

declare var $: any;
declare var google: any;
declare var moment: any;
@Component({
  selector: 'app-searchproducts',
  templateUrl: './searchproducts.component.html',
  styleUrls: ['./searchproducts.component.css']
})


export class SearchproductsComponent implements OnInit {
  innerWidth:any;
  searchProduct:any;
  isSmallScreen:any;
  activeQueryParams:any;
  @HostListener('window:size', ['$event'])
  onSize(event) {
    this.innerWidth = window.innerWidth;
  }
  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private _conf: Configuration,
    private _router: Router,
    private _service: HomeService,
    private _lang: LanguageService,
    private activeRoute: ActivatedRoute,
    private _location: Location,
    private deviceDetectorService: DeviceDetectorService,
    private breakpointObserver : BreakpointObserver
  ) {
    this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 992px)');
    this.getSearchProduct('');
    
  }
  private windowWidth:any;
  headerClose: string;
  token: string;
  offset = 0;
  queryParams:any;
  limit = 40;
  allList: any = [];
  allListEmpty: any = [];
  catList: any;
  catSelectList = false;
  subCat: any;
  catSubFilter: any;
  loadMoreList = false;
  loadMoreListArrow = false;
  allListData = false;
  placelatlng: any;
  recentCity: any;
  listLength: any;
  listsLength = false;
  offsetImg = 100;
  defaultImage = 'assets/images/lazyLoad.png';
  sellEmail: string;
  sellPhone: string;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  succesSeller = false;
  howPopupSuccess = false;
  listSell: any;
  signUpContent: boolean = true;

  filterPrice = false;
  categoryName: any;
  latlng: any;
  priceOrder = "lowToHigh";
  timeOrder = 0;
  distance = 30;
  minPrice: any;
  maxPrice: any;
  filterScroll: any;
  postedWithin: any = 0;
  currency: any;
  DefualtCurrency:any;
  bannerShow = false;
  loadCateg = false;

  home: any;
  data: any;

  infowindow: any;
  map: any;
  place: any;

  shimmer: any = [];
  searchItem = 0;
  IpinfoData:any;
  languageCode:any;
  ngOnInit() {
    this._conf.removeItem("CountryCode");
    let selectedLang  = Number(this._conf.getItem("Language"));
    if(selectedLang){
      switch(selectedLang){

        case 1: this.home = this._lang.engHome1;
                this.languageCode= 'fr';
                break;
        case 2: this.home = this._lang.engHome;
                this.languageCode= 'en';
                break;
        case 3: this.home = this._lang.engHome2;
                this.languageCode= 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.home = this._lang.engHome1;
      this.languageCode = 'fr';
    }
    let urlPath = this._conf.getItem("pathname");
    if (urlPath) {
      let list = urlPath.split("/");
      var index = list.indexOf("website");
      index > -1 ? list.splice(index, 1) :'';
      let url = list.join('/');
      this._router.navigate([url]);
      this._conf.removeItem("pathname");
    } else {

      this.seo();
      this.placeHolder();

      $(window).on('popstate', function () {
        $('.modal').modal('hide');
      });
      this.isSmallScreen ? this._missionService.confirmheaderClose(this.headerClose) :  this._missionService.confirmheaderOpen(this.headerClose);
      this.token = this._conf.getItem('authToken');
      let latlng = this._conf.getItem('latlng');
      let cityArea = this._conf.getItem('recentCity');
      if (cityArea) {
        ////console.log("city area is there",cityArea);
        $(".location").text(cityArea.toString());
        $("#cityAreai").val(cityArea.toString());
        $(".location").val(cityArea);
        this.recentCity = cityArea;
      }
      this.listGetCategories();
      // !latlng ? this.currentLocation() : Object.keys(this.activeQueryParams).length <= 0 ? this.searchCategory() :'';
      !latlng ? this.currentLocation() :'';
      //card#304. issue: Website - sidepanel refresh has an intermittent search, fixed by:sowmya Sv 
      setTimeout(() => {
        let latlng1 = this._conf.getItem('latlng');
        if (!latlng1 || !cityArea) {
          this.latLngNull();
        }
      }, 3000);
      this.currency = "USD";
      this.DefualtCurrency= "USD";
    }
  setTimeout(()=>{
    this.updatePresentUrl(0,'');
  },800);
  }

  placeHolder() {
    for (var i = 0; i < 12; i++) {
      this.shimmer.push(i);
    }
  }

  public currencyList = [
    { value: "ARP", text: "Argentina Pesos" },
    { value: "ATS", text: "Austria Schillings" },
    { value: "AUD", text: "Australia Dollars" },
    { value: "BSD", text: "Bahamas Dollars" },
    { value: "BBD", text: "Barbados Dollars" },
    { value: "BEF", text: "Belgium Francs" },
    { value: "BMD", text: "Bermuda Dollars" },
    { value: "BRR", text: "Brazil Real" },
    { value: "BGL", text: "Bulgaria Lev" },
    { value: "CAD", text: "Canada Dollars" },
    { value: "CHF", text: "Switzerland Francs" },
    { value: "CLP", text: "Chile Pesos" },
    { value: "CNY", text: "China Yuan Renmimbi" },
    { value: "CYP", text: "Cyprus Pounds" },
    { value: "CSK", text: "Czech Republic Koruna" },
    { value: "DEM", text: "Germany Deutsche Marks" },
    { value: "DKK", text: "Denmark Kroner" },
    { value: "DZD", text: "Algeria Dinars" },
    { value: 'DH' , text: 'Dirham'},
    { value: "EGP", text: "Egypt Pounds" },
    { value: "ESP", text: "Spain Pesetas" },
    { value: "EUR", text: "Euro" },
    { value: "FJD", text: "Fiji Dollars" },
    { value: "FIM", text: "Finland Markka" },
    { value: "FRF", text: "France Francs" },
    { value: "GRD", text: "Greece Drachmas" },
    { value: "HKD", text: "Hong Kong Dollars" },
    { value: "HUF", text: "Hungary Forint" },
    { value: "ISK", text: "Iceland Krona" },
    { value: "INR", text: "India Rupees" },
    { value: "IDR", text: "Indonesia Rupiah" },
    { value: "IEP", text: "Ireland Punt" },
    { value: "ILS", text: "Israel New Shekels" },
    { value: "ITL", text: "Italy Lira" },
    { value: "JMD", text: "Jamaica Dollars" },
    { value: "JPY", text: "Japan Yen" },
    { value: "JOD", text: "Jordan Dinar" },
    { value: "KRW", text: "Korea(South) Won" },
    { value: "LBP", text: "Lebanon Pounds" },
    { value: "LUF", text: "Luxembourg Francs" },
    { value: "MYR", text: "Malaysia Ringgit" },
    { value: 'MAD', text: 'Moroccan dirham'},
    { value: "MXP", text: "Mexico Pesos" },
    { value: "NLG", text: "Netherlands Guilders" },
    { value: "NZD", text: "New Zealand Dollars" },
    { value: "NOK", text: "Norway Kroner" },
    { value: "PKR", text: "Pakistan Rupees" },
    { value: "PHP", text: "Philippines Pesos" },
    { value: "PLZ", text: "Poland Zloty" },
    { value: "PTE", text: "Portugal Escudo" },
    { value: "ROL", text: "Romania Leu" },
    { value: "RUR", text: "Russia Rubles" },
    { value: "SAR", text: "Saudi Arabia Riyal" },
    { value: "SGD", text: "Singapore Dollars" },
    { value: "SKK", text: "Slovakia Koruna" },
    { value: "SDD", text: "Sudan Dinar" },
    { value: "SEK", text: "Sweden Krona" },
    { value: "TWD", text: "Taiwan Dollars" },
    { value: "THB", text: "Thailand Baht" },
    { value: "TTD", text: "Trinidad and Tobago Dollars" },
    { value: "TRL", text: "Turkey Lira" },
    { value: "USD", text: "United States Dollars" },
    { value: "VEB", text: "Venezuela Bolivar" },
    { value: "ZMK", text: "Zambia Kwacha" },
    { value: "XAG", text: "Silver Ounces" },
    { value: "XAU", text: "Gold Ounces" },
    { value: "XCD", text: "Eastern Caribbean Dollars" },
    { value: "XDR", text: "Special Drawing Right(IMF)" },
    { value: "XPD", text: "Palladium Ounces" },
    { value: "XPT", text: "Platinum Ounces" },
    { value: "ZAR", text: "South Africa Rand" },
    { value: 'RSD', text:'Serbian dinar'}
  ]

  latLngNull() {
    sessionStorage.setItem("CountryCode",'MA');
    $.get("https://ipapi.co/json/").done(response=>{
      this.placelatlng = {
        'lat': response.latitude,
        'lng': response.longitude
      }
      sessionStorage.setItem('IpInfo',JSON.stringify(response) );
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      // this.searchCategory();
      this.recentCity = response.city + ", " + response.country;
      this._conf.setItem('recentCity', this.recentCity);
      this._conf.setItem('currentCity',this.recentCity);
      $("#cityAreai").val(this.recentCity);
      $(".location").text(this.recentCity);
      this.searchCategory();
  }).fail(error=>{
    this.placelatlng = {
      'lat': "33.5731",
       'lng': "-7.5898"
     }
     this._conf.setItem('latlng',JSON.stringify(this.placelatlng))
     $("#cityAreai").val("2, Casablanca 20250, Morocco");
     $(".location").text("2, Casablanca 20250, Morocco");
     $(".location").val("2, Casablanca 20250, Morocco");
     this._conf.setItem('recentCity', "2, Casablanca 20250, Morocco");
     this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
     this.recentCity = "2, Casablanca 20250, Morocco";
    this.searchCategory();
  });
}
  seo() {
    let list = {
      type: 1
    }
    this._service.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.data = res.data;
          if (this.data) {
            this._meta.addTag({ name: 'title', content: this.data.title })
            this._meta.addTag({ name: 'description', content: this.data.description })
            this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
          }
        }
      });
  }

  emptyFigure() {

    for (var i = 0; i < 20; i++) {
      this.allListEmpty.push(i);
    }
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this._conf.setItem('currentLoc',JSON.stringify(this.placelatlng) )
        let latlng = this._conf.getItem('latlng');
        var latilng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latilng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (var i = 0; i < results[1].address_components.length; i += 1) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    var city = addressObj.long_name;
                  }
                  if (addressObj.types[j] === 'country') {
                    var country = addressObj.short_name;
                  }
                }
              }
              $("#cityAreai").val(city + ", " + country);
              $(".location").text(city + ", " + country);
              this._conf.setItem('recentCity', city + ", " + country);
              this._conf.setItem('currentCity', city + ", " + country);
            }
          }
        });
        latlng ?  this.searchCategory() :'';
      }, this.geoError);
    } else {
      this.placelatlng = {
        'lat': "33.5731",
        'lng': "-7.5898"
      }
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      $("#cityAreai").val("2, Casablanca 20250, Morocco");
      this._conf.setItem('recentCity',"2, Casablanca 20250, Morocco");
      this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
      $("#searchLocation1").val("2, Casablanca 20250, Morocco");
    }
  }

  geoError() {
  }

  itemDetails(postId, name) {
    let replace = name.split(' ').join('-');
    this._router.navigate(["./", this.languageCode,replace, postId]);
  }

  public bgcolor = [
    { 'bgcolor': 'rgb(134, 176, 222)' },
    { 'bgcolor': 'rgb(255, 63, 85)' },
    { 'bgcolor': 'rgb(115, 189, 197)' },
    { 'bgcolor': 'rgb(232, 111, 91)' },
    { 'bgcolor': 'rgb(166, 196, 136)' },
    { 'bgcolor': 'rgb(245, 205, 119)' },
    { 'bgcolor': 'rgb(190, 168, 210)' },
    { 'bgcolor': 'rgb(252, 145, 157)' },
    { 'bgcolor': 'rgb(83, 143, 209)' },
    { 'bgcolor': 'rgb(209, 169, 96)' },
    { 'bgcolor': 'rgb(227, 74, 107)' }
  ]

  listGetCategories() {
    this.catSelectList = false;
    this.selectCatList = [];
    this._service.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

  loadCat() {
    this.loadCateg = !this.loadCateg;
  }

  showWorldData:boolean = false;
  allListPosts() {
    this.bannerShow = false;
    let list = {
      offset: this.offset,
      limit: this.limit
    }
    this.placelatlng = this._conf.getItem('latlng');
    if(this.token){
      if (this.placelatlng) {
        this.placelatlng = JSON.parse(this.placelatlng);
        list['latitude'] = this.placelatlng.lat;
        list['longitude'] = this.placelatlng.lng;
      }
  
    }
    if (this.offset == 0) {
      this.allListData = false;
      setTimeout(() => {
        this.allListData = true;
      }, 3000);
    }
    this._service.homeAllList(list)
      .subscribe((res) => {
        this.allListData = true;
        let allList = res.data;
        if (res.code == 200) {
          this.offset === 0 ? this.allList = [] :'';
          allList.forEach(element => {
            this.allList.push(element);
          });
          this.allList && this.allList.length > 40 ? this.removeDublicates(this.allList,'postId'):'';
          if (this.allList && this.allList.length > 0) {
            this.listsLength = false;
          } else {
            this.listsLength = true;
            this.allListData = true;
          }
          this.listLength = this.allList.length;
          this.filterScroll = 0;
        } else {
          this.listsLength = true;
          this.allListData = true;
        }
      });
  }
  removeDublicates(arr,key){
    let uniqIds = {};
    let filtered = arr.filter(obj => !uniqIds[obj.postId] && (uniqIds[obj.postId] = true));
    this.allList = filtered;
  }
  onScroll() {
    if (this.listLength == 40 && this.filterScroll == 0) {
      this.offset += 1;
      this.searchCategory();
    } else if (this.listLength == 40 && this.filterScroll == 1) {
      this.offset += 40;
      this.searchCategory();
    }
  }

  emailValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.email_Error = false;
        this.registerSave = true;
      } else {
        this.email_Error = true;
        this.registerSave = false;
      }
    } else {
      this.email_Error = false;
    }
  }

  mobileValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      if (value.match(regexPhone)) {
        this.phone_Error = false;
        this.registerSave = true;
      } else {
        var val = value.replace(/([^+0-9]+)/gi, '')
        this.sellPhone = val;
        this.phone_Error = true;
        this.registerSave = false;
      }
      if (value.length == 10) {
        this.phone_Error = true;
        this.registerSave = false;
      }
    } else {
      this.phone_Error = false;
    }
  }

  sellHowSend() {
    if (this.sellEmail && this.sellPhone) {
      this.howPopupSuccess = false;
      this.phone_Error = false;
      this.email_Error = false;
      this.PEerror = "Please try anyone";
      setTimeout(() => {
        this.PEerror = false;
      }, 3000);
    } else if (this.sellEmail || this.sellPhone) {
      if (this.sellEmail) {
        this.sellSend(1);
      } else {
        this.sellSend(2);
      }
      this.PEerror = false;
    }
  }

  emptySell() {
    this.sellPhone = "";
    this.sellEmail = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.sellEmail
      }
    } else {
      this.listSell = {
        type: "2",
        phoneNumber: this.sellPhone
      }
    }
    this._service.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.howPopupSuccess = true;
          this.emptySell();
        } else {
          this.PEerror = res.message;
        }
      });
  }

  milesDistance(val) {
    var input = $(".slider_range").val();
    var value = (input - $(".slider_range").attr('min')) / ($(".slider_range").attr('max') - $(".slider_range").attr('min'));
    $(".slider_range").css('background-image',
      '-webkit-gradient(linear, left top, right top, '
      + 'color-stop(' + value + ', #008B9A), '
      + 'color-stop(' + value + ', #C5C5C5)'
      + ')'
    );
    this.offset = 0;
    if (val == 50) {
      val = 30;
    }
    this.distance = val;
    if (val != 0) {
      // this.navigateToFoo('dist',val,2);
      setTimeout(()=>{
       !this.isSmallScreen ?  this.filterToggle() : '';
      },1000)
    } else {
      this.searchCategory();
      // this.allListPosts();
    }

  }

  signUpContentToggle() {
    this.signUpContent = !this.signUpContent;
  }

  postedWithinList(val, index) {
    this.postedWithin = val;
    if (val == 0) {
      $('.radioClass').removeAttr('checked');
      this.categoryVal = [];
      this.catList.forEach(x => {
        x.activeimg = false;
      });
      this.categoryVal = [];
      this.selectCatList = [];
      this.catSelectList = false;
      this.categoryName = '';
      this.subCategoryName = '';
      this.selectedCategoryNodeId = '';
      this.subCategoryNodeId = '';
      // this.allListPosts();
      this.subCat = [];
      if(this.subCat){
        this.subCat.forEach(x => {
          x.activeimg = false;
        });
      } 
      this.catSubFilter = [];
      this.activeQueryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty( this.activeQueryParams,'subCat') : '' ;
      this.activeQueryParams.hasOwnProperty('cat') ? Reflect.deleteProperty( this.activeQueryParams,'cat') : '';
      this.updatePresentUrl(3,'cat');
      $("html, body").animate({ scrollTop: 0 }, 500);
      if (index == 0) {
        $("#sideDetailsId").removeClass('active');
        $("body").removeClass("hideHidden");
        // this.allListPosts();
        this.searchCategory();
        this.listGetCategories();
        this.offset = 0;
      }
      // this.allListPosts();
    } else if (val == 10) {
      $('.radioClass').removeAttr('checked');
      $('.inputCheckBox').removeAttr('checked');
      $("#myRange").val(0);
      $(".slider_range").css('background' ,'#d3d3d3');
      $(".froms").val('');
      $(".tos").val('');
      $('#fromToPricingInput').val('');
      $('.fromToPricingInput').val('');
      $('.inputBox').val('');
      this.currency = this.DefualtCurrency;
      this._conf.setItem('latlng', (this._conf.getItem('Currentlatlng')));
      this.placelatlng = JSON.parse(this._conf.getItem('Currentlatlng'));
      $("#cityAreai").val(this._conf.getItem('currentCity'));
      $(".location").text(this._conf.getItem('currentCity'));
      this._conf.setItem('recentCity', this._conf.getItem('currentCity'));
      $("#lati").val(this.placelatlng.lat),
      $("#lngi").val(this.placelatlng.lng)
      this.categoryVal = [];
      this.catList.forEach(x => {
        x.activeimg = false;
      })
      if(this.subCat){
        this.subCat.forEach(x => {
          x.activeimg = false;
        });
      }
      this.subCat = [];
      this.selectCatList = [];
      this.catSubFilter = "";
      this.catSelectList = false;
      this.selectedCategoryNodeId = '';
      this.subCategoryName = '';
      //card#366, issue : Website - sidepanel reset, fixedBy:SowmyaSV, date: 3-1-20
      this.subCategoryNodeId = '';
      this.categoryName  = '';
      this.activeQueryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty( this.activeQueryParams,'subCat') : '' ;
      this.activeQueryParams.hasOwnProperty('cat') ? Reflect.deleteProperty( this.activeQueryParams,'cat') : '';
      this.updatePresentUrl(3,'cat');
      // this.filterToggle();
      // this.updatePresentUrl(2,'');
    } else {
      this.offset = 0;
      this.postedWithin = val;
      console.log("val, this.post", this.postedWithin)
      // this.navigateToFoo('postWithIn', val,5);
      setTimeout(()=>{
        !this.isSmallScreen ? this.filterToggle() :'';
      },1000)
    }
    // if(this.subCat){
    //   this.subCat.forEach(x => {
    //     x.activeimg = false;
    //   })
    // }    
  }



  sortPrice(val) {
    this.offset = 0;
    this.priceOrder = val;
    console.log("val, this.post", this.priceOrder)
    //Card #373, issue: Website hangs in a special case, fixedBy: sowmyaSV, date: 6-1-20
    // this.navigateToFoo('sortPrice', val,4);
    setTimeout(()=>{
      !this.isSmallScreen ? this.filterToggle() :'';
    },1000)
  }

  numberDitchit(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".from1").val(val);
  }

  numberDitchit1(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".to1").val(val);
  }

  minimumPrice(val) {
    this.offset = 0;
    this.minPrice = val;
    setTimeout(()=>{
      !this.isSmallScreen ?  this.filterToggle() :'';
    },1000);
    // this.navigateToFoo('minPrice',val,6);
  }

  maximumPrice(val) {
    this.offset = 0;
    this.maxPrice = val;
    // this.navigateToFoo('maxPrice',val,7);
    setTimeout(()=>{
      !this.isSmallScreen ?  this.filterToggle() :'';
    },1000)

  }

  selectCurrency(val) {
    this.currency = val;
    setTimeout(()=>{
      !this.isSmallScreen ?  this.filterToggle() :'';
    },1000)
    // this.navigateToFoo('currency', val,5 )
  }

  categoryVal = [];
  catArray = [];
  selectCatList = [];
  selectedCategoryNodeId:any;
  searchCategoryName(val, i, filter) {
    $("#nameCategory").val("");
    this.selectCatList = [];
    this.catList.forEach(x => {
      x.activeimg = false
    })
    this.catList[i].activeimg = true;
    this.selectCatList.push(this.catList[i]);
    this.catSelectList = true;
    this.subCat = "";
    this.catSubFilter = "";
    filter == 0  ? (this.subCategoryName = "", this.subCategoryNodeId = '') :'';
    // console.log("filter", filter)
    this.categoryName = this.catList[i].name;
    this.searchProduct = this.categoryName;
    this.selectedCategoryNodeId = this.catList[i].categoryNodeId;
    this.filterPrice = false;
    this.offset = 0;    
    filter == 0 ? this.searchItem = 0 :this.searchItem = 1;
    this._service.getSubCategoriesList(this.catList[i].name,this.languageCode,this.selectedCategoryNodeId)
      .subscribe((res) => {
        this.subCat = res.data;
        if(this.subCategoryName){
          if(this.subCat){
            // console.log("search category1");
            let index = this.subCat.findIndex((item)=> item.subCategoryName == this.subCategoryName);
            this.catSubFilter = [];
            index > -1 ? this.subCategory(index,1) : '';
            this.searchCategory();
          }
        }
        else{
          // console.log("search category2", this.activeQueryParams);
          filter == 0 && this.queryParams && this.queryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty(this.queryParams,'subCat'): '';
          filter == 0 &&  this.activeQueryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty( this.activeQueryParams,'subCat') :'';
          filter == 0 ?  this.updatePresentUrl(1,'subCat'): ! this.subCat ? this.addAdvancedFilters(this.catList[i]) :'';
        }
      });
      // !this.isSmallScreen &&  filter == 0 ? this.searchCategory() :'';
      // !this.isSmallScreen &&  filter == 0 ? this.searchCategory() :  !this.subCategoryName && filter == 1 ? this.searchCategory() : '';
      (this.categoryName == "") ? this.postedWithinList(0, 1) :'';
      // setTimeout(()=>{
        filter == 0 && !this.isSmallScreen ? this.navigateToFoo('cat',this.categoryName,9) :'';
      // },1000)
  }
    // card#201, if category or subCategory has the advanced filters show and add them, fixedBy: sowmyaSV
    addAdvancedFilters(filterArr){
      var value = [];
      if(filterArr.filter && filterArr.filter.length > 0){
        filterArr.filter.forEach(x => {
          if (x.type == 2 || x.type == 4 || x.type == 6) {
            x.filterData = x.values.split(",");
            // x.value = split;
          }
          x.data = [];
          x.checked = false;
          value.push(x);
        });
        ////console.log("the cat filter is:", value);
        this.catSubFilter = value;
      }
    }
  cancelCat(i,val) {
    this.catList.forEach(x => {
      x.activeimg = false
    });
    this.subCat = "";
    this.categoryName = "";
    this.catSubFilter = "";
    this.selectCatList  = [];
    this.subCategoryNodeId = '';
    this.selectedCategoryNodeId = '';
    (this.isSmallScreen && !$("#sideDetailsId").hasClass("active")) ? this.postedWithinList(0, 0) :'';
    this.activeQueryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty( this.activeQueryParams,'subCat') : '' ;
    this.activeQueryParams.hasOwnProperty('cat') ? Reflect.deleteProperty( this.activeQueryParams,'cat') : '';
    this.updatePresentUrl(3,'cat');
    // this.listGetCategories();
  }
  // if any subcategory or category de-seelcted then remove them from url 
  updatePresentUrl(val,caseVal){
      let result1 =  Object.assign({}, this.activeQueryParams);
      val === 1 && caseVal == 'cat' ? result1.hasOwnProperty('cat') ? Reflect.deleteProperty(result1,'cat') : '' : '';
      val === 1 && caseVal == 'subCat' ? result1.hasOwnProperty('subCat') ?( Reflect.deleteProperty(result1,'subCat'), this.subCategoryName = '', this.subCategoryNodeId = '') : '' : '';
       // if cancel the category
      if(val == 2 || val == 3){
        result1.hasOwnProperty('subCat') ? Reflect.deleteProperty(result1,'subCat') : '' ;
        result1.hasOwnProperty('cat') ? Reflect.deleteProperty(result1,'cat') : '';
      }
      console.log("result",result1);
       if(Object.keys(result1).length > 0){
        this._location.replaceState(
          this._router.createUrlTree(
            [`${this.languageCode}/searchProduct/${this.searchingProduct}`], // Get uri
            {queryParams: result1} // Pass all parameters inside queryParamsObj
          ).toString()
        );
        this.searchItem = 0;
        //card#303, issues: Website - Sidepanel issue, fixed by:sowmyasv
        this.checkIfQueryParams(result1);
       }
       else{
        // console.log("result 1",result1)
         this.searchProduct = this.searchingProduct;
         this.searchItem = 0;
         //card#292, issue: side-pabel issue, fixedBy:sowmyaSV, date: 10-1-20
         this._location.replaceState(
          this._router.createUrlTree(
            [`${this.languageCode}/searchProduct/${this.searchingProduct}`], // Get uri // Pass all parameters inside queryParamsObj
          ).toString()
        );
        // this._location.replaceState(`${this.languageCode}/searchProduct/${this.searchingProduct}`);
        if((val == 1 || val == 2) && !this.isSmallScreen){
          $("html, body").animate({ scrollTop: 0 }, 500);
          $("#sideDetailsId").hasClass('active') ? $("#sideDetailsId").removeClass('active') : '';
          $("body").removeClass("hideHidden");
        }
        !this.isSmallScreen? this.searchCategory() : val  == 0 ? this.searchCategory() : '';
        // this.searchCategory();
        // this.listGetCategories();
       }
  }
  seachedProduct:any;
  searchingProduct:any;
  getSearchProduct(val){
    if(val && val.length > 0){
      this.searchProduct = val;
      this.searchItem = 0;
    }
    else{
        this.activeRoute.params.subscribe((params)=>{
          params['search'] ? (this.searchProduct = params['search'],this.searchItem = 0) : '';
          this.seachedProduct = params['search'];
          this.searchingProduct = params['search'];
        });
        this.activeRoute.queryParams.subscribe((queryParams)=>{
           this.activeQueryParams = queryParams;
        })
    }
  }
  subCategoryName: any;
  subCategoryNodeId:any;
  subCategory(i,val) {
    this.subCategoryName = this.subCat[i].subCategoryName;
    this.searchProduct = this.subCategoryName;
    this.subCat.forEach(x => {
      x.activeimg = false
    })
    this.subCat[i].activeimg = true;
    this.selectCatList.splice(1, 1);
    this.selectCatList.push(this.subCat[i]);
    this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
    var value = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        x.filterData = x.values.split(",");
        x.data = [];
        // x.value = split;
      }
      value.push(x);
    });

    this.catSubFilter = value;
    val != 1 ? this.navigateToFoo('subCat',this.subCategoryName, 1 ) :'';
    !this.isSmallScreen && val != 1 ? this.filterToggle() :'';
  }

  cancelsubCat() {
    this.subCat.forEach(x => {
      x.activeimg = false
    });
    this.catSubFilter = "";
    this.selectCatList.splice(1, 1);
    this.subCategoryNodeId ="";
    this.subCategoryName = "";
    this.categoryName ? this.getSearchProduct(this.categoryName) :'';
    this.searchItem = 1;
    this.searchCategory();
    this.activeQueryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty( this.activeQueryParams,'subCat') : '' ;
    this.updatePresentUrl(1,'subCat');
  }

  cancelCats(i) {
    $("#nameCategory").val("");
    let len = this.selectCatList.length;
    if (i == 1) {
      this.cancelsubCat();
    } else {
      this.postedWithinList(0, 0);
    }
  }


  documentTextList(event, i, range) {
    if (event.target.value.length > 0) {
      if (this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5) {
        if (range == 0) {
          this.catSubFilter[i].from = Number(event.target.value.replace(',','.'));
        } else {
          this.catSubFilter[i].to = Number(event.target.value.replace(',','.'));
        }
        this.catSubFilter[i].types = "range";
        this.catSubFilter[i].checked = true;
      } else if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
        var index = this.catSubFilter[i].data.findIndex(x => x.value == this.catSubFilter[i].filterData[range]);
        if (index > -1) {
          this.catSubFilter[i].data.splice(index, 1)
        } else {
          let list = {
            // type: "equlTo",
            type:this.catSubFilter[i].type,
            fieldName: this.catSubFilter[i].fieldName,
            value: this.catSubFilter[i].filterData[range]
          }
          this.catSubFilter[i].data.push(list);
        }
        if (this.catSubFilter[i].data && this.catSubFilter[i].data.length) {
          this.catSubFilter[i].checked = true;
          this.catSubFilter[i].typed = 4;
        } else {
          this.catSubFilter[i].checked = false;
        }
      } else {
        this.catSubFilter[i].data = event.target.value;
        this.catSubFilter[i].types = "equlTo";
        // this.catSubFilter[i].types =this.catSubFilter[i].type;
        // type:this.catSubFilter[i].type,
        this.catSubFilter[i].checked = true;
      }
    } else {
      this.catSubFilter[i].checked = false;
    }
    // this.navigateToFoo('filter',this.catSubFilter,3 )
    setTimeout(()=>{
      !this.isSmallScreen ? this.filterToggle() :'';
    },1000)

  }

  distances: any;
  errMsg: any;
  loaderButton: any;
  distanceList(val) {
    this.distances = Number(val);
  }

  filterToggle() {
    this.postedWithin == 10 ? this.updatePresentUrl(2,'subCat') : '';
    this.searchItem = 1;
    this.searchCategory();
  }
  appendURlForCurrent(params){
    let routeOfUrl = '';
    let quertParams:any;
    if(params['category']){
      routeOfUrl = this.categoryName;
      if(params['subCategory'] && params['category']){
        routeOfUrl = this.categoryName.concat("/"+this.subCategoryName);
      }
    }
    this._router.navigate(
      [''], 
      {
        relativeTo: this.activeRoute,
        replaceUrl: true,
      });
     setTimeout(() => {
      this._router.navigate(
        [routeOfUrl], 
        {
          relativeTo: this.activeRoute,
          replaceUrl: true,
          queryParams: { myParam: 'myNewValue' },
        }); 
     }, 1000);
      routeOfUrl = '';
  }
  //Card #367 , issue: Website - sidepanel reset, fixedBy:SowmyaSV, date: 3-1-20
  searchCategory() {
    this.searchItem = 1;
    this.errMsg = false;
    setTimeout(() => {
      this.loaderButton = false;
      this.errMsg = false;
    }, 3000);
    var filData = [];
    var filDatas = [];
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true && !x.typed) {       
          let list = {
            type: x.types,
            fieldName: x.fieldName,
            value: x.data || '',
            from: "",
            to: ""
          }
          if (x.types == "range") {
            list.from = x.from;
            list.to = x.to;
          }
          filData.push(list);
        } else {
          if (x.checked == true && x.typed == 4) {
            x.data.forEach(y => {
              let lisT = {
                type: y.type,
                fieldName: y.fieldName,
                value: y.value,
              }
              filData.push(lisT);
            });
          }
        }
      })
    }
    this.bannerShow = true;
    let ltlg = {
      lat: $("#lati").val(),
      lng: $("#lngi").val()
    }
    let ciSt = $("#cityAreai").val();
     $("#lati").val() ?  this._conf.setItem('latlng', JSON.stringify(ltlg)) :'';
     ciSt ? this.recentCity = ciSt :'';
    let lists = {
      token: this._conf.getItem('authToken'),
      offset: this.offset,
      limit: this.limit,
      location: "",
      latitude: "",
      longitude: "",
      currency: this.currency,
      postedWithin: this.postedWithin ||0,
      sortBy: this.priceOrder,
      // Card #370 ,  issue: Website - Sidepanel use case, fixedBy: sowmyaSV, date: 6-1-20
      distance: typeof this.distance == "string" ? parseInt(this.distance) : this.distance,
      langaugeCode: this.languageCode,
      categoryId : this.selectedCategoryNodeId ,
      price:JSON.stringify({
        currency: this.currency,
        from : parseInt(this.minPrice),
        to: parseInt(this.maxPrice),

      }),
      category: this.categoryName,
      subCategory: this.subCategoryName || '',
      subCategoryId: this.subCategoryNodeId || '',
      filter: JSON.stringify(filData),
      productName : this.searchingProduct
    }
    // if(this.token){
      let latlng = this._conf.getItem('latlng');
      if (latlng) {
        let latlng = JSON.parse(this._conf.getItem('latlng'));
        lists.latitude = latlng.lat;
        lists.longitude = latlng.lng;
      }
      if (this.recentCity) {
        lists.location = this.recentCity;
      } else {
        lists.location = this._conf.getItem('recentCity');
      }
    // }
    this.loaderButton = true;
    console.log("lists searched",lists)
    this._service.searchCategoriesList(lists, this.searchItem,this.searchingProduct)
      .subscribe((res) => {
        this.loaderButton = false;
        this.listsLength = true;
        this.allListData = true;
        let allList = res.data;
        if (res.code == 200) {
          //card#251, inifinate scroll issues , fixed by:sowmyaDV
          allList = this.sortPostsBasedOnDates(allList);
          $("#sideDetailsId").removeClass('active');
          $("body").removeClass("hideHidden");
          this.showWorldData = false
          this.offset == 0 ? this.allList = [] :'';
          allList.map((item)=>{
            this.allList.push(item);
          })
          if (this.allList && this.allList.length > 0) {
            this.listsLength = false;
          } else {
            this.listsLength = true;
          }
          this.listLength = this.allList.length;
          this.filterScroll = 1;
        } else {
          this.errMsg = res.message;
          this.listsLength = true;
          this.allListData = true;
          this._missionService.onScrollForFooter(true);
          $("html, body").animate({ scrollTop: 0 }, 500);
          $("#sideDetailsId").removeClass('active');
          $("body").removeClass("hideHidden");
          this.showWorldData = true;
          this.allListPosts();
        }
      });
      this.searchItem = 0;
    }

  setCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this.checkLocation();
      });
    }
    else{
      this.placelatlng = {
        'lat': "33.5731",
        'lng': "-7.5898"
      }

      $("#cityAreai").val("2, Casablanca 20250, Morocco");
      this._conf.setItem('recentCity',"2, Casablanca 20250, Morocco");
      $("#searchLocation1").val("2, Casablanca 20250, Morocco");
      this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
      $(".location").text("2, Casablanca 20250, Morocco");
      $(".location").val("2, Casablanca 20250, Morocco");
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this.checkLocation();
    }
  }


  checkLocation() {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      $("#locationMap").modal("show");
      this.continueMap();
    }
  }
  changeLocation(event) {
    var input = document.getElementById(event.target.id);
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
          }
        }
      }
      if (City) {
        $("#cityAreai").val(City + ", " + state);
      } else if (state) {
        $("#cityAreai").val(state);
      } else {
        $("#cityAreai").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#lati").val(lat);
      $("#lngi").val(lng);
      this.placelatlng = {
        'lat': lat,
        'lng': lng
      }
      this.latlng = {
        lat: lat,
        lng: lng
      };
    });
    event.type === "change" ? this.locationChange() :'';
  }

  currentSelectedLoc:any;
  locationChange1() {
    setTimeout(()=>{
      this.currentSelectedLoc ={
        lat: $("#lati").val(),
        lng : $("#lngi").val()
      }
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this._conf.setItem('recentCity',$("#cityAreai").val());
      !this.isSmallScreen ? this.filterToggle(): '';
    },200)
  }


  locationChange() {
    setTimeout(() => {
      let lat = $("#lati").val();
      let lng = $("#lngi").val();
      this.latlng = {
        lat: lat,
        lng: lng
      };
      this.currentSelectedLoc ={
        lat: $("#lati").val(),
        lng : $("#lngi").val()
      }
      this.placelatlng = this.latlng;
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this._conf.setItem('recentCity',$("#cityAreai").val());
      this.recentCity = this._conf.getItem('recentCity');
      this.continueMap();
    }, 500);
  }


  continueMap() {
    var latlng;
    if(!this.placelatlng){
      latlng = JSON.parse(this._conf.getItem('latlng'));
    }
    else{
      latlng = this.placelatlng;
    }
    var map = new google.maps.Map(document.getElementById('mapData'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      mapTypeControl: false,
      scrollwheel: false,
      // zoomControl: true,
      // scaleControl: false,
      // streetViewControl: false,
      // rotateControl: true,
      // fullscreenControl: true,
      // styles: this._conf.googleMapStyleJson,
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map,
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          $(".addressArea").val(results[0].formatted_address);
          $("#cityAreai").val(results[0].formatted_address);
          $(".location").text(results[0].formatted_address);
        }
      }
    });


    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $("#lati").val(marker.getPosition().lat());
            $("#lngi").val(marker.getPosition().lng());
            $(".location").text(results[0].formatted_address);
            $("#cityAreai").val(results[0].formatted_address);
            $(".addressArea").val(results[0].formatted_address);
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }
 // format price based on dirham currency (DH)
 getDirhamCurrencyPrice(list){
  let price = typeof list.price == 'string' ? parseInt(list.price).toFixed(2) : list.price.toFixed(2);
  price = this._conf.getDirhamCurrencyPrice(list);
  return price;

}
// show default  Casablanca, Morocco if current location items are not there

showCasablancePosts(){
  this.bannerShow = false;
  let list = {
    latitude: "33.5731",
    longitude: "-7.5898",
    offset: 0,
    limit: this.limit
  }
 
  if (this.offset == 0) {
    this.allListData = false;
    setTimeout(() => {
      this.allListData = true;
    }, 3000);
  }
  this._service.homeAllList(list)
    .subscribe((res) => {
      this.allListData = true;
      let allList = res.data;
      if (res.code == 200) {
        if (this.offset == 0) {
          this.allList = [];
        }
        let object = {};
        let result = [];

      this.allList.forEach(function (item,index) {
        let Item =  this.allList[index].postId
        if(!object[Item])
            object[Item] = 0;
          object[Item] += 1;
      })

      for (let prop in object) {
         if(object[prop] <= 2) {
             result.push(prop);
         }
      }
      this.allList = result;

        for (var i = 0; i < allList.length; i++) {
          this.allList.push(res.data[i]);
        }
        if (this.allList && this.allList.length > 0) {
          this.listsLength = false;
        } else {
          this.listsLength = true;
          this.allListData = true;
        }
        this.listLength = this.allList.length;
        this.filterScroll = 0;
      } else {
        this.listsLength = true;
        this.allListData = true;
      }
    });
}
 //card#191 ,bug : sort the posts based on dates, fixedBy:sowmya
 sortPostsBasedOnDates(postList){
  let sortedList = postList.sort(function(a, b){
    return moment.utc(a.postedOn * 1000 , '"h:mm:ss').diff(moment.utc(b.postedOn * 1000 , '"h:mm:ss'))
  });
  return sortedList;
}
//check if query are there 
checkIfQueryParams(queryParams){
  // console.log("qiery ", queryParams,queryParams.hasOwnProperty('subCat'))
   // distance filter
  if(queryParams['dist'] != undefined && queryParams.hasOwnProperty('dist')){
     this.distance = queryParams['dist'];
     this.milesDistance(this.distance);
    //  background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(0.8, rgb(92, 52, 163)), color-stop(0.8, rgb(197, 197, 197)));
  }
  //  // advanced Filter
  (queryParams['filter'] != undefined && queryParams.hasOwnProperty('filter'))?this.catSubFilter = queryParams['filter'] :'';
  // sort based onPrice
  (queryParams['sortPrice'] != undefined && queryParams.hasOwnProperty('sortPrice'))?this.priceOrder = queryParams['sortPrice'] :'';

   // filter based PostWithin
  (queryParams['postWithIn'] != undefined && queryParams.hasOwnProperty('postWithIn')) ? this.postedWithin = queryParams['postWithIn'] :'';
  // filter based minPrice
  (queryParams['minPrice'] != undefined && queryParams.hasOwnProperty('minPrice')) ? this.minPrice = queryParams['minPrice'] : '';
  // filter based maxPrice
  (queryParams['maxPrice'] != undefined && queryParams.hasOwnProperty('maxPrice'))? this.maxPrice = queryParams['maxPrice'] : '';
  // filter based on location 
  (queryParams['location'] != undefined && queryParams.hasOwnProperty('location'))?this.latlng = queryParams['location'] : '';
  // filter based on currency 
  (queryParams['currency'] != undefined && queryParams.hasOwnProperty('currency')) ? this.currency = queryParams['currency'] :'';
  //filters based on category
  (queryParams['cat'] != undefined && queryParams.hasOwnProperty('cat')) ? this.categoryName = queryParams['cat'] :'';
  //filters based on category
  (queryParams.hasOwnProperty('subCat')) ? this.subCategoryName = queryParams['subCat'] :'';

  // auto select cat and subcat 
  setTimeout(()=>{
    if(this.categoryName){
      if(this.catList){
        let index = this.catList.findIndex((item)=> item.name === this.categoryName);
        this.searchItem = 1;
        index > -1 ? this.searchCategoryName(this.categoryName,index,1) : '';
        !queryParams.hasOwnProperty('subCat') ? this.searchCategory() :'';
      }
    }
  },300)
}
// create new params for query params of url
getQueryParams(params){
  var resultantQuery = {...params};
  return resultantQuery;
}
// get queryparams 
navigateToFoo(key, value, caseValue){
  // changes the route without moving from the current view or
  // triggering a navigation event,
  // console.log("cat selected");
  let queries : any;
  // this.categoryName ? catIsSelected = `./category/${this.categoryName}` : catIsSelected = ``;
  this.activeRoute.queryParams.subscribe(params => {
    //card#369, issue: Website - clearing use case, fixedBy:sowmyaSV, date: 6-1-20
    //card#375, issue: Website - sidePanel issue, fixedBy:sowmyaSV, date: 8-1-20
    //card#375, issue: Website - sidePanel issue, fixedBy:sowmyaSV, date: 10-1-20
    // caseValue == 9 && params && params.hasOwnProperty('subCat') ? Reflect.deleteProperty(params,'subCat'): '';
    if(caseValue == 9 && params && params.hasOwnProperty('subCat')){
      params = Object.keys(params).reduce((object, key) => {
        key !== 'subCat' ? object[key] = params[key] : '';
        return object;
      }, {})
    }
    // caseValue == 9 && params && params.hasOwnProperty('subCat') ? delete params.subCat: '';
    caseValue == 9 && this.queryParams && this.queryParams.hasOwnProperty('subCat') ? Reflect.deleteProperty(this.queryParams,'subCat'): '';
    Object.getOwnPropertyNames(params).length > 0 ?  this.queryParams = this.getQueryParams(params):'';
    if(this.queryParams && this.queryParams.hasOwnProperty(key)){
      this.queryParams[key] = value;
    }
    else{
      switch(caseValue){
          case 1: queries = { [key] : value}
          break;
          case 2: queries = { [key] : value}
          // queryStr.set('dist',value );
          break;
          case 3: queries = { [key] : value}
          break;
          case 4: queries = { [key] : value}
          break;
          case 5: queries = { [key] : value}
          break;
          case 6: queries = { [key] : value}
          break;
          case 7: queries = { [key] : value}
          break;
          case 8: queries = { [key] : value}
          break;
          case 9: queries = { [key] : value}
          break;
        }
        this.queryParams = Object.assign(queries,this.queryParams);
      }
  });
  setTimeout(()=>{
    this.pushUrlState();
  },200)
}
stateParam = 0 ;
// set query params for present url
pushUrlState(){
  //console.log("push state")
  const isDesktopDevice = this.deviceDetectorService.isDesktop();
  if(!isDesktopDevice){
    $("html, body").animate({ scrollTop: 0 }, 500);
    $("#sideDetailsId").hasClass('active') ? $("#sideDetailsId").removeClass('active') : '';
    $("body").removeClass("hideHidden");
  }
  this._router.navigate([], {
    queryParams: this.queryParams,
    queryParamsHandling: 'merge',
    relativeTo: this.activeRoute
  });
}

 // loadMore item after scroll down, card#215 issue
 loadMoreItems(){
  if ($('.filterRecent').scrollTop() + $('.filterRecent').innerHeight() >= $('.filterRecent')[0].scrollHeight && this.listLength == 40 && this.filterScroll == 0) {
    this.offset += 40;
    this.allListPosts();
  }
  else if ($('.filterRecent').scrollTop() + $('.filterRecent').innerHeight() >= $('.filterRecent')[0].scrollHeight && this.listLength == 40 && this.filterScroll == 1) {
    this.offset += 40;
    this.searchCategory();
  }
}
// for performance purpose us trackBy for *ngFor directive
trackByIndex(index, item) {
  return index;
}
}
