import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';;
import { MissionService } from '../app.service';
import { Meta } from '@angular/platform-browser';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';
import { Configuration } from '../app.constants';
declare var $:any;
@Component({
  selector: 'trust',
  templateUrl: './trust.component.html',
  styleUrls: ['./trust.component.css']
})
export class TrustComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _serviceHome: HomeService,
    private _meta: Meta,
    private _router: Router,
    private location:Location,
    private route: ActivatedRoute,
    private _conf: Configuration,
    private _lang: LanguageService) { }

  headerOpen: string;
  trust: any;
  languageCode:any;

  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    // this.trust = this._lang.engTrust;
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.trust = this._lang.engTrust1;
                this.languageCode = 'fr';
                break;
        case 2: this.trust = this._lang.engTrust;
                this.languageCode = 'en';
                break;
        case 3: this.trust = this._lang.engTrust2;
                this.languageCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.trust = this._lang.engTrust;
      this.languageCode = 'en';
    }
    //  this.seo();
    this._meta.addTag({ name: 'title', content: 'Saftey Tips | Free Of Spams | Free Of Scams' })
    this._meta.addTag({ name: 'description', content: 'Our goal is to keep MobiVenta 100% free of scams and spam, and we appreciate your cooperation with this!' })
    this._missionService.confirmheaderOpen(this.headerOpen);
    // this._router.navigate([this.languageCode,"trust"])
    this.location.replaceState(`${this.languageCode}/trust`);
  }

  seo() {
    let list = {
      type: 4
    }
    this._serviceHome.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          let data = res.data;
          // ////console.log("desc", data.description);
          if (data) {
            this._meta.addTag({ name: 'title', content: data.title })
            this._meta.addTag({ name: 'description', content: data.description })
            this._meta.addTag({ name: 'keywords', content: data.keyword.toString() })
          }
        }
      });
  }

}
