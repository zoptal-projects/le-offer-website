import { Component, OnInit } from '@angular/core';
import {  Router, ActivatedRoute } from '@angular/router';
import { RouterLinkActive } from '@angular/router';
import { MissionService } from '../app.service';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { HomeService } from '../home/home.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SettingsService } from '../settings/settings.service';
import{ Location } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
declare var $:any;
@Component({
  selector: 'app-optp-verification',
  templateUrl: './optp-verification.component.html',
  styleUrls: ['./optp-verification.component.css']
})
export class OptpVerificationComponent implements OnInit {
  headerOpen:string = '';
  phoneNumber:any;
  toogleOtp:boolean = false;
  toogleOtp1:boolean = true;
  CardTitle:any;
  optVerify:any;
  header:any;
  phone_Error:boolean = false;
  otpNumber:any;
  errorMsg:any;
  phoneError:any
  constructor(private _missionService: MissionService, private homeService:HomeService,
    private _lang:LanguageService, private _conf:Configuration,private router: Router,
    private _service: SettingsService, private route:ActivatedRoute,private location:Location,
    private detectDevice:DeviceDetectorService) { 
    _missionService.missionheaderRefresh$.subscribe(
      headerRefresh => {
        this.ngOnInit();
      });
  }
  lang:any;
  languageCode:any;
  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? 
    //     sessionStorage.setItem("Language",'1'):sessionStorage.setItem("Language",'3')
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.optVerify = this._lang.engOtpVerify1;
                this.header = this._lang.engHeader1;
                this.lang = 'fr';
                break;
        case 2: this.optVerify = this._lang.engOtpVerify;
                this.header = this._lang.engHeader;
                this.lang = 'en';
                break;
        case 3: this.optVerify = this._lang.engOtpVerify2;
                this.header = this._lang.engHeader2;
                this.lang = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.optVerify = this._lang.engOtpVerify;
      this.header = this._lang.engHeader;
      this.lang = 'en';
    }
    if(this._conf.getItem('phone')){
      this.phoneNumber = this._conf.getItem('phone');
      this.phoneNumber == "undefined" && this.lang == 'fr'? this.phoneNumber  = 'indéfini'  :'';
      this.phoneNumber == "undefined" && this.lang == 'ar'? this.phoneNumber  = 'غير محدد'  :'';
      // this.phoneNumber == "undefined" && this.lang == 'ar'? this.phoneNumber  = 'غير محدد'  :'';
      ////console.log("the phone number is:",this._conf.getItem('phone'))
    }
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.toogleOtp = false;
    this.lang == 'en'? this.CardTitle = "Phone Number" : this.lang == 'fr' ? this.CardTitle = "Numéro de téléphone" : this.CardTitle = "رقم الهاتف";
    let devideInfo = this.detectDevice.getDeviceInfo();
    this._conf.setItem('deviceId',devideInfo.browser);
    // this.router.navigate([this.lang,"otpVerification"]);
    this.location.replaceState(`${this.lang}/otpVerification`);
    this.intlInput();
  }
  intlInput(){
    var countrySelected;
    (this._conf.getItem('isoCode')) ? countrySelected = this._conf.getItem('isoCode'):
    countrySelected = this._conf.getItem('countryCode');
    
    $("#phoneNum").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry:  countrySelected || 'MA',
      autoPlaceholder: true,
      // modify the auto placeholder
      // customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
  }
  SendOtp(){
    ////console.log("the toogle otp is :", this.toogleOtp);
    if(this.phoneNumber){
      let flag = $("#phoneNum").intlTelInput("getSelectedCountryData");
      let list = {
          token:this._conf.getItem('authToken'),
          phoneNumber: "+" + flag.dialCode + this._conf.getItem('phone')
        }
        ////console.log("the list for send otp,",list)
        this._service.getOtp(list)
        .subscribe((res) => {
          ////console.log("the res is :",res)
          if (res.code == 200) {
            this.phoneNumber = list.phoneNumber;
            this.lang == 'en'? this.CardTitle = "Verify Otp" : this.lang == 'fr' ? this.CardTitle = "Vérifier Otp" : this.CardTitle = "تحقق من Otp";
            // this.CardTitle ="Verify Otp";
            this.toogleOtp = true;
          }
        },error =>{
          let ErrorRes = error;
          this.errorMsg = ErrorRes.message;
        });
    }
    else{
    this.lang === "en" ?  this.errorMsg ="Please Enter The PhoneNumber" : this.lang = "fr" ? this.errorMsg = "S'il vous plaît entrer le numéro de téléphone":
    this.errorMsg = "الرجاء إدخال رقم الهاتف";
    }
    
  }
  verifyOtp(){
    ////console.log("the toogle otp is :", this.toogleOtp);
    let flag = $("#phoneNum").intlTelInput("getSelectedCountryData");
    let list = {
      otp: this.otpNumber,
      token:this._conf.getItem('authToken'),
      // deviceId: this._conf.getItem('deviceId'),
      phoneNumber: "+" + flag.dialCode + this.phoneNumber
    }
    ////console.log("the verifying code is:",list);
    if(this.otpNumber){
    this.homeService.otpVerify(list)
      .subscribe((res) => {
        ////console.log("the verifying code response", res)
        if(res.code == 200){
          this.router.navigate([this.lang,'settings']);
        }
        else{
          this.lang === "en" ? 
          this.errorMsg =  'Enter Otp Again': this.lang = "fr" ? this.errorMsg = "Entrez à nouveau dans Otp" : this.errorMsg = "أدخل Otp مرة أخرى"
        }
      },error =>{
        let ErrorRes = error;
        this.errorMsg = ErrorRes.message;
      });
    }
  }
  eraseerror(){
    this.errorMsg = '';
  }
  mobileValidation(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;

    if (value.match(regexPhone)) {
      this.phone_Error = false;
    } else {
      var val = value.replace(/([^+0-9]+)/gi, '');
      this.phoneNumber = val;
      this.phone_Error = true;
      this.lang === "en" ? this.phoneError = "phone number is incorrect": this.lang = "fr" ? this.phoneError = "le numéro de téléphone est incorrect"
       : this.phoneError = "رقم الهاتف غير صحيح";
    }
  }
  checkPorfileOtp(){
    ////console.log("the toogle otp is :", this.toogleOtp);
    let flag = $("#phoneNum").intlTelInput("getSelectedCountryData");
    let list = {
      otp: this.otpNumber,
      token:this._conf.getItem('authToken'),
      phoneNumber: "+" + flag.dialCode + this.phoneNumber
    }
    ////console.log("the verifying code is:",list);
    this.homeService.verifyOtp(list)
      .subscribe((res) => {
        ////console.log("the verifying code response", res)
        if(res.code == 200){
          (this.lang == 'en') ? 
          this.errorMsg ="Otp verified and Phone Number Updated Successfully": 
          this.lang === "fr" ? this.errorMsg ="Otp vérifié et numéro de téléphone mis à jour avec succès" : 
          this.errorMsg = "تم التحقق من رقم هاتف Otp بنجاح"
          setTimeout(()=>{
            this.router.navigate([this.lang,'settings']);
          },4000);

        }
        else{
          this.lang === "en" ? 
          this.errorMsg =  'Enter Otp Again': this.lang = "fr" ? this.errorMsg = "Entrez à nouveau dans Otp" : this.errorMsg = "أدخل Otp مرة أخرى"
        }
      },error =>{
        let ErrorRes = error;
        this.errorMsg = ErrorRes.message;
      });
  }
}
