import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptpVerificationComponent } from './optp-verification.component';

describe('OptpVerificationComponent', () => {
  let component: OptpVerificationComponent;
  let fixture: ComponentFixture<OptpVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptpVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptpVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
