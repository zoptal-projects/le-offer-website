import { Component, OnInit, AfterViewInit, EventEmitter } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { ItemService } from './item.service';
import { HomeService } from '../home/home.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Http, Response, Headers } from '@angular/http';
import { Cloudinary } from '@cloudinary/angular-5.x';
import{ Location } from '@angular/common';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import * as  cloudinary1 from 'cloudinary';
import { Ng2ImgMaxService } from 'ng2-img-max';
declare var $: any;
declare var google: any;
declare var FB: any;
declare var moment: any;


declare interface Window {
  adsbygoogle: any[];
}
declare var adsbygoogle: any[];

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  cloudinaryData:any;
  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private location:Location,
    private _conf: Configuration,
    private route: ActivatedRoute,
    public _router: Router,
    private homeService: HomeService,
    private _service: ItemService,
    private _http: Http,
    private Cloudinary: Cloudinary,
    private imgCompressService: Ng2ImgMaxService,
    private _lang: LanguageService) {
      // this.homeService.getCloudinaryData(this._conf.getItem('authToken')).subscribe((res)=>{
      //   ////console.log("the home cloudinary data", res);
      //   this.cloudinaryData = res.response;
      // });
      cloudinary1.config({
        cloud_name: this._conf.cloudinaryData.cloud_name,
        api_key:this._conf.cloudinaryData.api_key,
        api_secret:this._conf.cloudinaryData.api_secret,
        upload_preset:this._conf.cloudinaryData.upload_preset
      });
      this.route.params.subscribe((res)=>{
          Object.keys(res).length <= 2 ? this.location.replaceState(`${this.languageCode}/${res['name']}/${res['id']}`) :'';
      })
     }
  postPrice:any;
  headerOpen: string;
  subCat: any;
  catSubFilter: any;
  subCategory: any;
  category: any;
  postId: any;
  itemList: any = [];
  allListYelo = false;
  token: any = '';
  selectedIndex:any;
  categoryId:any;
  currentUrlLocation:string = '';
  ShowCopyUrl: boolean = false;
  followers: any;
  followOpen = false;
  loginUsername: any;
  otherOffer: any;
  offerListLength: any;
  reportList: any;
  reportReasonList: any;
  reportDes: any;
  reportDescription: any;
  reportErr: any;
  offset = 40;
  limit = 0;
  willing = 0;
  makeOfferPrice: string;
  offerSent = false;
  lengthTextarea: any;
  listComments = false;
  commentMsg: any;
  textAreaMsg: any;
  allComment = false;
  sellEmail: string;
  sellPhone: string;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  succesSeller = false;
  howPopupSuccess = false;
  postListImg = false;
  listSell: any;
  item: any
  successReport: any;
  postEdit: any;
  catList: any;

  loaderButton = false;
  errMsg: any;
  languageCode:any;
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    // cloudName: this.cloudinaryData.cloudName,
    uploadPreset: 'iirvrtog',
    autoUpload: true
  });
  newLineDesc:any =[];
  // uploader: CloudinaryUploader = new CloudinaryUploader(this.cloudinaryOptions);
  uploader: FileUploader;

  imageUploader = (item: any, response: string, status: number, headers: any): any => {
    let cloudinaryImage = JSON.parse(response);
    ////console.log("cloudinaryImage",cloudinaryImage)
    let list = {
      img: cloudinaryImage.secure_url,
      cloudinaryPublicId: cloudinaryImage.public_id,
      containerHeight: cloudinaryImage.height,
      containerWidth: cloudinaryImage.width
    }
    this.ImageLoader = false;
    this.submitData(list);
  };

  ngOnInit() {
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.item = this._lang.engItem1;
                this.languageCode= 'fr';
                break;
        case 2: this.item = this._lang.engItem;
                this.languageCode= 'en';
                break;
        case 3: this.item = this._lang.engItem2;
                this.languageCode= 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.item = this._lang.engItem;
      this.languageCode = 'en';
    }

    $(window).on('popstate', function () {
      $('.modal').modal('hide');
    });
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    this.loginUsername = this._conf.getItem('username');

    this.route.params.subscribe(params => {
      if (params['id'] == '1510560388842') {
        var script = document.createElement("script");
        script.src = "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/js/rating.json";
        script.type = "application/ld+json";
        document.body.appendChild(script);
      }
      this.postId = params['id'];
      if (this.postId && !this.token) {
        let Id = {
          postId: this.postId,
          languageCode: this.languageCode
        }
        this._service.guestPostList(Id)
          .subscribe((res) => {
            if(res.code === 200){
            this.allListYelo = true;
            this.itemList = res.data[0];
            this.itemList.description = this.splitDescription();
            //console.log("this.itemList",this.itemList)
            this._meta.addTag({ name: 'keywords', content: this.itemList.seoKeyword })
            this._meta.addTag({ name: 'description', content: this.itemList.seoDescription })
            this._meta.addTag({ name: 'title', content: this.itemList.seoTitle })
            this._meta.addTag({ name: 'og:keywords', content: this.itemList.seoKeyword })
            this._meta.addTag({ name: 'og:description', content: this.itemList.seoDescription })
            this._meta.addTag({ name: 'og:title', content: this.itemList.seoTitle })
            this._meta.addTag({ name: 'og:image', content: this.itemList.mainUrl })
            this.hashtagFunction();
            this.timeFunction();
            setTimeout(()=>{
              this.locationMap();
            },500);
            this.myOtherGuestOffers();
            if (res.code == 204) {
              this.postListImg = true;
            } else {
              this.postListImg = false;
            }
          }
          else{
            setTimeout(()=>{
              this._router.navigate(['']);
            },500)
          }
          });
    
      } else {
        let list = {
          token: this.token,
          postId: this.postId,
          latitude: "12.972442",
          longitude: "77.580643",
          city: "bengaluru",
          countrySname: "in",
          languageCode: this.languageCode
        }
        $.get("https://ipapi.co/json/", (res)=> {
         
          ////console.log(res)
          // let ipList = res.loc.split(",");
          list.latitude = res.latitude;
          list.longitude = res.longitude;
          list.city = res.city;
          list.countrySname = res.country;
        })
        this._service.userPostList(list)
          .subscribe((res) => {
            this.allListYelo = true;
            if (res.code == 200) {
              this.itemList = res.data[0];
              //console.log("this.itemList",this.itemList)
              this.itemList.description = this.splitDescription();
              this._meta.addTag({ name: 'keywords', content: this.itemList.seoKeyword })
              this._meta.addTag({ name: 'description', content: this.itemList.seoDescription })
              this._meta.addTag({ name: 'title', content: this.itemList.seoTitle })
              this._meta.addTag({ name: 'og:keywords', content: this.itemList.seoKeyword })
              this._meta.addTag({ name: 'og:description', content: this.itemList.seoDescription })
              this._meta.addTag({ name: 'og:title', content: this.itemList.seoTitle })
              this._meta.addTag({ name: 'og:image', content: this.itemList.mainUrl })
              this.hashtagFunction();
              this.timeFunction();
              if (this.itemList.commentData.length > 0) {
                this.commentTimeFunction();
              }
              setTimeout(()=>{
                this.locationMap();
              },500);
              this.getFollowers(this.itemList);
              this.myOtherUserOffers();
            }
            if (res.code == 204) {
              // this._router.navigate([this.languageCode]);
              this._router.navigate(['']);
              
            }
          });
      }
      // this._router.navigate([this.languageCode,params['name'],params['id']]);
      this.location.replaceState(`${this.languageCode}/${params['name']}/${params['id']}`);
    });
    this.listGetCategories();
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${
        this.Cloudinary.config().cloud_name
        }/auto/upload`,
    autoUpload: true,
    isHTML5: true,
    removeAfterUpload: true,
    headers: [{ name: 'X-Requested-With', value: 'XMLHttpRequest' },],
      // }
    };
    this.uploader = new FileUploader(uploaderOptions);
  }
  splitDescription(){
    let t = this.itemList.description;
    if(t !== null){
    if(t.indexOf('\n') > 0){
      this.newLineDesc = t.split('\n');
      this.itemList.description  = t;
    }
  }
   return t;
  }

  commentTimeFunction() {
    var len = this.itemList.commentData;
    for (var i = 0; i < len.length; i++) {
      var post = this.itemList.commentData[i].commentedOn;
      var poston = Number(post);
      var postdate = new Date(poston);
      var currentdate = new Date();
      var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
      var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);

      if (1 < diffDays) {
        this.itemList.commentData[i].commentedOn = diffDays + "d";
      } else if (1 <= diffHrs) {
        this.itemList.commentData[i].commentedOn = diffHrs + "h";
      } else {
        this.itemList.commentData[i].commentedOn = diffMins + "m";
      }
    }
  }

  timeFunction() {
    var post = this.itemList.postedOn;
    var poston = Number(post);
    var postdate = new Date(poston);
    var currentdate = new Date();
    var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
    var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
    var Ago =''
    if (1 < diffDays) {
      (this.languageCode == "en") ? Ago = "Days" : 
      this.languageCode == "fr" ? Ago = "Journées" : Ago = "أيام";
      var postedOn = diffDays+" "+Ago;
    } else if (1 <= diffHrs) {
      (this.languageCode == "en") ? Ago = "Hours" : 
      this.languageCode == "fr" ? Ago = "Heures" : Ago = "ساعات";
      var postedOn = diffHrs +" "+Ago;
    } else {
      this.languageCode == "ar" ? Ago = "الدقائق" : Ago = " minutes";
      var postedOn = diffMins + Ago;
    }
    this.itemList.postedOn = postedOn;
  }

  public currencyList = [
    // { value: "ARP", text: "Argentina Pesos" },
    // { value: "ATS", text: "Austria Schillings" },
    // { value: "AUD", text: "Australia Dollars" },
    // { value: "BSD", text: "Bahamas Dollars" },
    // { value: "BBD", text: "Barbados Dollars" },
    // { value: "BEF", text: "Belgium Francs" },
    // { value: "BMD", text: "Bermuda Dollars" },
    // { value: "BRR", text: "Brazil Real" },
    // { value: "BGL", text: "Bulgaria Lev" },
    // { value: "CAD", text: "Canada Dollars" },
    // { value: "CHF", text: "Switzerland Francs" },
    // { value: "CLP", text: "Chile Pesos" },
    // { value: "CNY", text: "China Yuan Renmimbi" },
    // { value: "CYP", text: "Cyprus Pounds" },
    // { value: "CSK", text: "Czech Republic Koruna" },
    // { value: "DEM", text: "Germany Deutsche Marks" },
    // { value: "DKK", text: "Denmark Kroner" },
    // { value: "DZD", text: "Algeria Dinars" },
    { value: 'DH' , text: 'Dirham'},
    // { value: "EGP", text: "Egypt Pounds" },
    // { value: "ESP", text: "Spain Pesetas" },
    // { value: "EUR", text: "Euro" },
    // { value: "FJD", text: "Fiji Dollars" },
    // { value: "FIM", text: "Finland Markka" },
    // { value: "FRF", text: "France Francs" },
    // { value: "GRD", text: "Greece Drachmas" },
    // { value: "HKD", text: "Hong Kong Dollars" },
    // { value: "HUF", text: "Hungary Forint" },
    // { value: "ISK", text: "Iceland Krona" },
    // { value: "INR", text: "India Rupees" },
    // { value: "IDR", text: "Indonesia Rupiah" },
    // { value: "IEP", text: "Ireland Punt" },
    // { value: "ILS", text: "Israel New Shekels" },
    // { value: "ITL", text: "Italy Lira" },
    // { value: "JMD", text: "Jamaica Dollars" },
    // { value: "JPY", text: "Japan Yen" },
    // { value: "JOD", text: "Jordan Dinar" },
    // { value: "KRW", text: "Korea(South) Won" },
    // { value: "LBP", text: "Lebanon Pounds" },
    // { value: "LUF", text: "Luxembourg Francs" },
    // { value: "MYR", text: "Malaysia Ringgit" },
    // { value: 'MAD', text: 'Moroccan dirham'},
    // { value: "MXP", text: "Mexico Pesos" },
    // { value: "NLG", text: "Netherlands Guilders" },
    // { value: "NZD", text: "New Zealand Dollars" },
    // { value: "NOK", text: "Norway Kroner" },
    // { value: "PKR", text: "Pakistan Rupees" },
    // { value: "PHP", text: "Philippines Pesos" },
    // { value: "PLZ", text: "Poland Zloty" },
    // { value: "PTE", text: "Portugal Escudo" },
    // { value: "ROL", text: "Romania Leu" },
    // { value: "RUR", text: "Russia Rubles" },
    // { value: "SAR", text: "Saudi Arabia Riyal" },
    // { value: "SGD", text: "Singapore Dollars" },
    // { value: "SKK", text: "Slovakia Koruna" },
    // { value: "SDD", text: "Sudan Dinar" },
    // { value: "SEK", text: "Sweden Krona" },
    // { value: "TWD", text: "Taiwan Dollars" },
    // { value: "THB", text: "Thailand Baht" },
    // { value: "TTD", text: "Trinidad and Tobago Dollars" },
    // { value: "TRL", text: "Turkey Lira" },
    // { value: "USD", text: "United States Dollars" },
    // { value: "VEB", text: "Venezuela Bolivar" },
    // { value: "ZMK", text: "Zambia Kwacha" },
    // { value: "XAG", text: "Silver Ounces" },
    // { value: "XAU", text: "Gold Ounces" },
    // { value: "XCD", text: "Eastern Caribbean Dollars" },
    // { value: "XDR", text: "Special Drawing Right(IMF)" },
    // { value: "XPD", text: "Palladium Ounces" },
    // { value: "XPT", text: "Platinum Ounces" },
    // { value: "ZAR", text: "South Africa Rand" },
    // { value: 'RSD', text:'Serbian dinar'}
  ]


  myOtherGuestOffers() {
    let list = {
      postId: this.postId,
      membername: this.itemList.membername,
      latitude: this.itemList.latitude,
      longitude: this.itemList.longitude,
      offset: this.offset,
      limit: this.limit
    }

    this._service.otherOffersGuestPostList(list)
      .subscribe((res) => {
        this.otherOffer = res.data;
        if (this.otherOffer && this.otherOffer.length > 0) {
          this.offerListLength = true;
        } else {
          this.offerListLength = false;
        }
      });
  }


  myOtherUserOffers() {
    let list = {
      token: this.token,
      postId: this.postId,
      membername: this.itemList.membername,
      latitude: this.itemList.latitude,
      longitude: this.itemList.longitude,
      offset: this.offset,
      limit: this.limit
    }

    this._service.otherOffersUserPostList(list)
      .subscribe((res) => {
        this.otherOffer = res.data;
        if (this.otherOffer && this.otherOffer.length > 0) {
          this.offerListLength = true;
        } else {
          this.offerListLength = false;
        }
      });
  }

  watchList(val, label) {
    if (this.token) {
      let list = {
        token: this.token,
        postId: this.postId,
        label: 'Photo'
      }
      if (label == 1) {
        list.label = 'Video';
      }
      if (val == 0) {
        this.itemList.likeStatus = 1;
        this._service.likePost(list)
          .subscribe((res) => {
            res && res.data && res.code == 200 ? this.itemList.likes = res.data[0].likes  :'';
          });
      } else {
        this.itemList.likeStatus = 0;
        this._service.unlikePost(list)
          .subscribe((res) => {
            res && res.data && res.code == 200 ? this.itemList.likes = res.data[0].likes  :'';
          });
      }
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }
  itemDetails(itemList) {
    $('html, body').animate({scrollTop:0}, 500);
    this._router.navigate(["./",this.languageCode,itemList.productName.split(' ').join('-'), itemList.postId]);
  }

  postTextArea(val) {
    !this.token && this.itemList.membername !== this.loginUsername || this.itemList.membername === this.loginUsername ? '':
    this.lengthTextarea = val.length  ;
  }


  changeImage(val) {
    $(".imgItem").removeClass("active");
    $(".item").removeClass("active");
    $("#" + val + "img").addClass("active");
    $("#" + val).addClass("active");
  }

  InnerCarousel() {
    setTimeout(() => {
      var idx = $('#myCarousel').find('.active').attr("id");
      $(".imgItem").removeClass("active");
      $("#" + idx + "img").addClass("active");
    }, 650);
  }

  getFollowers(name) {
    let list = {
      token: this.token,
      membername: name.membername,
      offset: 0,
      limit: 40
    }
    this._service.getFollowersPostList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.followers = res.memberFollowers;
          if (this.followers && this.followers.length > 0) {
            this.followOpen = true;
          } else {
            this.followOpen = false;
          }
        } else {
          this.followOpen = false;
        }
      });

  }

  memberProfile(val) {
    // let memberName = val.username;
    $(".modal").modal("hide");
    if (this.token) {
      if (this.loginUsername != val) {
        this._router.navigate([this.languageCode,"p", val]);
      } else {
        this._router.navigate([this.languageCode,"settings"]);
      }
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  followerButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        userNameToFollow: val
      }
      if (i == undefined) {
        this.itemList.followRequestStatus = 1;
      } else {
        this.followers[i].userFollowRequestStatus = 1;
      }
      this._service.followersPostList(list)
        .subscribe((res) => {
          if (res.code == 200) {
            $("#followItemPopup").modal("show");
          }
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }

  unFollowButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        unfollowUserName: val
      }
      if (i == undefined) {
        this.itemList.followRequestStatus = null;
      } else {
        this.followers[i].userFollowRequestStatus = null;
      }
      this._service.unfollowPostList(list)
        .subscribe((res) => {

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }

  makeOfferPriceEmpty() {
    this.makeOfferPrice = "";
    this.offerSent = false;
  }

  makeOffer(item) {
    let mqttId = this._conf.getItem('mqttId');
    let username = this._conf.getItem('username');
    let profilePicUrl = this._conf.getItem('profilePicUrl');

    if (!this.makeOfferPrice) {
      this.makeOfferPrice = $(".makeOfferPrice").val();
    }

    if (this.makeOfferPrice) {
      if (this.token && mqttId) {
        var list = {
          token: this.token,
          postId: this.postId,
          offerStatus: '1',
          price: this.makeOfferPrice.replace(',','.'),
          type: String(item.postType),
          membername: item.membername,
          sendchat: null
        }
        var today = new Date();
        var formattedtoday = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var date = moment(formattedtoday).valueOf();

        let itemList = {
          dataSize: "1",
          from: mqttId,
          id: String(date),
          isSold: "0",
          name: username,
          offerType: "1",
          payload: btoa(this.makeOfferPrice),
          productId: String(item.postId),
          productImage: item.mainUrl,
          productName: item.productName,
          productPrice: item.price,
          secretId: String(item.postId),
          thumbnail: item.thumbnailImageUrl,
          to: item.memberMqttId,
          toDocId: String(date),
          type: String(15),
          userImage: profilePicUrl
        }
        list.sendchat = itemList;
        // ////console.log(list);
        this._service.makeOfferPostList(list)
          .subscribe((res) => {
            if (res.code == 200) {
              this.offerSent = true;
              $("#makeOffer").modal("hide");
              this.languageCode == 'en' ? this.successReport = "Offer Sent" : 
              this.languageCode == "fr" ? this.successReport = "Offre envoyée" :
              this.successReport = "تم إرسال العرض";
              $("#reportItemPopup").modal("show");
              setTimeout(() => {
                $(".modal").modal("hide");
              }, 2000);
            }
          });
      } else {
        $("#makeOffer").modal("hide");
        this._missionService.confirmLogin(this.headerOpen);
      }
    }
  }

  reportItem() {
    if (this.token) {
      let list = {
        token: this.token
      }
      this._service.reportPostList(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.reportList = res.data;
            this.reportDes = true;
            this.languageCode == 'en'? this.successReport = "Reported" :
            this.languageCode == "fr" ?  this.successReport = "Signalé" : this.successReport = "ذكرت";
            $("#reportItemPopup").modal("show");
          }
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
      // this._missionService.confirmLogin(this.headerOpen);
    }
  }

  reportReason(report) {
    this.reportDescription = "";
    this.reportErr = "";
    this.reportDes = false;
    this.reportReasonList = report;
  }

  reportPostItem() {
    let list = {
      token: this.token,
      membername: this.itemList.membername,
      reasonId: this.reportReasonList._id,
      description: this.reportDescription,
      postId: this.postId
    }
    this._service.reportPosting(list)
      .subscribe((res) => {
        this.reportDes = false;
        this.reportList = false;
        this.reportErr = res.messsage;
        if (res.code == 200) {
          setTimeout(() => {
            $(".modal").modal("hide");
          }, 1000);
        }
      });

  }

  commentsList(val) {
    $(".listTap").removeClass("active");
    $(".commentDesc").removeClass("active");
    $("#" + val + "com").addClass("active");
    $("#" + val + "coms").addClass("active");
    $("#" + val + val + "com").addClass("active");
    $("#" + val + val + val + "com").addClass("active");
    if (val == 1) {
      this.listComments = false;
    } else {
      this.listComments = true;
    }
  }

  allCommentsList() {
    if (this.token) {
      let list = {
        token: this.token,
        postId: this.itemList.postId
      }
      this._service.allCommentPost(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.allComment = true;
            this.itemList.commentData = res.result;
            this.commentTimeFunction();
            this.hashtagFunction();
          }
        });

    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }


  postCommentSend(type, id) {
    if (this.token) {
      let list = {
        token: this.token,
        postId: id,
        type: type.toString(),
        comment: this.textAreaMsg
      }
      // this.commentMsg.push(list);
      this.textAreaMsg = "";
      this.lengthTextarea = false;
      this._service.commentPost(list)
        .subscribe((res) => {
          this.commentMsg = res.data;
          this.itemList.totalComments += 1;
          this.commentMsg[0].commentData[0].commentedOn = "0m";
          this.itemList.commentData.push(this.commentMsg[0].commentData[0]);
          this.hashtagFunction();
        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }
  }

  hashtagFunction() {
    if (this.itemList.commentData) {
      var len = this.itemList.commentData.length;
      for (var i = 0; i < len; i++) {
        if (this.itemList.commentData) {
          var commenttext = this.itemList.commentData;
          // commenttext.splice(x, 0);
          if (this.itemList.commentData[i].commentBody) {
            var postCommentNodeArr1 = commenttext[i].commentBody;
            // //////console.log(postCommentNodeArr1);
            var hashtag_regexp = /#([a-zA-Z0-9_]+)/g;
            var comment_text = postCommentNodeArr1.replace(
              hashtag_regexp,
              '<a href="p/$1" class="color">#$1</a>'
            );
            var hashtag_regexp1 = /@([a-zA-Z0-9_]+)/g;
            var comment_textt = comment_text.replace(
              hashtag_regexp1,
              '<a href="p/$1" class="color">@$1</a>'
            );
            this.itemList.commentData[i].commentBody = comment_textt;
          }
        }
      }
    }
  }

  deleteComment(item, i, type, id) {
    let list = {
      token: this.token,
      commentedByUser: item.commentedByUser,
      commentBody: item.commentBody,
      commentId: item.commentId,
      type: type.toString(),
      postId: id
    }
    this.itemList.commentData.splice(i, 1);
    this._service.deleteCommentPost(list)
      .subscribe((res) => {

      });
  }

  emailValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.email_Error = false;
        this.registerSave = true;
      } else {
        this.email_Error = true;
        this.registerSave = false;
      }
    } else {
      this.email_Error = false;
    }
  }

  mobileValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      if (value.match(regexPhone)) {
        this.phone_Error = false;
        this.registerSave = true;
      } else {
        var val = value.replace(/([^+0-9]+)/gi, '')
        this.sellPhone = val;
        this.phone_Error = true;
        this.registerSave = false;
      }
      if (value.length == 10) {
        this.phone_Error = true;
        this.registerSave = false;
      }
    } else {
      this.phone_Error = false;
    }
  }

  sellHowSend() {
    ////console.log(this.sellEmail, this.sellPhone);
    if (this.sellEmail && this.sellPhone) {
      this.howPopupSuccess = false;
      this.phone_Error = false;
      this.email_Error = false;
      this.PEerror = "Please try anyone";
      setTimeout(() => {
        this.PEerror = false;
      }, 3000);
    } else if (this.sellEmail || this.sellPhone) {
      if (this.sellEmail) {
        this.sellSend(1);
      } else {
        this.sellSend(2);
      }
      this.PEerror = false;
    }
  }

  emptySell() {
    this.sellPhone = "";
    this.sellEmail = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.sellEmail
      }
    } else {
      this.listSell = {
        type: "2",
        phoneNumber: this.sellPhone
      }
    }
    // ////console.log(this.listSell);
    this.homeService.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.howPopupSuccess = true;
          this.emptySell();
        } else {
          this.PEerror = res.message;
        }
      });
  }
  imgUrl = [];
  editPost(list) {
    this.imgUrl = [];
    this.postEdit = list;
    ////console.log("the post edit is:", list, this.postEdit)
    let imgUrl = [
      { img: this.postEdit.mainUrl, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight, containerWidth: this.postEdit.containerWidth },
      { img: this.postEdit.imageUrl1, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight1, containerWidth: this.postEdit.containerWidth1 },
      { img: this.postEdit.imageUrl2, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight2, containerWidth: this.postEdit.containerWidth2 },
      { img: this.postEdit.imageUrl3, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight3, containerWidth: this.postEdit.containerWidth3 },
      { img: this.postEdit.imageUrl4, cloudinaryPublicId: this.postEdit.cloudinaryPublicId, containerHeight: this.postEdit.containerHeight4, containerWidth: this.postEdit.containerWidth4 }
    ]
    for (var i = 0; i < imgUrl.length; i++) {
      if (imgUrl[i].img) {
        this.imgUrl.push(imgUrl[i]);
      }
    }
    if (list.isSwap == 1) {
      this.willing = 1;
      list.swapPost.forEach((element,index)=>{
          if(element.swapTitle != null){
            this.swapArr = list.swapPost;
          }
      })
    }
    if(this.postEdit.category.length > 0)
    {
      setTimeout(()=>{
        this.selectedIndex = this.catList.findIndex((item) => item.categoryNodeId === this.postEdit.categoryNodeId);
        this.selectCat(this.selectedIndex)
      },100)
      ////console.log("the selected category is:,", this.catList, this.selectedIndex,this.subCategory)
    }

    $(".sellModals").modal("show");
  }

  listGetCategories() {
    this.homeService.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

  selectCurrency(val) {
    this.postEdit.currency = val;
  }

  selectCondition(val) {
    this.postEdit.condition = val;
  }
  subSelectIndex:any = -1;
  selectCat(val) {
    if(val > -1){
    ////console.log("the post filter is:", this.postEdit.postFilter);
    let subCatFiler = this.postEdit.postFilter;
    this.category = this.catList[val].name;
    this.categoryId = this.catList[val].categoryNodeId;
    let value ='';
    this.subCat = "";
    this.catSubFilter = "";
    if(this.postEdit.subCategory){
      this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryId)
      .subscribe((res) => {
        this.subCat = res.data;
        if(this.postEdit.subCategoryNodeId){
          this.subCat.forEach((item,index) =>{
            if(item.subCategoryNodeId === this.postEdit.subCategoryNodeId){
              this.subCategory = this.postEdit.subCategory;
              this.subSelectIndex = index;
            }
          });
          if(this.subSelectIndex != -1){
            this.selectSubCat(this.subSelectIndex)
          }
        }
        else{
          this.subSelectIndex = -1;
        }
      },error =>{
        ////console.log("the error is:", error)
      })
    }
    else{
      this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryId)
      .subscribe((res) => {
        this.subCat = res.data;
      },error =>{
        ////console.log("the error is:", error)
      });
    }
  }
  }

 subcaegoryId:any;
  selectSubCat(i) {
    this.subCategory = this.subCat[i].subCategoryName;
    this.subcaegoryId = this.subCat[i].subCategoryNodeId;
    var value = [];
    this.catSubFilter =[];
    let filteredData = [];
    ////console.log("heselect subcat filter",this.subCat[i].filter )
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        let values = x.values.split(",");
        values.forEach((item)=>{
          filteredData.push({type:x.type, value:item, checked:false});
        });
        x.filterData = filteredData;
        x.data = '';
        x.checked = false;
        value.push(x);
        filteredData =[];
      }
      else{
        x.data = '';
        x.checked = false;
        value.push(x);
      }
    });
      this.catSubFilter = value;
      // //console.log("this.catSubFilter",this.catSubFilter)
      this.postEdit.postFilter.forEach((postEdit,index) =>{
        let indx = this.catSubFilter.findIndex((item) => item.id == postEdit.fieldId);
        if(indx > -1){
          if(this.catSubFilter[indx].hasOwnProperty('filterData')){
            let postValues = postEdit.values.split(',');
          // if(postEdit.values.indexOf(',') == -1){
            if(postValues.length <= 1){
            let indx1 = this.catSubFilter[indx].filterData.findIndex((filterItem) => filterItem.value == postEdit.values);
            this.catSubFilter[indx].filterData[indx1].checked = true;
            this.catSubFilter[indx].checked = true;
            this.catSubFilter[indx].data = postEdit.values;            
          }
          else{
            let checkBoxesValues = postEdit.values.split(',');
            checkBoxesValues.forEach((checkValue,index)=>{
            this.catSubFilter[indx].filterData.forEach((element, index) => {
               if(postEdit.values.indexOf(element.value) > -1){
                this.catSubFilter[indx].filterData[index].checked = true;
               }
            });
            this.catSubFilter[indx].checked = true;
            this.catSubFilter[indx].data = postEdit.values;            
            })
          }
        }
        else{
          let rangeTextValues = postEdit.values.split(',');
            this.catSubFilter[indx].checked = true;
            this.catSubFilter[indx].data = postEdit.values;
          // }
        }
        }
      });
    ////console.log("value", this.catSubFilter)
  }

  checkedFilter = [];
  documentTextList(event, i, range) {
    // //console.log("this.catSubFilter[i]", this.catSubFilter[i],range)
    // //console.log("d", event.target.value.length,Object.keys(this.catSubFilter[i].data).length)
    ////console.log("the selected value is:",range );
    if (event.target.value.length > 0) {
      
       if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
        var index = -1;
        if (index > -1) {
          //  this.catSubFilter[i].data.splice(index, 1);
         
        } else {
          let list = {
            type: "equlTo",
            fieldName: this.catSubFilter[i].fieldName,
            value:{}
          }
          if( this.catSubFilter[i].type == 4   && Object.keys(this.catSubFilter[i].data).length >=  1){
            // this.catSubFilter[i].data[0].value = this.catSubFilter[i].filterData[range];
            this.catSubFilter[i].filterData.map((item,index)=>{
              index != range ? item.checked = false :'';
            });
            this.catSubFilter[i].filterData[range].checked = true;
            // this.catSubFilter[i].checked = false;
            this.catSubFilter[i]['data'] = this.catSubFilter[i].filterData[range].value;
          }
          else if(this.catSubFilter[i].type == 4  && Object.keys(this.catSubFilter[i].data).length == 0){
            this.catSubFilter[i].filterData[range].checked = true;
            list['value'] = this.catSubFilter[i].filterData[range].value;
            this.catSubFilter[i]['data'] = list['value'] ;
            this.catSubFilter[i].checked = true;
          }
          else if( this.catSubFilter[i].type == 6   && Object.keys(this.catSubFilter[i].data).length >=  1){
            this.catSubFilter[i].data = range;
          }
          else if(this.catSubFilter[i].type == 6  && Object.keys(this.catSubFilter[i].data).length == 0){
            list['value'] = range;
            this.catSubFilter[i].data = list['value'];
            this.catSubFilter[i].checked = true;
          }
          else if(this.catSubFilter[i].type == 2){
            this.catSubFilter[i].filterData.forEach((item,index)=>{
              if(item.checked){
                this.checkedFilter.push({
                  checked: true,
                  name: item.value
                })
              }
            })
            const checkbox = event.target as HTMLInputElement;
            if(checkbox.checked){
              this.checkedFilter.push({
                checked: checkbox.checked,
                name: event.target.name,
                value: event.target.value
              })
            }
            else{
              let index = this.checkedFilter.findIndex((x)=>x.name === event.target.name);
              if(index > -1){
                this.checkedFilter.splice(index,1);
              } 
            }
          }
          let finalValue = '';
          if( this.checkedFilter.length > 0  && this.catSubFilter[i].type == 2){
            this.checkedFilter.forEach((x,index)=> {
              (index != this.checkedFilter.length - 1)? finalValue += (x.value)+',' :
              finalValue += (x.value);
            });
            list['value'] = finalValue; 
            this.catSubFilter[i].data = finalValue;
            this.catSubFilter[i].checked = true;
          }
          else if(this.checkedFilter.length == 0 && this.catSubFilter[i].type == 2){
            this.catSubFilter[i].data = '';
            this.catSubFilter[i].checked = false;
            this.checkedFilter = [];
          }
        }
        // if (this.catSubFilter[i].data && this.catSubFilter[i].data.length >= 1) {
        //   this.catSubFilter[i].checked = true;
        //   this.catSubFilter[i].typed = 4;
        // } else {
        //   this.catSubFilter[i].checked = false;
        // }
      } 
      else {
        this.catSubFilter[i].data = event.target.value.replace(',','.');
        this.catSubFilter[i].checked = true;
        ( this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5 ) ?
        this.catSubFilter[i].types = "range" : this.catSubFilter[i].types = "equlTo";
      }
    } 
    else {
          this.catSubFilter[i].checked = false;
    }
    ////console.log("the filter data is:", this.catSubFilter)

  }
  switchToggle(val) {
    if (val == true) {
      this.postEdit.negotiable = 1;
    } else {
      this.postEdit.negotiable = 0;
    }
  }

  willingToggle(val) {
    if (val == true) {
      this.willing = 1;
    } else {
      this.willing = 0;
    }
  }

  swapList: any;
  swapSearch(val) {
    let list = {
      token: this.token,
      productName: val
    }
    if (val.length > 0) {
      this.homeService.searchSwap(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.swapList = res.data;
          } else {
            this.swapList = [];
          }
        })
    } else {
      this.swapList = [];
    }
  }

  swapArr = [];
  swapPost: string;
  postSwap(list) {
    let data = {
      swapDescription: list.description,
      swapPostId: list.postId,
      swapTitle: list.productName
    }
    this.swapArr.push(data);
  }

  cancelSwapList(i) {
    this.swapArr.splice(i, 1)
  }

  blurSwap() {
    setTimeout(() => {
      this.swapPost = "";
      this.swapList = [];
    }, 1000)
  }

  cancelSwap() {
    this.swapPost = "";
    this.swapArr = [];
    this.swapList = [];
  }

  submitData(url) {
    // ////console.log("uuuuuuuu", url);
    if (this.imgUrl && this.imgUrl.length < 5) {
      this.imgUrl.push(url);
      ////console.log("iiiiiii", this.imgUrl);
    }
  }

  removeImg(i) {
    this.imgUrl.splice(i, 1);
    ////console.log("rm", this.imgUrl);
  }

  previewUrl:any;
  ImageLoader:any;
  fileUploader(event:EventEmitter<File[]>) {
    this.uploader.clearQueue();
    const file: File = event[0];
    this.ImageLoader = true;
    this.imgCompressService.resizeImage(file, 600, 800).subscribe(
      result => {
        const newImage = new File([result], result.name);
        //console.log("result", result);
      this.uploader.onBeforeUploadItem = (fileItem: any): any => {
      //console.log("compress image result fileItem",fileItem )
      fileItem.withCredentials = false;
      fileItem.secure = true;
    }
    this.uploader.onAfterAddingFile = (fileItem:any) => {
      fileItem.withCredentials = false;
      fileItem.upload_preset = this.Cloudinary.config().upload_preset;
      this.uploader.cancelAll();

    };
    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      this.uploader.cancelAll();
      form.append('upload_preset', this.Cloudinary.config().upload_preset);
    };
    this.uploader.onSuccessItem  = this.imageUploader;
    this.uploader.addToQueue([newImage]);
    this.uploader.uploadAll();
    },
      error => console.log(error)
    );
  }


  updatePost(list) {
    let filData = {};
    let allowToPost = false;
    let catSubFilterCount = 0;
    let FilterFillData = {};
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true) {
          
          filData[x.id] = x.data;
          x.checked && x.isMandatory == 'true' ? FilterFillData[x.id] = x.data :'';
        }
        x.isMandatory == 'true' ? catSubFilterCount+=1 :'';
      })
    }
    let city = $("#cityP").val();
    let state = $("#stateP").val();
    let location = $("#locateP").val()
    let lat = $("#latP").val();
    let lang = $("#lngP").val();
    if(this.imgUrl.length > 0 && location && this.category ){
      if(this.subCat && this.subCat.length > 0 && this.subCategory){
         if(this.catSubFilter && this.catSubFilter.length > 0){
            // if(Object.keys(filData).length > 0 &&  Object.keys(filData).length ===  this.catSubFilter.length){
            if(Object.keys(FilterFillData).length > 0 &&  Object.keys(FilterFillData).length ===  catSubFilterCount){
             allowToPost = true;
            } 
            else{
              catSubFilterCount > 0 ? allowToPost = false : allowToPost = true;
            //  allowToPost = false;
            } 
         }
         else{
          this.catSubFilter  && this.catSubFilter.length > 0 ? allowToPost = true : allowToPost = true;
         } 
      }
      else{
       !this.subCat  ? allowToPost = true : allowToPost = false;
      } 
   }
   else{
     allowToPost = false;
   }

    // this.imgUrl.length > 0 && location && this.category  ? this.subCat && this.subCat.length > 0 && this.subCategory && Object.keys(filData).length > 0 && Object.keys(filData).length ===  this.catSubFilter.length ? allowToPost = true : allowToPost = false : allowToPost = true;
    if (allowToPost) {

      let lists = {
        token: this.token,
        type: "0",
        mainUrl: this.imgUrl[0].img,
        thumbnailImageUrl: this.imgUrl[0].img,
        imageCount: this.imgUrl.length,
        containerHeight: this.imgUrl[0].containerHeight,
        containerWidth: this.imgUrl[0].containerWidth,
        cloudinaryPublicId: this.imgUrl[0].cloudinaryPublicId,
        postId: list.postId,
        price: list.price.toString().indexOf(',') > -1 ? list.price.replace(',','.') : list.price,
        currency: list.currency || 'DH',
        productName: list.productName,
        description: list.description,
        condition: list.condition,
        negotiable: list.negotiable,
        category: this.category || list.category,
        categoryNodeId:this.categoryId,
        subCategory: this.subCategory,
        isSwap: this.willing,
        swapingPost: JSON.stringify(this.swapArr),
        filter: JSON.stringify(filData),
        location: location,
        latitude: lat,
        longitude: lang,
        city: city,
        countrySname: state,
        imageUrl1: "",
        cloudinaryPublicId1: "",
        imageUrl2: "",
        cloudinaryPublicId2: "",
        imageUrl3: "",
        cloudinaryPublicId3: "",
        imageUrl4: "",
        cloudinaryPublicId4: ""
      }
      this.subCategory ? lists['subCategoryNodeId'] = this.subcaegoryId: 
      lists['subCategoryNodeId'] ='';
      // lists['subCategoryNodeId']: this.subcaegoryId,
      if (this.imgUrl && this.imgUrl[1]) {
        lists.imageUrl1 = this.imgUrl[1].img;
        lists.cloudinaryPublicId1 = this.imgUrl[1].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[2]) {
        lists.imageUrl2 = this.imgUrl[2].img;
        lists.cloudinaryPublicId2 = this.imgUrl[2].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[3]) {
        lists.imageUrl3 = this.imgUrl[3].img;
        lists.cloudinaryPublicId3 = this.imgUrl[3].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[4]) {
        lists.imageUrl4 = this.imgUrl[4].img;
        lists.cloudinaryPublicId4 = this.imgUrl[4].cloudinaryPublicId;
      }
      this.loaderButton = true;
      setTimeout(() => {
        this.loaderButton = false;
        this.errMsg = false;
      }, 3000);
      // //console.log("filter lists", lists)
      this.homeService.postEditProduct(lists)
        .subscribe((res) => {
          this.loaderButton = false;
          if (res.code == 200) {
            this._router.navigate([this.languageCode,"settings"]);
            $(".modal").modal("hide");
          }
          else{
            this.errMsg = res.message;
          }
        }, err => {
          this.loaderButton = false;
          ////console.log(err._body)
          var error = JSON.parse(err._body);
          this.errMsg = error.message;
        });
    }

  }

  delPostId: any;
  deletePosts(list) {
    $("#deletePost").modal("show");
    this.delPostId = list.postId;
  }

  deletePost() {
    ////console.log("the delete post")
    this.homeService.postDeleteProduct(this.token, this.delPostId)
      .subscribe((res) => {
        if (res.code == 200) {
          $("#deletePost").modal("hide");
          this._router.navigate(["./settings"]);
        }
      },error =>{
        ////console.log("the error is fro delete post",error)
      })
  }


  changeLocation() {
    var input = document.getElementById('sellLocates');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
          }
          if (addressObj.types[j] === 'country') {
            var state = addressObj.short_name;
          }
        }
      }
      if (City) {
        $("#cityP").val(City);
      } else if (state) {
        $("#cityP").val(state);
      } else {
        $("#cityP").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#latP").val(lat);
      $("#lngP").val(lng);
      $("#stateP").val(state);
      $("#locateP").val(place.formatted_address);
    });
  }
  onFacebookLoginClick(): void {
    FB.ui({
      method: 'share',
      mobile_iframe: true,
      href: 'https://developers.facebook.com/docs/',
    }, function (response) {
      ////console.log(response);
    });
  }


  locationMap() {
    var myLatLng = { lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) };
    var mapCanvas = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 13,
      center: myLatLng,
      scrollwheel: false
    });

    setTimeout(() => {
      google.maps.event.trigger(mapCanvas, "resize");
      mapCanvas.setCenter({ lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) });
    }, 300);
    var markerIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 25,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1,
      radius:2000
    };
    // var cityCircle = new google.maps.Circle({
    //   fillColor: 'rgba(167, 217, 206, 0.81)',
    //   fillOpacity: .9,
    //   scale: 50,
    //   strokeColor: 'rgba(167, 217, 206, 0.81)',
    //   strokeWeight: 1,
    //   map: mapCanvas,
    //   center: myLatLng,
    //   radius:300
    //   // radius: Math.sqrt(citymap[city].population) * 100
    // });

  //   google.maps.event.addListener(mapCanvas, 'zoom_changed', function() {
  //     console.log("zoom changed")

  //     var pixelSizeAtZoom0 = 8; //the size of the icon at zoom level 0
  //     var maxPixelSize = 350; //restricts the maximum size of the icon, otherwise the browser will choke at higher zoom levels trying to scale an image to millions of pixels
  
  //     var zoom = mapCanvas.getZoom();
  //     var relativePixelSize = Math.round(pixelSizeAtZoom0*Math.pow(2,zoom)); // use 2 to the power of current zoom to calculate relative pixel size.  Base of exponent is 2 because relative size should double every time you zoom in
  
  //     if(relativePixelSize > maxPixelSize) //restrict the maximum size of the icon
  //         relativePixelSize = maxPixelSize;
  
  //     //change the size of the icon
  //     marker.setIcon(
  //         new google.maps.MarkerImage(
  //             marker.getIcon().url, //marker's same icon graphic
  //             null,//size
  //             null,//origin
  //             null, //anchor
  //             new google.maps.Size(relativePixelSize, relativePixelSize) //changes the scale
  //         )
  //     );        
  // });
    // var circle = new google.maps.Circle({
    //   map: mapCanvas,
    //   radius: 2000,  
    //   fillColor: 'rgba(167, 217, 206, 0.81)',
    //   fillOpacity: .9,
    //   scale: 25,
    //   strokeColor: 'rgba(167, 217, 206, 0.81)',
    //   strokeWeight: 1
    // });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: mapCanvas,
      icon: markerIcon
    });

    // var circle = new google.maps.Circle({
    //   map: mapCanvas,
    //   radius: 2000,  
    //   fillColor: 'rgba(167, 217, 206, 0.81)',
    //   fillOpacity: .9,
    //   scale: 25,
    //   strokeColor: 'rgba(167, 217, 206, 0.81)',
    //   strokeWeight: 1
    // });
    // circle.bindTo('center', marker, 'position');
  }

  popupLocation() {
    var myLatLng = { lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) };

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: myLatLng,
      scrollwheel: false
    });
    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter({ lat: parseFloat(this.itemList.latitude), lng: parseFloat(this.itemList.longitude) });
    }, 300);
    // var markerIcon = {
    //   path: google.maps.SymbolPath.CIRCLE,
    //   fillColor: 'rgba(167, 217, 206, 0.81)',
    //   fillOpacity: .9,
    //   scale: 50,
    //   strokeColor: 'rgba(167, 217, 206, 0.81)',
    //   strokeWeight: 1
    // };
    var cityCircle = new google.maps.Circle({
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 50,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1,
      map: map,
      center: myLatLng,
      radius:300
      // radius: Math.sqrt(citymap[city].population) * 100
    });
    // var marker = new google.maps.Marker({
    //   position: myLatLng,
    //   map: map,
    //   icon: cityCircle
    // });
    // var cityCircle = new google.maps.Circle({
    //   strokeColor: '#FF0000',
    //   strokeOpacity: 0.8,
    //   strokeWeight: 2,
    //   fillColor: '#FF0000',
    //   fillOpacity: 0.35,
    //   map: map,
    //   center: myLatLng,
    //   // radius: Math.sqrt(citymap[city].population) * 100
    // });
  //   google.maps.event.addListener(map, 'zoom_changed', function() {
  //     console.log("zoom changed"); 
  //     var pixelSizeAtZoom0 = 8; //the size of the icon at zoom level 0
  //   var maxPixelSize = 350; //restricts the maximum size of the icon, otherwise the browser will choke at higher zoom levels trying to scale an image to millions of pixels

  //   var zoom = map.getZoom();
  //   var relativePixelSize = Math.round(pixelSizeAtZoom0*Math.pow(2,zoom)); // use 2 to the power of current zoom to calculate relative pixel size.  Base of exponent is 2 because relative size should double every time you zoom in

  //   if(relativePixelSize > maxPixelSize) //restrict the maximum size of the icon
  //       relativePixelSize = maxPixelSize;

  //   //change the size of the icon
  //   marker.setIcon(
  //       new google.maps.MarkerImage(
  //           marker.getIcon().url, //marker's same icon graphic
  //           null,//size
  //           null,//origin
  //           null, //anchor
  //           new google.maps.Size(relativePixelSize, relativePixelSize) //changes the scale
  //       )
  //   );     
  //     // marker.setIcon(markerIcon[map.getZoom()]);
  //     // zoomLevel = map.getZoom();
  //     // if (zoomLevel >= minFTZoomLevel) {
  //     //     FTlayer.setMap(map);
  //     // } else {
  //     //     FTlayer.setMap(null);
  //     // }
  // });

  }

  filterToggle() {
    $("#sideDetailsId").toggleClass('active');
  }
  // function : copyUrl, developer: sowmya sv , desc: to show sharing the current url
  copyUrl(caseVal){
    switch(caseVal){
      case 0: this.ShowCopyUrl = true;
              this.currentUrlLocation = window.location.href;
              break;

      case 1: this.ShowCopyUrl = false;
              let  copyText = document.getElementById("shareUrl") as HTMLInputElement;
              copyText.select();
              let successfulCopy =  document.execCommand("copy");
              break;
    }
  }
  checkForLogin(){
    (this.token.length <= 0) ? ($("#loginModal").modal('show'),$("#askItemPopup").modal('hide')) : (
      $("#loginModal").modal('hide'),$("#askItemPopup").modal('show'));
  }
  // format price based on dirham currency (DH)
    getDirhamCurrencyPrice(list){
      let price;
      // if(list && list.price  &&list.price.length >= 4){
      list && list.price ? price = list.price.toFixed(2) : price = list.makeOfferPrice;
    // }
    // else{
    //   list && list.price ? price = list.price.toFixed(2) : price = list.makeOfferPrice;
    if(list.currency == 'DH' ){
      price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      price = price.replace('.',',') ; 
    }
    else if(list.currency == 'Dirham' ){
      price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      price = price.replace('.',',') ; 
    }
    else if(list.currency == 'dirham' ){
      price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      price = price.replace('.',',') ; 
    }
    else if(list.currency == 'dh' ){
      price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      price = price.replace('.',',') ; 
    }
    // }
      return price;

    }
  // for add review if user is not loggin then oprn login page

  openLoggin(){
    //console.log("clicke got loggin ")
    !this.token && this.itemList.membername !== this.loginUsername || this.itemList.membername === this.loginUsername ? $("#loginModal").modal('show'): $("#loginModal").modal('hide')  }
}
