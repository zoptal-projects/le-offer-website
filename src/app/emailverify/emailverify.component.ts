import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';;
import { MissionService } from '../app.service';
import { ChangePasswordService } from '../changepassword/changepassword.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';

declare var $ : any;
@Component({
  selector: 'emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.css']
})
export class EmailverifyComponent implements OnInit {

  constructor(
    private _missionService: MissionService, 
    private _conf: Configuration, 
    private _router: Router,
    private _service: ChangePasswordService,
    private route: ActivatedRoute,
    private location:Location,
    private _lang: LanguageService) { }

  headerClosed: string;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  registerErrMsg: any;
  email_Error = false;
  registerSave = false;
  token: any;
  email:any;
  loaderButton = false;
  emailVerify:any;
  languageCode:any;
  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    // this.emailVerify = this._lang.engEmailVerify;
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.emailVerify = this._lang.engEmailVerify1;
                this.languageCode ='fr';
                break;
        case 2: this.emailVerify = this._lang.engEmailVerify;
                this.languageCode = 'en';
                break;
        case 3: this.emailVerify = this._lang.engEmailVerify2;
                this.languageCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.emailVerify = this._lang.engEmailVerify;
      this.languageCode ='en';
    }
    this._missionService.confirmheaderClosed(this.headerClosed);
    this.token = this._conf.getItem('authToken');
    // this._router.navigate(["./",this.languageCode,'emailVerify']);
    this.location.replaceState(`${this.languageCode}/emailVerify`);
  }


  emailChange() {
    let list = {
      token: this.token,
      email:this.email
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 3000);
    this._service.verifyEmail(list)
      .subscribe((res) => {
        if (res.code == 200) {         
          this.registerErrMsg = false;
          this.languageCode === 'en' ? this.registerErrMsg = "success! check your mail" : this.languageCode === 'fr'  ? 
          this.registerErrMsg = "Succès! vérifier votre courrier" : this.registerErrMsg = "نجاح! راجع بريدك";
          setTimeout(() => {
            this.registerErrMsg = false;
            this._router.navigate([this.languageCode,"settings"]);
          }, 3000);          
        } else {
          this.loaderButton = false;          
          this.registerErrMsg = res.message;
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        }

      });
  }

  emailValidation(value) {
    var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

    if (regexEmail.test(value)) {
      this.email_Error = false;
      this.registerSave = true;
    } else {
      this.email_Error = true;
      this.registerSave = false;
    }
  }
}
