import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { MemberProfileService } from './memberprofile.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';

declare var $: any;
declare var google: any;
@Component({
  selector: 'memberprofile',
  templateUrl: './memberprofile.component.html',
  styleUrls: ['./memberprofile.component.css']
})
export class MemberprofileComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _conf: Configuration,
    private location:Location,
    private route: ActivatedRoute,
    private _router: Router,
    private _service: MemberProfileService,
    private _lang: LanguageService) { }
  headerOpen: string;
  membername: any;
  allListYelo = false;
  token: any;
  offset = 0;
  limit = 40;
  followers: any;
  followOpen = false;
  memberProfileData: any;
  memberList: any;
  memberListLength: any;
  loginUsername: any;
  memberProfileSub: any;
  languageCode:any;
  ngOnInit() {

    // this.memberProfileSub = this._lang.engMemberProfile;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.memberProfileSub = this._lang.engMemberProfile1;
                this.languageCode = 'fr'; 
                break;
        case 2: this.memberProfileSub = this._lang.engMemberProfile;
                this.languageCode = 'en'; 
                break;
        case 3: this.memberProfileSub = this._lang.engMemberProfile2;
                this.languageCode = 'ar'; 
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.memberProfileSub = this._lang.engMemberProfile;
      this.languageCode = 'en'; 
    }
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    this.loginUsername = this._conf.getItem('username');
    if (this.token) {
      this.route.params.subscribe(params => {
        console.log("params", params)
        this.membername = params['name'];
        let list = {
          token: this.token,
          membername: this.membername,
          offset: this.offset,
          limit: this.limit
        }
        this.allListYelo = false;
        this._service.memberProfileList(list)
          .subscribe((res) => {
            this.allListYelo = true;
            if (res.code == 200) {
              let memberList = res.memberPostsData;
              if (memberList && memberList.length > 0) {
                this.memberList = res.memberPostsData;
                this.memberListLength = true;
              } else {
                this.memberListLength = false;
              }
              this.memberProfileData = res.memberProfileData[0];
              if (this.memberProfileData.memberLatitude) {
                setTimeout(()=>{
                  this.locationMap();
                },500);
              }
            } else if (res.code == 206) {
              this.memberProfileData = res.memberProfileData[0];
              if (this.memberProfileData.memberLatitude) {
                setTimeout(()=>{
                  this.locationMap();
                },500);
              }
            } else {
              this.memberListLength = false;
            }
          }, (err) => {
            let error = JSON.parse(err._body);
            
            var listErr = error.code;
            if (listErr == 401) {
              this._conf.clear();
              this._missionService.confirmLogin(this.headerOpen);
            }
          });
        this.getFollowers();
      });
      this.location.replaceState(`${this.languageCode}/p/${this.membername}`);
    } else {
      // this._missionService.confirmLogin(this.headerOpen);
      this._router.navigate(['']);
    }
    // this._router.navigate([this.languageCode,"p", this.membername])
  }

  itemDetails(list) {
    let replace = list.productName.split(' ').join('-');
    this._router.navigate(["./",this.languageCode,replace, list.postId]);
  }
  memberProfile(val) {
    $(".modal").modal("hide");
    this._router.navigate([this.languageCode,"p", val]);
  }

  followerButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        userNameToFollow: val
      }
      if (i == undefined) {
        this.memberProfileData.userFollowRequestStatus = 1;
      } else {
        this.followers[i].userFollowRequestStatus = 1;
      }
      this._service.followersPostList(list)
        .subscribe((res) => {

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }

  unFollowButton(val, i) {
    if (this.token) {
      let list = {
        token: this.token,
        unfollowUserName: val
      }
      if (i == undefined) {
        this.memberProfileData.userFollowRequestStatus = null;
      } else {
        this.followers[i].userFollowRequestStatus = null;
      }
      this._service.unfollowPostList(list)
        .subscribe((res) => {

        });
    } else {
      this._missionService.confirmLogin(this.headerOpen);
    }

  }

  getFollowers() {
    let list = {
      token: this.token,
      membername: this.membername,
      offset: 0,
      limit: 40
    }
    this._service.getFollowersPostList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.followers = res.memberFollowers;
          if (this.followers && this.followers.length > 0) {
            this.followOpen = true;
          } else {
            this.followOpen = false;
          }
        } else {
          this.followOpen = false;
        }
      });

  }


  locationMap() {
    var myLatLng = { lat: parseFloat(this.memberProfileData.memberLatitude), lng: parseFloat(this.memberProfileData.memberLongitude) };
    var mapCanvas = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 15,
      center: myLatLng,
      scrollwheel: false
    });

    setTimeout(() => {
      google.maps.event.trigger(mapCanvas, "resize");
      mapCanvas.setCenter({ lat: parseFloat(this.memberProfileData.memberLatitude), lng: parseFloat(this.memberProfileData.memberLongitude) });
    }, 300);
    var markerIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 50,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1
    };
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: mapCanvas,
      icon: markerIcon
    });
  }
  getLcoationOfUser:any;
  popupLocation() {
    var myLatLng = { lat: parseFloat(this.memberProfileData.memberLatitude), lng: parseFloat(this.memberProfileData.memberLongitude) };

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: myLatLng,
      scrollwheel: false
    });
    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter({ lat: parseFloat(this.memberProfileData.memberLatitude), lng: parseFloat(this.memberProfileData.memberLongitude) });
    }, 300);

    var cityCircle = new google.maps.Circle({
      fillColor: 'rgba(167, 217, 206, 0.81)',
      fillOpacity: .9,
      scale: 50,
      strokeColor: 'rgba(167, 217, 206, 0.81)',
      strokeWeight: 1,
      map: map,
      center: myLatLng,
      radius:300
      // radius: Math.sqrt(citymap[city].population) * 100
    });
    // var markerIcon = {
    //   path: google.maps.SymbolPath.CIRCLE,
    //   fillColor: 'rgba(167, 217, 206, 0.81)',
    //   fillOpacity: .9,
    //   scale: 50,
    //   strokeColor: 'rgba(167, 217, 206, 0.81)',
    //   strokeWeight: 1
    // };
    // var marker = new google.maps.Marker({
    //   position: myLatLng,
    //   map: map,
    //   icon: markerIcon
    // });
    // card#247, Website - wrong Map title , fixed:sowmyaSv
    var latlng = new google.maps.LatLng(parseFloat(this.memberProfileData.memberLatitude), parseFloat(this.memberProfileData.memberLongitude));
    var geocoder = new google.maps.Geocoder();
    // google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng':latlng }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          console.log("results", results);
          if(results.length >= 4){
            this.getLcoationOfUser = results[3].formatted_address;
          }
          else{
            this.getLcoationOfUser = results[0].formatted_address;
          }
          // if (results[0]) {
          //   this.getLcoationOfUser = results[0].formatted_address;
          //   // $("#lati").val(marker.getPosition().lat());
          //   // $("#lngi").val(marker.getPosition().lng());
          //   // $(".location").text(results[0].formatted_address);
          //   // $("#cityAreai").val(results[0].formatted_address);
          //   // $(".addressArea").val(results[0].formatted_address);
          // }
        }
      });
    // });

  }
  // format price based on dirham currency (DH)
  getDirhamCurrencyPrice(list){
    let price = parseInt(list.price).toFixed(2);
    price = this._conf.getDirhamCurrencyPrice(list);
    // let price = list.price.toFixed(2);
    // if(list.currency == 'DH' || list.currency == 'Dirham'|| list.currency == 'dirham' || list.currency == 'dh'){
    //     price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //     price = price.replace('.',',') ; 
    // }
    // if(list.currency == 'DH' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'Dirham' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'dirham' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // else if(list.currency == 'dh' ){
    //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    //   price = price.replace('.',',') ; 
    // }
    // list.currency == 'DH' ? price.replace(',',' ').replace('.',',')  : '';
    return price;
  
  }
 

}
