import { Injectable, Inject } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import { Headers } from '@angular/http';
import { DOCUMENT } from '@angular/common';
declare var Buffer: any;
@Injectable()
export class Configuration {
    // Server: string = 'http://34.209.156.227:3000/';
    Server: string = 'https://le-offers.com/';
    ApiUrl: string = 'api/';
    Url = this.Server + this.ApiUrl;

    // authPassword = '&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb';
    authPassword = '&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb';
    auth = 'Basic ' + new Buffer("yelo:" + this.authPassword).toString('base64');
    // token = localStorage.getItem('user') || '';

    headers = new Headers({ 'Content-Type': 'application/json' });
    cloudinaryData = {
        cloud_name: 'annonzilla',
        api_key:'439258344394228',
        api_secret:'mwONZMAiLT0WE5LvksAS6lfa7A8',
        upload_preset:'oyd5r4as'
    }
    constructor(@Inject (DOCUMENT) private doc:Document) {
        // this.headers.append('token', this.token);
        this.headers.append('authorization', this.auth);
    }

   getDocument(){
        return this.doc;
   }
    setItem(item, val) {
        // try {
        //     localStorage.setItem(item, val);
        // } catch (exception) {
        // this[item] = val;      
        Cookie.set(item, val, 1, "/");
        // }
    }
    getItem(item) {
        // try {
        //     return localStorage.getItem(item);
        // } catch (exception) {
        // return this[item];
        return Cookie.get(item);
        // }
    }

    removeItem(item) {
        // try {
        //     localStorage.removeItem(item);
        // } catch (exception) {
        // return this[item];
        Cookie.delete(item, "/");
        // }
    }

    clear() {
        // try {
        //     localStorage.clear();
        // } catch (exception) {
        //  Cookie.clear();
        Cookie.deleteAll("/");
        // }
    }
    getDirhamCurrencyPrice(list){
        let price = list.price.toFixed(2);
        if(list.currency == 'DH' ){
          price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
        else if(list.currency == 'Dirham' ){
          price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
        else if(list.currency == 'dirham' ){
          price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
        else if(list.currency == 'dh' ){
          price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
        price = price.replace('.',',') ; 
        // list.currency == 'DH' ? price.replace(',',' ').replace('.',',')  : '';
        return price;
      
    }

}