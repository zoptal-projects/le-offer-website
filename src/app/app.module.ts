import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { FileUploadModule } from 'ng2-file-upload';
import { AppComponent } from './app.component';
import { Configuration } from './app.constants';
import { MissionService } from './app.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { HomeService } from './home/home.service';
import { AboutComponent } from './about/about.component';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { TrustComponent } from './trust/trust.component';
import { ItemComponent } from './item/item.component';
import { ItemService } from './item/item.service';;
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { RegisterComponent } from './register/register.component';
import { RegisterService } from './register/register.service';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SettingsComponent } from './settings/settings.component';
import { SettingsService } from './settings/settings.service';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ChangePasswordService } from './changepassword/changepassword.service';
import { NeedhelpComponent } from './needhelp/needhelp.component';
import { LogoutComponent } from './logout/logout.component';
import { MemberprofileComponent } from './memberprofile/memberprofile.component';
import { MemberProfileService } from './memberprofile/memberprofile.service';
import { TermsComponent } from './terms/terms.component';
import { TermsService } from './terms/terms.service';
import { PrivacyComponent } from './privacy/privacy.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { VerifyemailComponent } from './verifyemail/verifyemail.component';
import { EmailverifyComponent } from './emailverify/emailverify.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LanguageService } from './app.language';
import { AdsenseModule } from 'ng2-adsense';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CategoryComponent } from './category/category.component';
import { FiltersComponent } from './filters/filters.component';
import { LayoutModule } from '@angular/cdk/layout';
// import {
//   SocialLoginModule,
//   AuthServiceConfig,
//   GoogleLoginProvider,
//   FacebookLoginProvider,
// } from "ngx-angular-social-login";
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider,SocialLoginModule  } from 'angularx-social-login';
import { OptpVerificationComponent } from './optp-verification/optp-verification.component';
import { CloudinaryModule, CloudinaryConfiguration,provideCloudinary } from '@cloudinary/angular-5.x';
// import { Cloudinary } from 'cloudinary-core';
// import * as cloudinary from 'cloudinary-core';

import { Cloudinary } from 'cloudinary-core';
import { SearchproductsComponent } from './searchproducts/searchproducts.component';
// import { LayoutModule } from '@angular/cdk/layout';
import { Ng2ImgMaxModule } from 'ng2-img-max';
// import { NgxMasonryModule } from 'ngx-masonry';

export const cloudinaryLib = {
  Cloudinary: Cloudinary
};



const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('527165199246-n9nd3rrap60l01joh9b4d4tvhupa94hs.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('1686026238176818')
  },
  // {
  //   id: LinkedInLoginProvider.PROVIDER_ID,
  //   provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
  // }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,  
    HowitworksComponent,
    TrustComponent,
    ItemComponent, 
    LoginComponent,
    RegisterComponent,
    ResetpasswordComponent,    
    SettingsComponent,
    ChangepasswordComponent,
    NeedhelpComponent,
    LogoutComponent,
    MemberprofileComponent,
    TermsComponent,
    PrivacyComponent,     
    PasswordresetComponent,
    VerifyemailComponent,
    EmailverifyComponent,
    OptpVerificationComponent,
    FiltersComponent,
    CategoryComponent,
    SearchproductsComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'website' }),
    InfiniteScrollModule,
    FormsModule,
    HttpModule,
    routing,
    Ng2CloudinaryModule,
    FileUploadModule,
    LazyLoadImageModule,
    HttpClientModule,
    Angular2SocialLoginModule,
    AdsenseModule.forRoot({
      adClient: 'ca-pub-8037091146947232',
      adSlot: 7259870550
    }),
    // LayoutModule,
    SocialLoginModule,
    DeviceDetectorModule.forRoot(),
    CloudinaryModule.forRoot(cloudinaryLib,
      {
        cloud_name: 'dxaxmyifi',
        api_key:'993998845632776',
        api_secret:'l6cu_aS0kPAbTJboTRNC0PIAf4U',
        upload_preset:'dlzrq2ef',
        transformations: {
          width:300,
          height:300,
          crop:'fit'
        }
        
      } as CloudinaryConfiguration),
      Ng2ImgMaxModule,
      LayoutModule,
      // NgxMasonryModule
    // CloudinaryModule.forRoot(cloudinaryLib,
    //   {
    //     cloud_name: 'dpad42rqw',
    //     api_key:'284564261334961',
    //     api_secret:'924OApLmp632577Wdoxufb5OAOA',
    //     upload_preset:'cfc4jwdl',
    //   })
  ],
  providers: [   
    AuthGuard,
    Configuration,
    MissionService,
    HomeService,   
    ItemService,
    RegisterService,
    LoginService,
    SettingsService,
    ChangePasswordService,
    MemberProfileService,  
    TermsService,
    LanguageService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Angular2SocialLoginModule.loadProvidersScripts(providers);
