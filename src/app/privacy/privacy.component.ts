import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { TermsService } from '../terms/terms.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';

declare var $:any;
@Component({
  selector: 'privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor(
    private _missionService: MissionService,
    private _serviceHome: HomeService,
    private _meta: Meta,
    private _conf: Configuration,
    private location:Location,
    private _router: Router,
    private _service: TermsService,
    private route:ActivatedRoute,
    private _lang: LanguageService) { }

  headerOpen: string;
  token: any;
  type = 2;
  privacy: any;
  privacySub: any;
  languageCode:any;
  ngOnInit() {

    // this.privacySub = this._lang.engPrivacy;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ?  sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.privacySub = this._lang.engPrivacy1;
                this.languageCode = 'fr';
                break;
        case 2: this.privacySub = this._lang.engPrivacy;
                this.languageCode = 'en';
                break;
        case 3: this.privacySub = this._lang.engPrivacy2;
                this.languageCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.privacySub = this._lang.engPrivacy;
      this.languageCode = 'en';
    }
    // this.seo();
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    this.privacyList();
    // this._router.navigate([this.languageCode,"privacy"])
    this.location.replaceState(`${this.languageCode}/privacy`);
  }

  seo() {
    let list = {
      type: 5
    }
    this._serviceHome.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          let data = res.data;
          // ////console.log("desc", data.description);
          if (data) {
            this._meta.addTag({ name: 'title', content: data.title })
            this._meta.addTag({ name: 'description', content: data.description })
            this._meta.addTag({ name: 'keywords', content: data.keyword.toString() })
          }
        }
      });
  }

  privacyList() {

    this._service.webContent(this.languageCode, this.type)
      .subscribe((res) => {
        if (res.code == 200) {
          this.privacy = res.data[0].configData;
        }
      });
  }

}
