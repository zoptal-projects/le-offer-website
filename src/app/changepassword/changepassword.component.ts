import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';;
import { MissionService } from '../app.service';
import { ChangePasswordService } from './changepassword.service';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';

declare var $ : any;
@Component({
  selector: 'changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private _missionService: MissionService, 
    private _conf: Configuration, private _router: Router, 
    private _service: ChangePasswordService,
    private location: Location,
    private route:ActivatedRoute,
    private _lang: LanguageService) { }

  headerClosed: string;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  registerErrMsg: any;
  token: any;
  loaderButton = false;
  changePass:any;
  languageCode:any;
  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    // this.changePass = this._lang.engchangePassword;
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.changePass = this._lang.engchangePassword1;
                this.languageCode = 'fr';
                break;
        case 2: this.changePass = this._lang.engchangePassword;
                this.languageCode = 'en';
                break;

        case 3: this.changePass = this._lang.engchangePassword2;
                this.languageCode= 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.changePass = this._lang.engchangePassword;
      this.languageCode = 'en';
    }
    this._missionService.confirmheaderClosed(this.headerClosed);
    this.token = this._conf.getItem('authToken');
    // this._router.navigate([this.languageCode,"password"]);
    this.location.replaceState(`${this.languageCode}/password`);
  }

  resetValue() {
    this.oldPassword = "";
    this.newPassword = "";
    this.confirmPassword = "";
  }

  passwordChange() {
    let list = {
      token: this.token,
      oldPassword: this.oldPassword,
      newPassword: this.newPassword,
      confirmPassword: this.confirmPassword
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 3000);
    this._service.changePassword(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.resetValue();
          this.registerErrMsg = false;
          this.registerErrMsg = res.message;
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
          this._router.navigate([this.languageCode,"password"]);
        } else {
          this.loaderButton = false;
          this.resetValue();
          this.registerErrMsg = res.message;
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        }

      });
  }

}
