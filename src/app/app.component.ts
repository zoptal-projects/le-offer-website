import { Component, OnInit, Inject } from '@angular/core';
import { Router,RoutesRecognized,NavigationEnd, NavigationStart} from '@angular/router';
import { Location , DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private _routeScrollPositions: {[url: string] : number}[] = [];
  private _subscriptions: Subscription[] = [];

  constructor(private _router: Router, public location: Location,@Inject(DOCUMENT) private document: Document){
    //console.log("router.url ")
  }
  title = 'app works!';
  name: string;
  menu: Array<any> = [];
  breadcrumbList: Array<any> = [];
  masonryItems = [
    { title: 'item 1' },
    { title: 'item 2' },
    { title: 'item 3' },
    { title: 'item 4' },
    { title: 'item 5' },
    { title: 'item 6' }
  ];
  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, 500);
  }
  onActivate(){
    this._subscriptions.push(
      // save or restore scroll position on route change
      this._router.events.pairwise().subscribe(([prevRouteEvent, currRouteEvent]) => {
        if (prevRouteEvent instanceof NavigationEnd && currRouteEvent instanceof NavigationStart) {
          this._routeScrollPositions[prevRouteEvent.url] = window.pageYOffset;
        }
        if (currRouteEvent instanceof NavigationEnd) {
          window.scrollTo(0, this._routeScrollPositions[currRouteEvent.url] || 0);
        }
      })
    );
    this._router.events
    .subscribe(res=>{
      if(res instanceof RoutesRecognized) {
        // console.log("check out ", res.url.indexOf('searchProduct'));
        res.url !== '/' ?  window.scrollTo(0, 0) : this._routeScrollPositions && Object.keys(this._routeScrollPositions).length  > 0 ?   window.scrollTo(0, this._routeScrollPositions[res.url]) :(window.scrollTo(0, this._routeScrollPositions[res.url]));
        // res.url !== '/' || res.url.indexOf('searchProduct') > -1 || res.url.indexOf('category') > -1 ?  window.scrollTo(0, 0) : this._routeScrollPositions && Object.keys(this._routeScrollPositions).length  > 0 ?   window.scrollTo(0, this._routeScrollPositions[res.url]) :(window.scrollTo(0, this._routeScrollPositions[res.url]));
      }
    });
  }
}


