import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { ChangePasswordService } from '../changepassword/changepassword.service';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';
import { Configuration } from '../app.constants';
declare var $: any;
@Component({
  selector: 'passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.css']
})
export class PasswordresetComponent implements OnInit {

  constructor(
    private _missionService: MissionService, 
    private _router: Router, 
    private route: ActivatedRoute, 
    private location: Location,
    private _service: ChangePasswordService,
    private _conf :Configuration,
    private _lang: LanguageService) { }
    
  headerClosed: string;
  headerRefresh: string;
  newPassword: string;
  repeatPassword: string;
  postId: any;
  registerErrMsg: any;
  loaderButton = false;
  passwordReset: any;
  headerOpen:string;
  languageCode:any;
  ngOnInit() {

    // this.passwordReset = this._lang.engPasswordReset;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1'):
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.passwordReset = this._lang.engPasswordReset1;
                this.languageCode ='fr';
                break;
        case 2: this.passwordReset = this._lang.engPasswordReset;
                this.languageCode ='en';
                break;
        case 3: this.passwordReset = this._lang.engPasswordReset2;
                this.languageCode ='ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.passwordReset = this._lang.engPasswordReset;
      this.languageCode ='en';
    }

    this._missionService.confirmheaderClosed(this.headerClosed);
    this.route.params.subscribe(params => {
      this.postId = params['id'];
      ////console.log("the post id is:",this.postId )
    });
    // this._router.navigate([this.languageCode,"reset"]);
    this.location.replaceState(`${this.languageCode}/reset/${this.postId}`);
  }

  resetValue() {
    this.newPassword = "";
    this.repeatPassword = "";
  }

  passwordChange() {
    let list = {
      passwordResetLink: this.postId,
      password: this.newPassword,
      repeatPassword: this.repeatPassword
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 3000);
    this._service.resetChangePassword(list)
      .subscribe((res) => {
        this.loaderButton = false;
        if (res.code == 200) {
          $(".successLink").show();
          this.resetValue();
          this.registerErrMsg = false;
          this._missionService.confirmLogin(this.headerOpen);
        } else {
          $(".successLink").hide();
          this.registerErrMsg = res.message;
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        }
      });
  }

}
