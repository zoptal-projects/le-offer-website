import { Component, OnInit, HostListener, AfterViewInit, NgZone, Inject, ComponentFactoryResolver, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute,NavigationStart, RoutesRecognized, NavigationEnd  } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { MissionService } from '../app.service';
import { HomeService } from '../home/home.service';
import { Location } from '@angular/common';
import { Configuration } from '../app.constants';
import { LanguageService } from '../app.language';
import { CloudinaryOptions } from 'ng2-cloudinary';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
// import { Cloudinary } from 'cloudinary-core';
import { Cloudinary } from '@cloudinary/angular-5.x';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DeviceDetectorService } from 'ngx-device-detector';
  // import * as  Cloudinary from 'cloudinary-core';
declare var $: any;
declare var fs:any;
declare var google: any;
declare var FB: any;
declare var gapi: any;
declare const BMap:any;
declare var BMAP_STATUS_SUCCESS:any;
declare var ImageCompressor: any;
 
// const compressor = new ImageCompressor();
@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  searchLayer: boolean = true;
  signUpContent: boolean = true;
  loginSignUpFB: boolean = false;
  header: any;
  closeShow = false;
  sellToggle = false;
  PhoneNum:any;
  CurrentLocation:any;
  subCategoryNodeId:any;
  cloudinaryData:any;
  browserDetectedLang : any;
  constructor(
    private missionService: MissionService,
    private _conf: Configuration,
    private _router: Router,
    private homeService: HomeService,
    private _lang: LanguageService,
    public _auth: AuthService,
    private Cloudinary: Cloudinary,
    private activatedRoute: ActivatedRoute,
    private imgCompressService: Ng2ImgMaxService,
    private DeviceDetectorService:DeviceDetectorService,
    @Inject(DOCUMENT) private window: Window
  ) {
    missionService.missionheaderOpen$.subscribe(
      headerOpen => {
        // this.headerListOpen = true;
        // this.headerListHelpOpen = false;
        // $(".headerMenu").addClass("headerOpenactive");
        // $(".HeaderPageList").addClass("container");
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderClose$.subscribe(
      headerClose => {
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = true;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderClosed$.subscribe(
      headerClosed => {
        // this.headerListOpen = false;
        // this.headerListHelpOpen = false;
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });
    missionService.missionheaderHelpClose$.subscribe(
      headerHelpClose => {
        // this.headerListHelpOpen = true;
        this.headerListOpen = true;
        this.headerListHelpOpen = false;
        this.headerFilter = false;
        $(".headerMenu").removeClass("headerOpenactive");
        $(".HeaderPageList").removeClass("container");
      });

    missionService.missionServer$.subscribe(
      serverError => {
        $("#serverError").modal("show");
      });

    missionService.missionPopup$.subscribe(
      headerRefresh => {
        this.loginSignUp();
      });

    missionService.missionheaderRefresh$.subscribe(
      headerRefresh => {
        this.ngOnInit();
      });
  }

  headerFilter = false;
  headerOpen = false;
  menuListDrop = false;
  headerListOpen = false;
  headerListHelpOpen = false;
  radioSelectList = false;
  loginList = false;
  token: any;
  userName: any;
  profilePicUrl: any;
  searchCat: any;
  headerRefresh: string;
  phoneNumber: string;
  email: string;
  otpNumber: string;
  deviceOs: string;
  registerSave = false;
  phone_Error = false;
  email_Error: any;
  succesSeller = false;
  placelatlng: any;
  listSell: any;
  sideListMenu = false;
  login_Toggle = false;
  signUp_toggle = false;
  SocialToggle = false;
  forgot_Toggle = false;
  otp_Toggle = false;
  registerPhoneErrMsg: any;
  registerUserErrMsg: any;

  username: string;
  phonenumber: string;
  password: string;
  registerErrMsg: any;
  loaderButton = false;
  loaderButtonfb = false;
  fullName: string;
  registerEmailErrMsg: any;
  loginModal = false;
  registerfullErrMsg: any;
  sub: any;
  accessToken: any;

  catList: any;
  toggleSlide = false;
  willingSlide = false;

  productName: string;
  productDescription: string;
  productPrice: string;
  currency: string;
  condition: string;
  category: string;
  subCategory: string;
  negotiable = "0";
  willing = 0;
  subCat: any;
  catSubFilter: any;
  imgUrl = [];
  errMsg: any;
  cloudinaryImages: any;
  singnImg: any;
  cloudinaryOptions: CloudinaryOptions = new CloudinaryOptions({
    cloudName: 'yeloadmin',
    uploadPreset: 'iirvrtog',
    autoUpload: true,
  });
    // url: `https://api.cloudinary.com/v1_1/${
    //     // this.Cloudinary.config().cloud_name
    //     }/image/upload`,
  categoryNodeId:any;
  currentCountry:any;
  languageCode:any;
  uploader: FileUploader;
  uploaders: FileUploader;
  compressResult:any;
  uploaderOptions: FileUploaderOptions = {
  
   url: `https://api.cloudinary.com/v1_1/${
          this.Cloudinary.config().cloud_name
          }/auto/upload`,
  autoUpload: true,
  isHTML5: true,
  removeAfterUpload: true,
  headers: [
    { name: 'X-Requested-With', value: 'XMLHttpRequest' },
  ]
    // }
  };
  imageUploader = (item: any, response: string, status: number, headers: any): any => {
    let cloudinaryImage = JSON.parse(response);
    console.log("cloudinaryImage",cloudinaryImage)
    let list = {
      img: cloudinaryImage.secure_url,
      cloudinaryPublicId: cloudinaryImage.public_id,
      containerHeight: cloudinaryImage.height,
      containerWidth: cloudinaryImage.width
    }
    this.ImageLoader = false;
    this.submitData(list);
  };
  imageUploaders = (item: any, response: string, status: number, headers: any): any => {
    let cloudinaryImage = JSON.parse(response);
    ////console.log(cloudinaryImage);
    this.ImageLoader = false;
    this.submitSignup(cloudinaryImage.secure_url);
  };

  ngOnInit() {
    let browserLang = sessionStorage.getItem('browserDetectedLang');
    ////console.log("browserLang ", browserLang)
    if(browserLang === null){
      this.browserDetectedLang = navigator.language || navigator['userLanguage']; 
      this._conf.setItem('browserDetectedLang',this.browserDetectedLang );
    }
    this.setSupportedLangauge();
    let latlng1 = this._conf.getItem('latlng');
    if (!latlng1) {
      this.currentLocation();
    }
    this.token = this._conf.getItem('authToken');
    this.profilePicUrl = this._conf.getItem('profilePicUrl');
    if (this.token) {
      this.userName = this._conf.getItem('username');
      this.loginList = true;
      this.userReject();
    } else {
      this.loginList = false;
    }
    $('ul.dropdown-menu').on('click', function (event) {
      event.stopPropagation();
    });

    this.listGetCategories();
    $.getJSON("https://restcountries.eu/rest/v1/alpha/in", (data) => {
      this.currency = 'USD';
    });

    $("#signPwd").keydown((event) => {
      if (event.keyCode == 32) {
        event.preventDefault();
      }
    });
    // cloudinary and angular
    let images;
    let compressorSettings = {
      toWidth : 100,
      toHeight : 100,
      mimeType : 'image/png',
      mode : 'strict',
      quality : 0.6,
      grayScale : true,
      sepia : true,
      threshold : 127,
      vReverse : true,
      hReverse : true,
      speed : 'low'
  };
  // //console.log("cloudinary ", )
    this.uploader = new FileUploader(this.uploaderOptions);
    this.uploaders = new FileUploader(this.uploaderOptions);
    // this.uploader.onBeforeUploadItem = (fileItem: any): any => {
    //   //console.log("compress image result fileItem",fileItem )
    //   fileItem.withCredentials = false;
    //   fileItem.secure = true;
    // }
    // this.uploader.onAfterAddingFile = (fileItem:any) => {
    //   fileItem.withCredentials = false;
    //   fileItem.upload_preset = this.Cloudinary.config().upload_preset;
    //   this.uploader.cancelAll();

    // };
    // this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
    //   this.uploader.cancelAll();
    //   form.append('upload_preset', this.Cloudinary.config().upload_preset);
    // };
    // this.uploader.onSuccessItem  = this.imageUploader;
    // this.uploaders.onSuccessItem  = this.imageUploaders;
  }

  setSupportedLangauge(){
    if(this.browserDetectedLang === 'en-GB' || this.browserDetectedLang === 'en-gb'){
      this._conf.setItem('Language',"2");
    }
    else if(this.browserDetectedLang === 'fr' || this.browserDetectedLang === 'fr-mc' || this.browserDetectedLang === 'fr-MC'){
      this._conf.setItem('Language',"1");
    }
    else if(this.browserDetectedLang === 'ar' || this.browserDetectedLang === 'ar-ma' || this.browserDetectedLang === 'ar-MA'){
      this._conf.setItem('Language',"3");
    }
    // else if(this.browserDetectedLang && this.browserDetectedLang.length == 0){
    //   this._conf.setItem('Language',"1");
    // }

    // setLanguages
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      ////console.log("the selected langauges is:",this.browserDetectedLang,selectedLang )
      switch(selectedLang){
        case 1:this.header = this._lang.engHeader1;
               this.languageCode = 'fr';
               break;
        case 2:this.header = this._lang.engHeader;
               this.languageCode = 'en';
               break;
        case 3:this.header = this._lang.engHeader2;
               $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
               this.languageCode = 'ar';
               break;
      }
    }
    else{
      ////console.log("french");

      this.activatedRoute.params.subscribe(params => {
        ////console.log("the  params are:",params )
        if(params['language']){
          if(params['language'] === 'fr'){
            this.header = this._lang.engHeader1;
            this.languageCode = 'fr';
            this._conf.setItem('Language',"1");
          }
          else if(params['language'] === 'ar'){
            this.header = this._lang.engHeader2;
            this.languageCode = 'ar';
            this._conf.setItem('Language',"3");
          }
          else if(params['language'] === 'en'){
            this.header = this._lang.engHeader1;
            this.languageCode = 'en';
            this._conf.setItem('Language',"2");
          }
        }
        else{
          this.header = this._lang.engHeader;
            this.languageCode = 'en';
        }
  
      });
    }
    // get to know which langauge is selected
    this.header.languageOptions.forEach((element,index) => {
      if(element.langCode === this.languageCode){
       this.header.languageOptions[index].checked = true;
      }
      else{
       this.header.languageOptions[index].checked = false;
       this.header.languageOptions[index].checked = false;
      }
    });
  }


  currenctLocation1(){
  }
  listGetCategories() {
    this.homeService.getCategoriesList(this.languageCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
        }
      });
  }

  homeLogo() {
    // $("html, body").animate({ scrollTop: 0 }, 500);
    this._router.navigate([``]);
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        // this.placelatlng = {
        //   'lat': "33.5731",
        //   'lng': "-7.5898"
        // }
        ////console.log("currentLatLng", this.placelatlng);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this._conf.setItem('Currentlatlng', JSON.stringify(this.placelatlng));
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (var i = 0; i < results[1].address_components.length; i += 1) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    var city = addressObj.long_name;       
                  }
                  if (addressObj.types[j] === 'country') {
                    var country = addressObj.short_name;
                  }
                }
              }
              $("#cityAreai").val(city + ", " + country);
              this._conf.setItem('recentCity', city + ", " + country);
              $("#searchLocation1").val(city + ", " + country);
            }
          }
        });

      }, this.geoError);
    } else {
      this.placelatlng = {
        'lat': "33.5731",
        'lng': "-7.5898"
      }
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this._conf.setItem('Currentlatlng', JSON.stringify(this.placelatlng));
      $("#cityAreai").val("2, Casablanca 20250, Morocco");
      this._conf.setItem('recentCity',"2, Casablanca 20250, Morocco");
      $("#searchLocation1").val("2, Casablanca 20250, Morocco");
      ////console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    ////console.log("Geolocation is not supported by this browser.");
  }
  userReject() {
    let list = {
      latitude: "",
      longitude: "",
      token: this.token,
      offset: "0",
      limit: "40"
    }
    this.placelatlng = this._conf.getItem('latlng');
    if (this.placelatlng) {
      this.placelatlng = JSON.parse(this.placelatlng);
      list.latitude = this.placelatlng.lat;
      list.longitude = this.placelatlng.lng;
    }
    this.homeService.homeAllList(list)
      .subscribe((res) => {
        if (res.code == 200) {

        }
      }, (err) => {
        var error = JSON.parse(err._body);
        var listErr = error.code;
        if (listErr == 401) {
          ////console.log(error);
          this._conf.clear();
          $("#userRejected").modal("show");
        }
      });
  }




  radioSelect(val) {
    this.emptySell();
    if (val != 1) {
      this.radioSelectList = true;
    } else {
      this.radioSelectList = false;
    }
  }
  succesSellerList() {
    $("#phoneSell").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry:'ma',
      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
    $("#sellModal").modal("show");
    this.succesSeller = false;
    this.emptySell();
    // this._router.navigate(["./sell"]);
  }


  emptySell() {
    this.phoneNumber = "";
    this.email = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.email
      }
    } else {
      let flag = $("#phoneSell").intlTelInput("getSelectedCountryData");
      this.listSell = {
        type: "2",
        phoneNumber: "+" + flag.dialCode + this.phoneNumber
      }
    }
    this.homeService.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.succesSeller = true;
          this.radioSelectList = false;
          this.emptySell();
        } else {
          this.email_Error = res.message;
        }
      });
  }


  searchCategory(itemName,event) {
    // this._router.navigate([`${this._router.url}`]);
    this.searchCat = '';
    itemName && itemName.length > 0 ? this._router.navigateByUrl(`${this.languageCode}/searchProduct/${itemName}`):'';
    this._router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.closeShow = true;
    ////console.log("the category name is :",this.searchCat,val);
    // if (val == 1 ) {
    //   this.closeShow = true;
    // } else {
    //   this.closeShow = true;
    //   // if(this.searchCat.length > 0)
    //   this.missionService.confirmcatName(this.searchCat);
    // }
  }

  cancelSearch() {
    this._router.navigate([`${this._router.url}`]);
    // this._router.navigate([`${this._router.url}`]);    
    this.closeShow = false;
    this.missionService.confirmcatName("1");
    this.searchCat = "";
  }

  menuListDropdown1() {
    $(".listMenu").removeClass("open");
    $(".firstMenu").addClass("open");
    $(".firstMenu").removeClass("opened");
  }
  menuListDropdown() {
    $(".firstMenu").addClass("opened");
    $(".listMenu").removeClass("open");
    $(".secondMenu").addClass("open");
  }

  logOut() {

    let google = this._conf.getItem("google");
    if (google == '1') {
      let url = window.location.href;
      document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=" + url;
    }
    this._conf.clear();
    this.missionService.confirmheaderRefresh(this.headerRefresh);
    this.window.location.reload();
    this._router.navigate(['']);
    //this.location.replaceState('');   
  }

  logOutReject() {
    this._conf.clear();
    this.window.location.reload();
    this._router.navigate(['']);
  }

  serverError() {
    //this.location.replaceState('');
  }

  searchLayerToggle() {
    this.searchLayer = false;
  }

  removeLayer() {
    this.searchLayer = true;
    $("#search_input").val("");
  }
  latlng:any;
  filterToggle() {
    $("#sideDetailsId").animate({ scrollTop: 0 }, 500);
    $("#sideDetailsId").addClass('active');
    $("body").addClass("hideHidden");
    var someValue = $(".location").text();
    //console.log(".location",someValue)
    let cityArea = this._conf.getItem('recentCity');
      if (cityArea) {
        $(".location").text(cityArea);
        $(".location").val(cityArea);
      }
    let lat = $("#lati").val();
    let lng = $("#lngi").val();
    this.latlng = {
      lat: lat,
      lng: lng
    }
    // if(this.latlng){
    //   var geocoder = new google.maps.Geocoder();
    //   var latlng = new google.maps.LatLng(this.latlng.lat,this.latlng.lng);
    //   geocoder.geocode({ 'latLng': latlng }, function (results, status) {
    //     if (status == google.maps.GeocoderStatus.OK) {
    //       if (results[1]) {
    //         ////console.log("results[0].formatted_address",results[0].formatted_address)
    //         $(".addressArea").val(results[0].formatted_address);
    //         $("#cityAreai").val(results[0].formatted_address);
    //         $(".location").text(results[0].formatted_address);
    //       }
    //     }
    //   });
    // }
    // });
  }

  sideMenuOpen() {
    $("html, body").animate({ scrollTop: 0 }, 500);
    this.sideListMenu = !this.sideListMenu;
  }


  loginSignUp() {
    $("#phone").intlTelInput({
      nationalMode: true,
      separateDialCode: true,
      initialCountry: 'ma',
      autoPlaceholder: false,
      // modify the auto placeholder
      customPlaceholder: null,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
    });
    $("#loginModal").modal("show");
    this.socialLink();
  }

  loginToggle() {
    this.login_Toggle = true;
    this.signUp_toggle = false;
    this.SocialToggle = true;
    this.otp_Toggle = false;
    this.forgot_Toggle = false;
    this.registerValue();
  }
  signUpToggle() {
    this.login_Toggle = false;
    this.signUp_toggle = true;
    this.SocialToggle = true;
    this.forgot_Toggle = false;
  }
  socialLink() {
    this.registerValue();
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.SocialToggle = false;
    this.otp_Toggle = false;
    this.forgot_Toggle = false;
  }

  resetToggle() {
    this.registerValue();
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.forgot_Toggle = true;
  }

  otpToggle() {
    this.SocialToggle = true;
    this.loaderButton = false;
    this.login_Toggle = false;
    this.signUp_toggle = false;
    this.forgot_Toggle = false;
    this.otp_Toggle = true;
  }

  sellPopup() {
    if (this.token) {
      this.restSell();
      this.imgUrl = [];
      this.category = "";
      this.categoryNodeId ="";
      this.subCat = "";
      this.catSubFilter = "";
      this.subCategory = "";
      this.subCategoryNodeId = "";
      $("#sellLocate").val();
      this.homeService.getCloudinaryData(this._conf.getItem('authToken')).subscribe((res)=>{
          ////console.log("the home cloudinary data", res);
          this.cloudinaryData = res.response;
      });
        $("#sellModals").modal("show");

    } else {
      this.DeviceDetectorService.isMobile  ? this.sideMenuOpen() :'';  
      this.loginSignUp();
    }
  }

  restSell() {
    this.productPrice = "";
    this.productName = "";
    this.productDescription = "";
    $("#selCat").val('');
    $("#selProd").val('');
  }

  selectCurrency(val) {
    this.currency = val;
  }

  selectCondition(val) {
    this.condition = val;
  }

  selectCat(val) {
    ////console.log('the category list is :', val,this.catList[val])
    this.category = this.catList[val].name;
    this.categoryNodeId = this.catList[val].categoryNodeId;
    this.subCat = [];
    this.catSubFilter = "";
    this.homeService.getSubCategoriesList(this.category,this.languageCode,this.categoryNodeId)
      .subscribe((res) => {
        res.code === 200 ? this.subCat = res.data :this.subCat = "";
      });
    //console.log("the subcat is", this.subCat);
  }

  selectSubCat(i) {
    this.subCategory = this.subCat[i].subCategoryName;
    this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
    var value = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        x.filterData = x.values.split(",");
        x.data = '';
        x.checked = false;
        // x.value = split;
      }
      value.push(x);
    });

    this.catSubFilter = value;
    //console.log("value", this.catSubFilter , this.catSubFilter.length);
  }

  checkedFilter = [];
  documentTextList(event, i, range) {
    ////console.log("d", event.target.value.length)
    ////console.log("the selected value is:",range );
    if (event.target.value.length > 0) {
      
       if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
        var index = -1;
        if (index > -1) {
          //  this.catSubFilter[i].data.splice(index, 1);
         
        } else {
          let list = {
            type: "equlTo",
            fieldName: this.catSubFilter[i].fieldName,
          }
          if( this.catSubFilter[i].type == 4   && Object.keys(this.catSubFilter[i].data).length >=  1){
            // this.catSubFilter[i].data[0].value = this.catSubFilter[i].filterData[range];
            this.catSubFilter[i].data = this.catSubFilter[i].filterData[range];
          }
          else if(this.catSubFilter[i].type == 4  && Object.keys(this.catSubFilter[i].data).length == 0){
            list['value'] = this.catSubFilter[i].filterData[range];
            // this.catSubFilter[i].data.push(list);
            this.catSubFilter[i].data = list['value'] ;
            this.catSubFilter[i].checked = true;
          }
          else if( this.catSubFilter[i].type == 6   && Object.keys(this.catSubFilter[i].data).length >=  1){
            this.catSubFilter[i].data = range;
          }
          else if(this.catSubFilter[i].type == 6  && Object.keys(this.catSubFilter[i].data).length == 0){
            list['value'] = range;
            this.catSubFilter[i].data = list['value'];
            this.catSubFilter[i].checked = true;
          }
          else if(this.catSubFilter[i].type == 2){
            // //console.log("chekc once", event)
            const checkbox = event.target as HTMLInputElement;
            if(checkbox.checked){
              this.checkedFilter.push({
                checked: checkbox.checked,
                name: event.target.name,
                value: event.target.value
              })
            }
            else{
              let index = this.checkedFilter.findIndex((x)=>x.name === event.target.name);
              if(index > -1){
                this.checkedFilter.splice(index,1);
              } 
            }
          }
          let finalValue = '';
          if( this.checkedFilter.length > 0  && this.catSubFilter[i].type == 2){
            this.checkedFilter.forEach((x,index)=> {
              (index != this.checkedFilter.length - 1)? finalValue += (x.value)+',' :
              finalValue += (x.value);
            });
            list['value'] = finalValue; 
            if(this.catSubFilter[i].data.length == 0){
              this.catSubFilter[i].data = finalValue;
            }
            else{
              // let index = this.catSubFilter[i].data.findIndex(x => x.fieldName == this.catSubFilter[i].fieldName);
              this.catSubFilter[i].data = finalValue;
              this.catSubFilter[i].checked = true;
            }
          }
          else if(this.checkedFilter.length == 0 && this.catSubFilter[i].type == 2){
            this.catSubFilter[i].data = '';
            this.catSubFilter[i].checked = false;
          }
        }
      } 
      else {
        this.catSubFilter[i].data = event.target.value.replace(',','.');
        this.catSubFilter[i].checked = true;
        ( this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5 ) ?
        this.catSubFilter[i].types = "range" : this.catSubFilter[i].types = "equlTo";
      }
    } 
    else {
          this.catSubFilter[i].checked = false;
    }
    // //console.log("the filter data is:", this.catSubFilter)

  }
  switchToggle(val) {
    if (val == true) {
      this.negotiable = "1";
    } else {
      this.negotiable = "0";
    }
  }

  willingToggle(val) {
    this.swapArr = [];
    if (val == true) {
      this.willing = 1;
    } else {
      this.willing = 0;
    }
  }

  uploadProduct() {
    var filData = {};
    let allowToPost = false;
    let catSubFilterCount = 0;
    let FilterFillData = {};
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true) {
          // filData[x.fieldName] = x.data;
          filData[x.id] = x.data;
          x.checked && x.isMandatory == 'true' ? FilterFillData[x.id] = x.data :'';
        }
        x.isMandatory == 'true' ? catSubFilterCount+=1 :'';
      });
    }
    console.log("catSubFilterCount",catSubFilterCount)
    let city = $("#cityS").val();
    let state = $("#state").val();
    let location = $("#locate").val()
    let lat = $("#latS").val();
    let lang = $("#lngS").val();
    setTimeout(() => {
      this.loaderButton = false;
      this.errMsg = false;
    }, 3000);
    //console.log("all sub cta is", this.subCat.length,Object.keys(filData).length > 0,this.catSubFilter.length );
    if(this.imgUrl.length > 0 && location && this.category &&  this.productName ){
       if(this.subCat && this.subCat.length > 0 && this.subCategory){
          if(this.catSubFilter && this.catSubFilter.length > 0){
            //  if(Object.keys(filData).length > 0 &&  Object.keys(filData).length ===  this.catSubFilter.length){
            if(Object.keys(FilterFillData).length > 0 &&  Object.keys(FilterFillData).length ===  catSubFilterCount){
              allowToPost = true;
             } 
             else{
              catSubFilterCount > 0 ? allowToPost = false : allowToPost = true;
              // allowToPost = false;
              // this.catSubFilter  && this.catSubFilter.length > 0 ? allowToPost = true : allowToPost = true;
             } 
          }
          else{
            this.catSubFilter  && this.catSubFilter.length > 0 ? allowToPost = true : allowToPost = true;
            // //console.log("allowToPost",allowToPost)
          } 
       }
       else{
        this.subCat  ? allowToPost = false : allowToPost = true;
       } 
    }
    else{
      allowToPost = false;
    }
    // this.subCat && this.subCat.length > 0 && this.subCategory && Object.keys(filData).length > 0 &&  Object.keys(filData).length ===  this.catSubFilter.length ? subCatFilter =  true : subCatFilter =  false;
    // this.imgUrl.length > 0 && location && this.category &&  this.productName ? subCatFilter ? 
    // allowToPost = true : subCatFilter === false ? allowToPost = true : '' : allowToPost = true;
    // if (this.imgUrl.length > 0 && location && this.category) {
    if (allowToPost) {
      this.errMsg = false;
      let list = {
        token: this.token,
        type: "0",
        mainUrl: this.imgUrl[0].img,
        thumbnailImageUrl: this.imgUrl[0].img,
        imageCount: this.imgUrl.length,
        containerHeight: this.imgUrl[0].containerHeight,
        containerWidth: this.imgUrl[0].containerWidth,
        cloudinaryPublicId: this.imgUrl[0].cloudinaryPublicId,
        price: this.productPrice.indexOf(',') > -1 ?  this.productPrice.replace(',','.') : this.productPrice,
        currency: this.currency,
        productName: this.productName,
        description: this.productDescription,
        condition: this.condition,
        negotiable: this.negotiable,
        category: this.category,
        categoryNodeId:this.categoryNodeId,
        subCategory: this.subCategory,
        subCategoryNodeId :this.subCategoryNodeId,
        isSwap: this.willing,
        swapingPost: JSON.stringify(this.swapArr),
        filter: JSON.stringify(filData),
        location: location,
        latitude: lat,
        longitude: lang,
        city: city,
        countrySname: state,
        imageUrl1: "",
        cloudinaryPublicId1: "",
        imageUrl2: "",
        cloudinaryPublicId2: "",
        imageUrl3: "",
        cloudinaryPublicId3: "",
        imageUrl4: "",
        cloudinaryPublicId4: ""
      }
      this.subCategory ? list['subCategoryNodeId'] = this.subCategoryNodeId: 
      list['subCategoryNodeId'] ='';
      if (this.imgUrl && this.imgUrl[1]) {
        list.imageUrl1 = this.imgUrl[1].img;
        list.cloudinaryPublicId1 = this.imgUrl[1].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[2]) {
        list.imageUrl2 = this.imgUrl[2].img;
        list.cloudinaryPublicId2 = this.imgUrl[2].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[3]) {
        list.imageUrl3 = this.imgUrl[3].img;
        list.cloudinaryPublicId3 = this.imgUrl[3].cloudinaryPublicId;
      }
      if (this.imgUrl && this.imgUrl[4]) {
        list.imageUrl4 = this.imgUrl[4].img;
        list.cloudinaryPublicId4 = this.imgUrl[4].cloudinaryPublicId;
      }
      this.loaderButton = true;
      //console.log("list", list);
      this.homeService.postProduct(list)
        .subscribe((res) => {
          this.loaderButton = false;
          if (res.code == 200) {
            this._router.navigate([this.languageCode,"settings"]);
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            $(".modal").modal("hide");
          }
        }, err => {
          this.loaderButton = false;
          var error = JSON.parse(err._body);
          this.errMsg = error.message;
        });
    } else {
      this.languageCode == "en" ? 
      this.errMsg = "Mandatory field is missing (Please fill the fields with * symbol) ":
      this.languageCode == "fr" ?  this.errMsg = "Le champ obligatoire est manquant (Veuillez remplir les champs avec le symbole *)": this.errMsg ="الحقل الإلزامي مفقود (يرجى ملء الحقول برمز *)";
    }
  }

  swapList: any;
  swapSearch(val) {
    let list = {
      token: this.token,
      productName: val
    }
    if (val.length > 0) {
      this.homeService.searchSwap(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.swapList = res.data;
          } else {
            this.swapList = [];
          }
        })
    } else {
      this.swapList = [];
    }
  }

  swapArr = [];
  swapPost: string;
  postSwap(list) {
    let data = {
      swapDescription: list.description,
      swapPostId: list.postId,
      swapTitle: list.productName
    }
    this.swapArr.push(data);
  }

  cancelSwapList(i) {
    this.swapArr.splice(i, 1)
  }

  blurSwap() {
    setTimeout(() => {
      this.swapPost = "";
      this.swapList = [];
    }, 1000)
  }

  cancelSwap() {
    this.swapPost = "";
    this.swapArr = [];
    this.swapList = [];
  }

  submitData(url) {
    if (this.imgUrl && this.imgUrl.length < 5) {
      this.imgUrl.push(url);
    }
  }

  removeImg(i) {
    this.imgUrl.splice(i, 1);
  }

  
  ImageLoader:any;
  fileUploader(event:EventEmitter<File[]>) {
    this.uploader.clearQueue();
    const file: File = event[0];
    this.ImageLoader = true;
    this.imgCompressService.resizeImage(file, 600, 800).subscribe(
      result => {
        const newImage = new File([result], result.name);
        //console.log("result", result);
      this.uploader.onBeforeUploadItem = (fileItem: any): any => {
      //console.log("compress image result fileItem",fileItem )
      fileItem.withCredentials = false;
      fileItem.secure = true;
    }
    this.uploader.onAfterAddingFile = (fileItem:any) => {
      fileItem.withCredentials = false;
      fileItem.upload_preset = this.Cloudinary.config().upload_preset;
      this.uploader.cancelAll();

    };
    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      this.uploader.cancelAll();
      form.append('upload_preset', this.Cloudinary.config().upload_preset);
    };
    this.uploader.onSuccessItem  = this.imageUploader;
    this.uploaders.onSuccessItem  = this.imageUploaders;

        this.uploader.addToQueue([newImage]);
        this.uploader.uploadAll();
    },
      error => console.log(error));
  }

  public currencyList = [
    { value: "ARP", text: "Argentina Pesos" },
    { value: "ATS", text: "Austria Schillings" },
    { value: "AUD", text: "Australia Dollars" },
    { value: "BSD", text: "Bahamas Dollars" },
    { value: "BBD", text: "Barbados Dollars" },
    { value: "BEF", text: "Belgium Francs" },
    { value: "BMD", text: "Bermuda Dollars" },
    { value: "BRR", text: "Brazil Real" },
    { value: "BGL", text: "Bulgaria Lev" },
    { value: "CAD", text: "Canada Dollars" },
    { value: "CHF", text: "Switzerland Francs" },
    { value: "CLP", text: "Chile Pesos" },
    { value: "CNY", text: "China Yuan Renmimbi" },
    { value: "CYP", text: "Cyprus Pounds" },
    { value: "CSK", text: "Czech Republic Koruna" },
    { value: "DEM", text: "Germany Deutsche Marks" },
    { value: "DKK", text: "Denmark Kroner" },
    { value: 'DH' , text: 'Dirham'},
    { value: "DZD", text: "Algeria Dinars" },
    { value: "EGP", text: "Egypt Pounds" },
    { value: "ESP", text: "Spain Pesetas" },
    { value: "EUR", text: "Euro" },
    { value: "FJD", text: "Fiji Dollars" },
    { value: "FIM", text: "Finland Markka" },
    { value: "FRF", text: "France Francs" },
    { value: "GRD", text: "Greece Drachmas" },
    { value: "HKD", text: "Hong Kong Dollars" },
    { value: "HUF", text: "Hungary Forint" },
    { value: "ISK", text: "Iceland Krona" },
    { value: "INR", text: "India Rupees" },
    { value: "IDR", text: "Indonesia Rupiah" },
    { value: "IEP", text: "Ireland Punt" },
    { value: "ILS", text: "Israel New Shekels" },
    { value: "ITL", text: "Italy Lira" },
    { value: "JMD", text: "Jamaica Dollars" },
    { value: "JPY", text: "Japan Yen" },
    { value: "JOD", text: "Jordan Dinar" },
    { value: "KRW", text: "Korea(South) Won" },
    { value: "LBP", text: "Lebanon Pounds" },
    { value: "LUF", text: "Luxembourg Francs" },
    { value: "MYR", text: "Malaysia Ringgit" },
    { value: 'MAD', text: 'Moroccan dirham'},
    { value: "MXP", text: "Mexico Pesos" },
    { value: "NLG", text: "Netherlands Guilders" },
    { value: "NZD", text: "New Zealand Dollars" },
    { value: "NOK", text: "Norway Kroner" },
    { value: "PKR", text: "Pakistan Rupees" },
    { value: "PHP", text: "Philippines Pesos" },
    { value: "PLZ", text: "Poland Zloty" },
    { value: "PTE", text: "Portugal Escudo" },
    { value: "ROL", text: "Romania Leu" },
    { value: "RUR", text: "Russia Rubles" },
    { value: "SAR", text: "Saudi Arabia Riyal" },
    { value: "SGD", text: "Singapore Dollars" },
    { value: "SKK", text: "Slovakia Koruna" },
    { value: "SDD", text: "Sudan Dinar" },
    { value: "SEK", text: "Sweden Krona" },
    { value: "TWD", text: "Taiwan Dollars" },
    { value: "THB", text: "Thailand Baht" },
    { value: "TTD", text: "Trinidad and Tobago Dollars" },
    { value: "TRL", text: "Turkey Lira" },
    { value: "USD", text: "United States Dollars" },
    { value: "VEB", text: "Venezuela Bolivar" },
    { value: "ZMK", text: "Zambia Kwacha" },
    { value: "XAG", text: "Silver Ounces" },
    { value: "XAU", text: "Gold Ounces" },
    { value: "XCD", text: "Eastern Caribbean Dollars" },
    { value: "XDR", text: "Special Drawing Right(IMF)" },
    { value: "XPD", text: "Palladium Ounces" },
    { value: "XPT", text: "Platinum Ounces" },
    { value: "ZAR", text: "South Africa Rand" },
    { value: 'RSD', text:'Serbian dinar'}
  ]


  resetPassword() {
    let list = {
      email: this.email,
      type: '0'
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 1000);
    if (this.email) {
      this.homeService.resetPwd(list)
        .subscribe((res) => {
          this.loaderButton = false;
          if (res.code == 200) {
            this.email = "";
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Succès! Veuillez vérifier votre adresse e-mail pour le lien de réinitialisation du mot de passe" :
            this.registerErrMsg = "النجاح! يرجى التحقق من بريدك الإلكتروني للحصول على رابط إعادة تعيين كلمة المرور"
            setTimeout(() => {
              this.loginToggle();
            }, 3000);
          } else {
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Essayez encore une fois!" :
            this.registerErrMsg = "حاول مرة أخرى!"
          }
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        });
    } else {
      this.loaderButton = false;
      // this.registerErrMsg = "field is missing";
      this.languageCode == "en" ? 
      this.registerErrMsg = "Mandatory field is missing":
      this.languageCode == "fr" ?  this.registerErrMsg = "Le champ obligatoire est manquant": this.registerErrMsg = "حقل إلزامي مفقود";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }




  resetValue() {
    this.username = "";
    this.password = "";
  }
  mySubscription:any;
  login() {
    let list = {
      username: this.username.replace(' ',''),
      password: this.password,
      loginType: 1,
      pushToken: "",
      place: "",
      city: "",
      countrySname: "",
      latitude: "",
      longitude: ""
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    if (this.username && this.password) {
      this.homeService.loginYelo(list)
        .subscribe((res) => {
          // this.loaderButton = false;
          if (res.code == 200) {
            this._conf.setItem('authToken', res.token);
            this._conf.setItem('email', res.email);
            this._conf.setItem('username', res.username);
            this._conf.setItem('userId', res.userId);
            this._conf.setItem('profilePicUrl', res.profilePicUrl);
            this._conf.setItem('mqttId', res.mqttId);
            this.registerErrMsg = false;
            this.resetValue();
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            $(".modal").modal("hide");
            // this._router.routeReuseStrategy.shouldReuseRoute = () => false;
            // this._router.onSameUrlNavigation = 'reload';
            
            this._router.routeReuseStrategy.shouldReuseRoute = function () {
              return false;
            };
            this.mySubscription = this._router.events.subscribe((event) => {
              if (event instanceof NavigationEnd) {
                // Trick the Router into believing it's last link wasn't previously loaded
                this._router.navigated = false;
              }
            });
            this._router.navigate([`${this._router.url}`]);
            //this.location.replaceState('');


          } else {
            this.loaderButton = false;
            // this.registerErrMsg = res.message;
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Essayez encore" :
            this.registerErrMsg = "حاول مرة أخرى"
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 3000);
          }
        });
    } else {
      this.loaderButton = false;
      // this.registerErrMsg = "field is missing";
      this.languageCode == "en" ? 
      this.registerErrMsg = "Mandatory field is missing":
      this.languageCode == "fr" ?  this.registerErrMsg = "Le champ obligatoire est manquant": this.registerErrMsg = "حقل إلزامي مفقود";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }

  logDevice() {
    var module = {
      options: [],
      header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor],
      dataos: [
        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
        { name: 'Windows', value: 'Win', version: 'NT' },
        { name: 'iPhone', value: 'iPhone', version: 'OS' },
        { name: 'iPad', value: 'iPad', version: 'OS' },
        { name: 'Kindle', value: 'Silk', version: 'Silk' },
        { name: 'Android', value: 'Android', version: 'Android' },
        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
        { name: 'Linux', value: 'Linux', version: 'rv' },
        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
      ],
      databrowser: [
        { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
        { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
        { name: 'Safari', value: 'Safari', version: 'Version' },
        { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
        { name: 'Opera', value: 'Opera', version: 'Opera' },
        { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
        { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
      ],
      init: function () {
        var agent = this.header.join(' '),
          os = this.matchItem(agent, this.dataos),
          browser = this.matchItem(agent, this.databrowser);

        return { os: os, browser: browser };
      },
      matchItem: function (string, data) {
        var i = 0,
          j = 0,
          html = '',
          regex,
          regexv,
          match,
          matches,
          version;

        for (i = 0; i < data.length; i += 1) {
          regex = new RegExp(data[i].value, 'i');
          match = regex.test(string);
          if (match) {
            regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
            matches = string.match(regexv);
            version = '';
            if (matches) { if (matches[1]) { matches = matches[1]; } }
            if (matches) {
              matches = matches.split(/[._]+/);
              for (j = 0; j < matches.length; j += 1) {
                if (j === 0) {
                  version += matches[j] + '.';
                } else {
                  version += matches[j];
                }
              }
            } else {
              version = '0';
            }
            return {
              name: data[i].name,
              version: parseFloat(version)
            };
          }
        }
        return { name: 'unknown', version: 0 };
      }
    };

    var e = module.init(),
      debug = '';

    let list = {
      token: this._conf.getItem('authToken'),
      username: this._conf.getItem('username'),
      deviceName: e.os.name,
      deviceOs: e.os.version,
      appVersion: e.browser.version,
      deviceType: 3,
    }
    // ////console.log(list);
    this.homeService.logDeviceYelo(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this._router.navigate([`${this._router.url}`]);
          //this.location.replaceState('');
        }
      });
  }


  deviceId() {
    var module = {
      options: [],
      header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor],
      dataos: [
        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
        { name: 'Windows', value: 'Win', version: 'NT' },
        { name: 'iPhone', value: 'iPhone', version: 'OS' },
        { name: 'iPad', value: 'iPad', version: 'OS' },
        { name: 'Kindle', value: 'Silk', version: 'Silk' },
        { name: 'Android', value: 'Android', version: 'Android' },
        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
        { name: 'Linux', value: 'Linux', version: 'rv' },
        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
      ],
      databrowser: [
        { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
        { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
        { name: 'Safari', value: 'Safari', version: 'Version' },
        { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
        { name: 'Opera', value: 'Opera', version: 'Opera' },
        { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
        { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
      ],
      init: function () {
        var agent = this.header.join(' '),
          os = this.matchItem(agent, this.dataos),
          browser = this.matchItem(agent, this.databrowser);

        return { os: os, browser: browser };
      },
      matchItem: function (string, data) {
        var i = 0,
          j = 0,
          html = '',
          regex,
          regexv,
          match,
          matches,
          version;

        for (i = 0; i < data.length; i += 1) {
          regex = new RegExp(data[i].value, 'i');
          match = regex.test(string);
          if (match) {
            regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
            matches = string.match(regexv);
            version = '';
            if (matches) { if (matches[1]) { matches = matches[1]; } }
            if (matches) {
              matches = matches.split(/[._]+/);
              for (j = 0; j < matches.length; j += 1) {
                if (j === 0) {
                  version += matches[j] + '.';
                } else {
                  version += matches[j];
                }
              }
            } else {
              version = '0';
            }
            return {
              name: data[i].name,
              version: parseFloat(version)
            };
          }
        }
        return { name: 'unknown', version: 0 };
      }
    };

    var e = module.init(),
      debug = '';

    this.deviceOs = e.os.name;
    this._conf.setItem("deviceId", this.deviceOs);
    // this.otpToggle();
    this.otpSend();
  }


  otpSend() {
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      deviceId: this.deviceOs,
      phoneNumber: "+" + flag.dialCode + this.phonenumber
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    let fullname = $("#fullname").val();
    let username = $("#username").val();
    let email = $("#email").val();
    let phone = $("#phone").val();
    let signPwd = $("#signPwd").val();
    if (fullname && username && email && signPwd && phone) {
      this.otpToggle();
      this.homeService.otpCheck(list)
        .subscribe((res) => {
          ////console.log("the res is :",res)
          this.PhoneNum = list.phoneNumber;
          this.loaderButton = false;
          if (res.code == 200) {
            // this.otpVerify();
          }
        });
    } else {
      this.loaderButton = false;
      (this.languageCode == 'en') ? this.registerErrMsg = "field is missing":
      this.languageCode == "fr" ? this.registerErrMsg = "champ est manquant":
      this.registerErrMsg = "الحقل مفقود";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }

  otpVerify() {
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      otp: this.otpNumber,
      deviceId: this.deviceOs,
      // phoneNumber: "+" + flag.dialCode + this.phonenumber
      phoneNumber: this.PhoneNum
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    this.homeService.verifyOtp(list)
      .subscribe((res) => {
        this.loaderButton = false;
        if (res.code == 200) {
          if(this.social == 1){
            this.fbRegisterlogin();
          }else if(this.social == 2){
            this.googleRegister();
          }else{
          this.register();
          }
        } 
        else {
          this.loaderButton = false;
          (this.languageCode == 'en') ? this.registerErrMsg = "field is missing":
          this.languageCode == "fr" ? this.registerErrMsg = "champ est manquant":
          this.registerErrMsg = "الحقل مفقود"
          setTimeout(() => {
            this.registerErrMsg = false;
          }, 3000);
        }
      });
    }


    fileSignupUploader(event:EventEmitter<File[]>) {
      this.uploaders.clearQueue();
      const file: File = event[0];
      this.imgCompressService.resizeImage(file, 600, 800).subscribe(
        result => {
          const newImage = new File([result], result.name);
          //console.log("result", result);
        this.uploaders.onBeforeUploadItem = (fileItem: any): any => {
        //console.log("compress image result fileItem",fileItem )
        fileItem.withCredentials = false;
        fileItem.secure = true;
      }
      this.uploaders.onAfterAddingFile = (fileItem:any) => {
        fileItem.withCredentials = false;
        fileItem.upload_preset = this.Cloudinary.config().upload_preset;
        this.uploaders.cancelAll();
  
      };
      this.uploaders.onBuildItemForm = (fileItem: any, form: FormData): any => {
        this.uploaders.cancelAll();
        form.append('upload_preset', this.Cloudinary.config().upload_preset);
      };
      this.uploader.onSuccessItem  = this.imageUploader;
      this.uploaders.onSuccessItem  = this.imageUploaders;
  
          this.uploaders.addToQueue([newImage]);
          this.uploaders.uploadAll();
      },
        error => console.log(error)
      );
      // this.uploaders.uploadAll();
    }

  submitSignup(url) {
    this.singnImg = url;
  }

  register() {
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      username: this.username,
      fullName: this.fullName,
      email: this.email,
      phoneNumber:  this.PhoneNum,
      password: this.password,
      signupType: 1,
      deviceType: 3,
      deviceId: "",
      pushToken: "",
      location: "",
      latitude: "",
      longitude: "",
      profilePicUrl: ""
    }
    if (this.singnImg) {
      list.profilePicUrl = this.singnImg;
    }
    this.loaderButton = true;
    setTimeout(() => {
      this.loaderButton = false;
    }, 5000);
    if (this.username && this.fullName && this.email && this.password && this.PhoneNum) {
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          // this.loaderButton = false;
          if (res.code == 200) {
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', res.response.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('mqttId', res.response.mqttId);
            this._conf.setItem('profilePicUrl', res.response.profilePicUrl);
            this.registerValue();
            this.registerErrMsg = false;
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            $(".modal").modal("hide");
            this._router.navigate([`${this._router.url}`]);
          } else {
            this.loaderButton = false;
            // this.registerErrMsg = res.message;
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Essayez encore" :
            this.registerErrMsg = "حاول مرة أخرى"
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 3000);
          }
        });
    } else {
      this.loaderButton = false;
      // this.registerErrMsg = "field is missing";
      this.languageCode == "en" ? 
      this.registerErrMsg = "Mandatory field is missing":
      this.languageCode == "fr" ?  this.registerErrMsg = "Le champ obligatoire est manquant": this.registerErrMsg = "حقل إلزامي مفقود";
      setTimeout(() => {
        this.registerErrMsg = false;
      }, 2000);
    }
  }

  emailValidationCheck(val) {
    this.emailValidation(val);
    let list = {
      email: val
    }
    this.homeService.emailCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerEmailErrMsg = res.message;
          this.email = "";
          setTimeout(() => {
            this.registerEmailErrMsg = false;
          }, 3000);
        } else {
          this.registerEmailErrMsg = false;
        }
      });
  }

  usernameCheck(val) {
    let list = {
      username: val
    }
    this.homeService.userNameCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerUserErrMsg = res.message;
          this.username = "";
          setTimeout(() => {
            this.registerUserErrMsg = false;
          }, 3000);
        } else {
          this.registerUserErrMsg = false;
        }
      });
  }

  phonenumberCheck(val) {
    this.mobileValidation(val);
    let flag = $("#phone").intlTelInput("getSelectedCountryData");
    let list = {
      phoneNumber: "+" + flag.dialCode + val
    }
    this.homeService.phoneNumberCheck(list)
      .subscribe((res) => {
        if (res.code != 200) {
          // alert(res.message);
          this.registerPhoneErrMsg = res.message;
          this.phonenumber = "";
          setTimeout(() => {
            this.registerPhoneErrMsg = false;
          }, 3000);
        } else {
          this.registerPhoneErrMsg = false;
        }
      });
  }

  registerValue() {
    this.fullName = "";
    this.username = "";
    this.email = "";
    this.phonenumber = "";
    this.password = "";
    this.otpNumber = "";
  }

  emailValidation(value) {
    this.registerErrMsg = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.registerEmailErrMsg = false;
        this.registerSave = true;
      } else {
        // this.registerEmailErrMsg = "email is incorrect";
        this.languageCode == "en" ? this.registerEmailErrMsg = "email is incorrect" :
        this.languageCode == "fr"? this.registerEmailErrMsg =  "l'email est incorrect": this.registerEmailErrMsg = "البريد الالكتروني غير صحيح"
        this.registerSave = false;
      }
    }
  }

  mobileValidation(value) {
    this.registerErrMsg = false;
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;

    if (value.match(regexPhone)) {
      this.registerPhoneErrMsg = false;
      this.phone_Error = false;
      this.registerSave = true;
    } else {
      var val = value.replace(/([^+0-9]+)/gi, '');
      this.phonenumber = val;
      this.phone_Error = true;
      this.languageCode == "en" ? this.registerPhoneErrMsg = "phone number is incorrect" :
      this.languageCode == "fr"? this.registerPhoneErrMsg =  "le numéro de téléphone est incorrect": this.registerPhoneErrMsg = "رقم الهاتف غير صحيح"
      this.registerSave = false;
    }
  }

  signInWithGoogle(): void {
    this._auth.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => 
      {
        //console.log("google", x)
        console.log("google", x);
        this.fullName = x.name;
        this.email = x.email;
        this.onGoogle();
      });
  }

  signInWithFB(): void {
    this._auth.signIn(FacebookLoginProvider.PROVIDER_ID).then(x =>
       {
        this.fullName = x.name;
        this.email = x.email;
        console.log("facebook", x)
         this.me();
        });
  }
  onGoogle() {
    this._auth.authState.subscribe((user) => {
      var googleUser = user;
      //console.log("test", user,user && user.provider == 'GOOGLE');
      if (user && user.provider == 'GOOGLE') {
        // this._zone.run(() => {
          let list = {
            googleId: googleUser.id,
            email: googleUser.email,
            profilePicUrl: "",
            loginType: 3,
            pushToken: "",
            place: "",
            city: "",
            countrySname: "",
            latitude: "",
            longitude: ""
          }
          list.profilePicUrl = "";
          if (googleUser.photoUrl) {
            list.profilePicUrl = googleUser.photoUrl;
          } else {
            list.profilePicUrl = "";
          }
          // $.getJSON("https://freegeoip.net/json/", (data) => {
            if(this._conf.getItem('IpInfo')){
              let geoData =  JSON.parse(this._conf.getItem('IpInfo'));
              list.city = geoData.city;
              list.countrySname = geoData.country_name;
              list.latitude = String(geoData.latitude);
              list.longitude = String(geoData.longitude);
            }
            else{
              let data = JSON.parse(this._conf.getItem('latlng'));
              list.city = this._conf.getItem('recentCity');
              // list.countrySname = geoData.country_name;
              list.latitude = String(data.lat);
              list.longitude = String(data.lng);
            }
          // });
          // ////console.log(list);

          $(".loadgoogleSocial").show();
          setTimeout(() => {
            $(".loadgoogleSocial").hide();
          }, 3000);
          this.homeService.loginYelo(list)
            .subscribe((res) => {
              if (res.code == 200) {
                $(".loadgoogleSocial").hide();
                this._conf.setItem('authToken', res.token);
                this._conf.setItem('email', res.email);
                this._conf.setItem('username', res.username);
                this._conf.setItem('userId', res.userId);
                this._conf.setItem('profilePicUrl', res.profilePicUrl);
                this._conf.setItem('mqttId', res.mqttId);
                this.registerErrMsg = false;
                this.logDevice();
                this.missionService.confirmheaderRefresh(this.headerRefresh);
                $(".modal").modal("hide");
                this._router.navigate([`${this._router.url}`]);
                //this.location.replaceState('');
              } else {
                $("#fullname").val(googleUser.name);
                $("#email").val(googleUser.email);
                $("#googleId").val(googleUser.id);
                $("#googleToken").val(googleUser.idToken);
                $(".signupClick").trigger("click");
                $(".ls").hide();
                $(".signUpgoogle").show();
              }
            });
        // });
      }
    });

  }

  me() {
    this.resetValue();
    FB.api('/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {
        //console.log("result in facebook", result)
        let list = {
          facebookId: result.id,
          email: result.email,
          profilePicUrl: "",
          loginType: 2,
          pushToken: "",
          place: "",
          city: "",
          countrySname: "",
          latitude: "",
          longitude: ""
        }
        if (result.picture.data.url) {
          list.profilePicUrl = result.picture.data.url;
        } else {
          list.profilePicUrl = "";
        }
        $(".loadSocial").show();
        setTimeout(() => {
          $(".loadSocial").hide();
        }, 3000);
        this.homeService.loginYelo(list)
          .subscribe((res) => {
            if (res.code == 200) {
              $(".loadSocial").hide();
              this._conf.setItem('authToken', res.token);
              this._conf.setItem('email', res.email);
              this._conf.setItem('username', res.username);
              this._conf.setItem('userId', res.userId);
              this._conf.setItem('profilePicUrl', res.profilePicUrl);
              this._conf.setItem('mqttId', res.mqttId);
              this.registerErrMsg = false;
              this.logDevice();
              this.missionService.confirmheaderRefresh(this.headerRefresh);
              this._router.navigate([`${this._router.url}`]);
              //this.location.replaceState('');
            } else {
              this.registerFb();
              $("#fullname").val(result.name);
              $("#email").val(result.email);
              $(".signupClick").trigger("click");
              $(".signupRegister").hide();
              $(".signUpFb").show();             
            }
          });
      });
  }

  social:any;
  
  registerFb() {
    this.social = 1;
    FB.getLoginStatus((response: any) => {
      if (response) {
        this.accessToken = response.authResponse.accessToken;
        ////console.log("test", this.accessToken);
      }
    });

    FB.api('/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {

        this.profilePicUrl = result.picture.data.url;
        this.accessToken = this.accessToken;

      });
  }

  fbRegisterlogin() {
    let fullname = $("#fullname").val();
    let email = $("#email").val();
    if (fullname) {
      this.registerfullErrMsg = false;
      let list = {
        accessToken: this.accessToken,
        username: this.username,
        fullName: fullname,
        email: email,
        phoneNumber: this.phonenumber,
        password: this.password,
        profilePicUrl: this.profilePicUrl,
        signupType: 2,
        deviceType: 3,
        deviceId: "",
        pushToken: "pushToken",
        location: "",
        latitude: "",
        longitude: ""
      }
      this.loaderButton = true;
      setTimeout(() => {
        this.loaderButton = false;
      }, 3000);
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.loaderButton = false;
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', this.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('profilePicUrl', this.profilePicUrl);
            this._conf.setItem('mqttId', res.response.mqttId);
            this.registerErrMsg = false;
            this.logDevice();
            this.registerValue();
            this.missionService.confirmheaderRefresh(this.headerRefresh);           
            //this.location.replaceState('');
          } else {
            // this.registerErrMsg = res.message;
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Essayez encore" :
            this.registerErrMsg = "حاول مرة أخرى"
            this.loaderButtonfb = false;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 5000);
          }
        });
    } else {
      this.registerfullErrMsg = true;
      setTimeout(() => {
        this.registerfullErrMsg = false;
      }, 5000);
    }
  }
  onSuccess = (googleUser) => {
    ////console.log("the test for google login", googleUser)
    // this._zone.run(() => {
      let list = {
        googleId: googleUser.uid,
        email: googleUser.email,
        profilePicUrl: "",
        loginType: 3,
        pushToken: "",
        place: "",
        city: "",
        countrySname: "",
        latitude: "",
        longitude: ""
      }
      if (googleUser.image) {
        list.profilePicUrl = googleUser.image;
      } else {
        list.profilePicUrl = "";
      }
      $.getJSON("https://freegeoip.net/json/", (data) => {
        list.city = data.city;
        list.countrySname = data.country_name;
        list.latitude = String(data.latitude);
        list.longitude = String(data.longitude);
      });
      $(".loadgoogleSocial").show();
      setTimeout(() => {
        $(".loadgoogleSocial").hide();
      }, 3000);
      this.homeService.loginYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            $(".loadgoogleSocial").hide();
            this._conf.setItem('authToken', res.token);
            this._conf.setItem('email', res.email);
            this._conf.setItem('username', res.username);
            this._conf.setItem('userId', res.userId);
            this._conf.setItem('profilePicUrl', res.profilePicUrl);
            this._conf.setItem('mqttId', res.mqttId);
            this.registerErrMsg = false;
            this.logDevice();
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            this._router.navigate([`${this._router.url}`]);
            //this.location.replaceState('');
          } else {
            $("#fullname").val(googleUser.name);
            $("#email").val(googleUser.email);
            $("#googleId").val(googleUser.uid);
            $("#googleToken").val(googleUser.idToken);
            $(".signupClick").trigger("click");
            $(".ls").hide();
            $(".signUpgoogle").show();
            this.social = 2;
          }
        });
    // });

  }


  googleRegister() {
    let fullname = $("#fullname").val();
    let email = $("#email").val();
    let googleId = $("#googleId").val();
    let googleToken = $("#googleToken").val();
    if (fullname) {
      this.registerfullErrMsg = false;
      let list = {
        googleToken: googleToken,
        googleId: googleId,
        username: this.username,
        fullName: fullname,
        email: email,
        phoneNumber: this.phonenumber,
        password: this.password,
        profilePicUrl: this.profilePicUrl,
        signupType: 3,
        deviceType: 3,
        deviceId: "",
        pushToken: "pushToken",
        location: "",
        latitude: "",
        longitude: ""
      }

      this.loaderButton = true;
      setTimeout(() => {
        this.loaderButton = false;
      }, 3000);
      this.homeService.registerYelo(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.loaderButton = false;
            this._conf.setItem('authToken', res.response.authToken);
            this._conf.setItem('email', res.response.email);
            this._conf.setItem('username', this.username);
            this._conf.setItem('userId', res.response.userId);
            this._conf.setItem('profilePicUrl', this.profilePicUrl);
            this._conf.setItem('mqttId', res.response.mqttId);
            this.registerErrMsg = false;
            this.logDevice();
            this.registerValue();
            // this._conf.setItem("google", '1');
            this.missionService.confirmheaderRefresh(this.headerRefresh);
            this._router.navigate([`${this._router.url}`]);
            //this.location.replaceState('');
          } else {
            // this.registerErrMsg = res.message;
            this.languageCode === "en" ? this.registerErrMsg = res.message : this.languageCode === "fr" ? this.registerErrMsg= "Essayez encore" :
            this.registerErrMsg = "حاول مرة أخرى"
            this.loaderButtonfb = false;
            setTimeout(() => {
              this.registerErrMsg = false;
            }, 5000);
          }
        });
    } else {
      this.registerfullErrMsg = true;
      setTimeout(() => {
        this.registerfullErrMsg = false;
      }, 5000);
    }
  }


  onFailure(error) {
    ////console.log(error);
  }


  changeLocation() {
    var input = document.getElementById('sellLocate');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
          }
          if (addressObj.types[j] === 'country') {
            var state = addressObj.short_name;
          }
        }
      }
      if (City) {
        $("#cityS").val(City);
      } else if (state) {
        $("#cityS").val(state);
      } else {
        $("#cityS").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#latS").val(lat);
      $("#lngS").val(lng);
      $("#state").val(state);
      $("#locate").val(place.formatted_address);
    });
  }
  selectedLanguage(languageValue){
    this.browserDetectedLang = '';
    this._conf.setItem('browserDetectedLang',"4");
    ////console.log("the slected value :",typeof languageValue );
    this._conf.setItem('Language',languageValue)
    switch(languageValue){
     
      case "1":this.header = this._lang.engHeader1;
              //  this.header.language = "Français";
            break; 
      case "2": this.header = this._lang.engHeader;
                // this.header.language = "English";
                break;
      case "3":this.header = this._lang.engHeader2;
              //  this.header.language = "عربي";
             break;
    }
    this._router.navigate([`${this._router.url}`]);
    window.location.reload();
  }

  GoToHelp(){
    this.DeviceDetectorService.isMobile ? this.sideMenuOpen() :'';
    this._router.navigate([`./${this.languageCode}/help`])
  }
  goToSetting(){
    this.DeviceDetectorService.isMobile ? this.sideMenuOpen() :'';
    this._router.navigate([`./${this.languageCode}/settings`])
  }
   // card#292, issues : footer links are not redirecting into respective page, fixedBY: sowmyaSV, date: 10-1-20
    goToRespectivePage(caseValue){
      switch(caseValue){
        case 1: this._router.navigate([`${this.languageCode}/about`]);
                 break;
        case 2: this._router.navigate([`${this.languageCode}/howitworks`]);
                break;
        case 3: this._router.navigate([`${this.languageCode}/terms`]);
                break;
        case 4: this._router.navigate([`${this.languageCode}/privacy`]);
                break;
        case 5: this._router.navigate([`${this.languageCode}/help`]);
                break;
        case 6: this._router.navigate([`${this.languageCode}/trust`]);
                break;
          
      }
    }
    ngOnDestroy() {
    this.mySubscription ? this.mySubscription.unsubscribe() :'';
    }
}
