import { Component, OnInit,ElementRef,HostListener } from '@angular/core';
import { Router ,ActivatedRoute, RouterModule, NavigationEnd} from '@angular/router';
import { Location,LocationStrategy} from '@angular/common';
import { MissionService } from '../app.service';
import { HomeService } from '../home/home.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { LanguageService } from '../app.language';
import { Observable } from 'rxjs/Observable';
import { HttpClient,HttpParams } from "@angular/common/http";
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { concatStatic } from 'rxjs/operator/concat';
declare var $: any;
declare var google: any;
@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {

  param1: any;
  scrollEvent$:any;
  currentScrollPos : number;
  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    const number = window.scrollY;
    if (number > 500) {
        this.currentScrollPos = number;
    }
  }
  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private _conf: Configuration,
    private _router: Router,
    private _service: HomeService,
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private location : Location,
    private el: ElementRef,
    private httpClient: HttpClient,
    private locationStrategy : LocationStrategy
  ) {
    _missionService.missionCatName$.subscribe(
      catName => {
        ////////////console.log(catName, this.categoryVal)
        if (catName == "1") {
          $('.radioClass').removeAttr('checked');
          this.categoryVal = [];
          this.catList.forEach(x => {
            x.activeimg = false;
          })
          // this.allListPosts();
          // }
        } else {
          // this.categoryName = catName;
          // if(this.param1 === '/searchItem'){
          //   this.searchItem = 1;
          //   this.searchCategory();
          // }
          // else{
          //   this.searchItem = 0;
          // }
        }
      });
      this.scrollEvent$ = Observable.fromEvent(window, "scroll").subscribe(e => {
                           this.onWindowScroll(e);
      });
    }
  headerClose: string;
  token: string;
  offset = 0;
  langaugeCode: string ='es';
  limit = 40;
  allList: any = [];
  allListEmpty: any = [];
  catList: any;
  catSelectList = false;
  subCat: any;
  catSubFilter: any;
  loadMoreList = false;
  loadMoreListArrow = false;
  allListData = false;
  placelatlng: any;
  recentCity: any;
  selectedCategoryNodeId:any;
  listLength: any;
  queryParams : any = {};
  listsLength = false;
  offsetImg = 100;
  defaultImage = 'assets/images/lazyLoad.png';
  sellEmail: string;
  sellPhone: string;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  succesSeller = false;
  howPopupSuccess = false;
  listSell: any;
  signUpContent: boolean = true;

  filterPrice = false;
  categoryName: any;
  latlng: any;
  priceOrder = "lowToHigh";
  timeOrder = 0;
  distance = 3000;
  minPrice: any;
  maxPrice: any;
  filterScroll: any;
  postedWithin: any = 0;
  currency: string = '';
  DefualtCurrency:string = '';
  bannerShow = false;
  loadCateg = false;

  home: any;
  data: any;

  infowindow: any;
  map: any;
  place: any;

  shimmer: any = [];
  searchItem = 0;
  IpinfoData:any;

  ngOnInit() {
      // sessionStorage.clear();
      this._conf.removeItem("CountryCode");
      let selectedLang  = Number(this._conf.getItem("Language"));;
      if(selectedLang){
       switch(selectedLang){
  
         case 1: this.home = this._lang.engHome1;
                  this.langaugeCode = 'fr';
                  break; 
         case 2:this.home = this._lang.engHome;
                 this.langaugeCode = 'en';
                 break;
         case 3:this.home = this._lang.engHome2;
                 this.langaugeCode = 'ar';
                 $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                 break;
           
        }
      }
      else{
        this.home = this._lang.engHome;
        this.langaugeCode = 'en';
      }
  
      let urlPath = this._conf.getItem("pathname");
      if (urlPath) {
        let list = urlPath.split("/");
        var index = list.indexOf("website");
        if (index > -1) {
          list.splice(index, 1);
        }
        let url = list.join('/');
        this._router.navigate([url]);
        this._conf.removeItem("pathname");
      } else {
  
        this.seo();
        this.placeHolder();
  
        $(window).on('popstate', function () {
          $('.modal').modal('hide');
        });
  
        this._missionService.confirmheaderClose(this.headerClose);
        $(".location").text("Change Location");
        this.token = this._conf.getItem('authToken');
        let latlng = this._conf.getItem('latlng1');
        let cityArea = this._conf.getItem('cityArea');
        if (cityArea) {
          $("#cityAreai").val(cityArea);
        }
          // this.currentLocation();
          this.allListPosts();
        // }
        this.listGetCategories();
        setTimeout(() => {
          let latlng1 = this._conf.getItem('latlng');
          if (!latlng1 || !cityArea) {
            this.latLngNull();
          }
        }, 3000);
        // $.getJSON("https://restcountries.eu/rest/v1/alpha/in", (data) => {
        //    ////////////console.log("the currency code",data)
        // });
        this.currency = "DH";
        this.DefualtCurrency = 'DH';
      }
      // this.location.replaceState('');
    }
  
    placeHolder() {
      for (var i = 0; i < 12; i++) {
        this.shimmer.push(i);
      }
    }
  
    public currencyList = [
      { value: 'DH' , text: 'Dirham'},
    ]
  
  
    // latLngNull() {
    //   ////////console.log("latlngNull")
    //   sessionStorage.setItem("CountryCode",'MA');
    //   $.get("https://ipapi.co/json/").done(response=>{
    //   // sessionStorage.setItem("CountryCode",response.country);
    //   this.placelatlng = {
    //     // 'lat': ipList[0],
    //     // 'lng': ipList[1]
    //     'lat':response.latitude,
    //     'lng':response.longitude
      
    //   }
    //   this._conf.setItem('IpInfo',JSON.stringify(response) );
    //   }).fail(error=>{
    //     ////console.log("error ipapi")
    //   });
    //   let response :any;
    //   this._conf.getItem('IpInfo') ? response = JSON.parse(this._conf.getItem('IpInfo')) :'';
    //     if(response){
    //       // sessionStorage.setItem("CountryCode",response['country']);
    //       this.placelatlng = {
    //         'lat':response['latitude'],
    //         'lng':response['longitude']
          
    //       }
    //       this.IpinfoData  = response;
    //       this.recentCity = response['city'] + ", " + response['country'];
    //       this._conf.setItem('recentCity', this.recentCity);
    //       $("#cityAreai").val(this.recentCity);
    //       $(".location").text(this.recentCity);
    //     }
    //     else{
    //       this.placelatlng = {
    //         'lat': "33.5731",
    //          'lng': "-7.5898"
    //        }
    //        this._conf.setItem('latlng',JSON.stringify(this.placelatlng))
    //        $("#cityAreai").val("2, Casablanca 20250, Morocco");
    //        $(".location").text("2, Casablanca 20250, Morocco");
    //        $(".location").val("2, Casablanca 20250, Morocco");
    //        this._conf.setItem('recentCity', "2, Casablanca 20250, Morocco");
    //        this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
    //        this.recentCity = "2, Casablanca 20250, Morocco";
    //     }
       
    // }
    latLngNull() {
      sessionStorage.setItem("CountryCode",'MA');
      $.get("https://ipapi.co/json/").done(response=>{
        // sessionStorage.setItem("CountryCode",response.country);
        // // let ipList = response.loc.split(",");
        this.placelatlng = {
          'lat': response.latitude,
          'lng': response.longitude
        }
        // this.ipinfoData  = response;
        sessionStorage.setItem('IpInfo',JSON.stringify(response) );
        // let latlng1 = this._conf.getItem('latlng');
        // if (!latlng1) {
          ////console.log("the placeTAg :,", this.placelatlng);
          this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
          this.searchCategory();
        // }
        this.recentCity = response.city + ", " + response.country;
        this._conf.setItem('recentCity', this.recentCity);
        $("#cityAreai").val(this.recentCity);
        $(".location").text(this.recentCity);
    }).fail(error=>{
      this.placelatlng = {
        'lat': "33.5731",
         'lng': "-7.5898"
       }
       this._conf.setItem('latlng',JSON.stringify(this.placelatlng))
       $("#cityAreai").val("2, Casablanca 20250, Morocco");
       $(".location").text("2, Casablanca 20250, Morocco");
       $(".location").val("2, Casablanca 20250, Morocco");
       this._conf.setItem('recentCity', "2, Casablanca 20250, Morocco");
       this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
       this.recentCity = "2, Casablanca 20250, Morocco";
      //console.log("error ipapi");
      this.searchCategory();
    });
  
  
    }
  
    seo() {
      let list = {
        type: 1
      }
      this._service.seoList(list)
        .subscribe((res) => {
          if (res.code == 200) {
            this.data = res.data;
            // ////////////console.log("desc", res.data);
            if (this.data) {
              this._meta.addTag({ name: 'title', content: this.data.title })
              this._meta.addTag({ name: 'description', content: this.data.description })
              this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
            }
          }
        });
    }
  
    emptyFigure() {
      for (var i = 0; i < 20; i++) {
        this.allListEmpty.push(i);
      }
    }
  
    currentLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          var lat = position.coords.latitude;
          var lng = position.coords.longitude;
          this.placelatlng = {
            'lat': lat,
            'lng': lng
          }
          ////////////console.log("HomeLatLng", this.placelatlng);
          this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
          this._conf.setItem('Curlatlng', JSON.stringify(this.placelatlng));
          let latlng = this._conf.getItem('latlng');
          if (latlng) {
            this.allListPosts();
          }
          var latilng = new google.maps.LatLng(lat, lng);
          var geocoder = geocoder = new google.maps.Geocoder();
          geocoder.geocode({ 'latLng': latilng }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[1]) {
                for (var i = 0; i < results[1].address_components.length; i += 1) {
                  var addressObj = results[1].address_components[i];
                  for (var j = 0; j < addressObj.types.length; j += 1) {
                    if (addressObj.types[j] === 'locality') {
                      var city = addressObj.long_name;           
                    }
                    if (addressObj.types[j] === 'country') {
                      var country = addressObj.short_name;       
                    }
                  }
                }
                $("#cityAreai").val(city + ", " + country);
                $(".location").text(city + ", " + country);
                this._conf.setItem('recentCity', city + ", " + country);
                this._conf.setItem('currentCity',city + ", " + country);
              }
            }
          });
        }, this.geoError);
      } else {
        ////////////console.log("Geolocation is not supported by this browser.");
        // $("#cityAreai").val("Bengaluru, IN");
        // $(".location").text("Bengaluru, IN");
        // $(".location").val("Bengaluru, IN");
        // this._conf.setItem('recentCity', "Bengaluru, IN");
        // this._conf.setItem('currentCity', "Bengaluru, IN");
        $("#cityAreai").val("2, Casablanca 20250, Morocco");
        $(".location").text("2, Casablanca 20250, Morocco");
        $(".location").val("2, Casablanca 20250, Morocco");
        this._conf.setItem('recentCity', "2, Casablanca 20250, Morocco");
        this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
        this.placelatlng = {
          'lat': "33.573",
          'lng': "-7.5898"
        }
      }
    }
  
    geoError() {
      ////////////console.log("Geolocation is not supported by this browser.");
    }
  
    itemDetails(postId, name) {
      // let replace = name.split(' ').join('-');
      let replace = name.replace(/[^a-zA-Z0-9]/g, '-');
      // ////////console.log("itemDetails",replace);
      this._router.routeReuseStrategy.shouldReuseRoute = () => false;
      this._router.onSameUrlNavigation = 'reload';
      this._router.navigate([`./${replace}/${postId}`]);
    }
  
    public bgcolor = [
      { 'bgcolor': 'rgb(134, 176, 222)' },
      { 'bgcolor': 'rgb(255, 63, 85)' },
      { 'bgcolor': 'rgb(115, 189, 197)' },
      { 'bgcolor': 'rgb(232, 111, 91)' },
      { 'bgcolor': 'rgb(166, 196, 136)' },
      { 'bgcolor': 'rgb(245, 205, 119)' },
      { 'bgcolor': 'rgb(190, 168, 210)' },
      { 'bgcolor': 'rgb(252, 145, 157)' },
      { 'bgcolor': 'rgb(83, 143, 209)' },
      { 'bgcolor': 'rgb(209, 169, 96)' },
      { 'bgcolor': 'rgb(227, 74, 107)' }
    ]
  
    listGetCategories() {
      this.catSelectList = false;
      this.selectCatList = [];
      this._service.getCategoriesList(this.langaugeCode)
        .subscribe((res) => {
          if (res.code == 200) {
            this.catList = res.data;
            this.route.params.subscribe(params => {
              ////////////console.log('the params is : , ',params['name']);
              if(params['name']){
                this.param1 = params['name'];
                this.categoryName = params['name'];
                let index = this.catList.findIndex((item)=> item.name === params['name']);
                (index > -1) ? this.searchCategoryName(this.categoryName,index,0) : '';
              }
              else{
                this.route.queryParams.subscribe(params => {
                  ////////console.log("queryParams ->>>>>>>", params)
                  if(Object.getOwnPropertyNames(params).length  > 0 ){
                    this.queryParams = this.getQueryParams(params);
                    ////////////console.log("this.queryParams ->>>>>>>>",this.queryParams)
                    this.checkIfQueryParams(this.queryParams)
                  }
                });
              }
             });
          }
        });
    }
    hasLocation:any;
    checkIfQueryParams(queryParams){
      ////////console.log("dist --->", queryParams)
      // subCategory Filter
      // if(queryParams.hasOwnProperty('subCat')){
      //    let index = this.subCat.findIndex(item => item.subCategoryName === queryParams['subCat']);
      //    ////////////console.log("param queryParams", queryParams, index, queryParams['subCat']);
      //    index > - 1 ? this.subCategory(index): '';
      // }
       // distance filter
      if(queryParams['dist'] != undefined && queryParams.hasOwnProperty('dist')){
         this.distance = queryParams['dist'];
         ////////////console.log("dist --->", this.distance)
         this.milesDistance(this.distance);
        //  background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(0.8, rgb(92, 52, 163)), color-stop(0.8, rgb(197, 197, 197)));
      }
      //  // advanced Filter
      (queryParams['filter'] != undefined && queryParams.hasOwnProperty('filter'))?this.catSubFilter = queryParams['filter'] :'';
      // sort based onPrice
      (queryParams['sortPrice'] != undefined && queryParams.hasOwnProperty('sortPrice'))?this.priceOrder = queryParams['sortPrice'] :'';

       // filter based PostWithin
      (queryParams['postWithIn'] != undefined && queryParams.hasOwnProperty('postWithIn')) ? this.postedWithin = queryParams['postWithIn'] : '';
      // filter based minPrice
      (queryParams['minPrice'] != undefined && queryParams.hasOwnProperty('minPrice')) ? this.minPrice = queryParams['minPrice'] : '';
      // filter based maxPrice
      (queryParams['maxPrice'] != undefined && queryParams.hasOwnProperty('maxPrice'))? this.maxPrice = queryParams['maxPrice'] : '';
      // filter based on location 
      (queryParams['location'] != undefined && queryParams.hasOwnProperty('location'))?(this.latlng = JSON.parse(queryParams['location']),this._conf.setItem('latlng',queryParams['location'])): '';
      // filter based on currency 
      (queryParams['currency'] != undefined && queryParams.hasOwnProperty('currency')) ? this.currency = queryParams['currency'] :'';
      ////////////console.log("this.latlng",this.latlng);
      this.searchItem = 1;
      this.searchCategory();
    }
    loadCat() {
      this.loadCateg = !this.loadCateg;
    }
  
    LoaderShow: boolean = false;
    allListPosts() {
      this.bannerShow = false;
      this.LoaderShow = true;
      if (this.offset == 0) {
        this.allListData = false;
        setTimeout(() => {
          this.allListData = true;
        }, 3000);
      }
      this.allListData = true;
      this.LoaderShow = false;
      // this.listGetCategories();
    }
    LoaderMoreErr: Boolean = false;
    onScroll() {
      if (this.listLength == 40 && this.filterScroll == 0) {
        this.offset += 40;
        ////////////console.log("allScroll", this.listLength);
        // this.allListPosts();
      } else if (this.listLength == 40 && this.filterScroll == 1) {
        this.offset += 40;
        ////////////console.log("filterScroll", this.listLength);
        this.searchCategory();
      } else {
        ////////////console.log("error", this.listLength);
        this.LoaderMoreErr = true;
        setTimeout(()=>{
          this.LoaderMoreErr = false;
        },3000)
      }
    }
  
    emailValidation(value) {
      this.PEerror = false;
      if (value.length > 5) {
        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        if (regexEmail.test(value)) {
          this.email_Error = false;
          this.registerSave = true;
        } else {
          this.email_Error = true;
          this.registerSave = false;
        }
      } else {
        this.email_Error = false;
      }
    }
  
    mobileValidation(value) {
      this.PEerror = false;
      if (value.length > 5) {
        var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
        if (value.match(regexPhone)) {
          this.phone_Error = false;
          this.registerSave = true;
        } else {
          var val = value.replace(/([^+0-9]+)/gi, '')
          this.sellPhone = val;
          this.phone_Error = true;
          this.registerSave = false;
        }
        if (value.length == 10) {
          this.phone_Error = true;
          this.registerSave = false;
        }
      } else {
        this.phone_Error = false;
      }
    }
  
    sellHowSend() {
      ////////////console.log(this.sellEmail, this.sellPhone);
      if (this.sellEmail && this.sellPhone) {
        this.howPopupSuccess = false;
        this.phone_Error = false;
        this.email_Error = false;
        this.PEerror = "Please try anyone";
        setTimeout(() => {
          this.PEerror = false;
        }, 3000);
      } else if (this.sellEmail || this.sellPhone) {
        if (this.sellEmail) {
          this.sellSend(1);
        } else {
          this.sellSend(2);
        }
        this.PEerror = false;
      }
    }
  
    emptySell() {
      this.sellPhone = "";
      this.sellEmail = "";
      this.email_Error = false;
      this.phone_Error = false;
    }
  
    sellSend(val) {
      this.email_Error = false;
      this.phone_Error = false;
      if (val == 1) {
        this.listSell = {
          type: "1",
          emailId: this.sellEmail
        }
      } else {
        this.listSell = {
          type: "2",
          phoneNumber: this.sellPhone
        }
      }
      this._service.sellList(this.listSell)
        .subscribe((res) => {
          if (res.code == 200) {
            this.howPopupSuccess = true;
            this.emptySell();
          } else {
            this.PEerror = res.message;
          }
        });
    }
  
    // changeLocation() {
    //   var input = document.getElementById('changeLocationId');
    //   var autocomplete = new google.maps.places.Autocomplete(input);
  
    //   autocomplete.addListener('place_changed', function () {
    //     var place = autocomplete.getPlace();
    //     for (var i = 0; i < place.address_components.length; i += 1) {
    //       var addressObj = place.address_components[i];
    //       for (var j = 0; j < addressObj.types.length; j += 1) {
    //         if (addressObj.types[j] === 'locality') {
    //           var City = addressObj.long_name;
    //           // ////////////console.log(addressObj.long_name);
    //         }
    //         if (addressObj.types[j] === 'administrative_area_level_1') {
    //           var state = addressObj.short_name;
    //           // ////////////console.log(addressObj.short_name);
    //         }
    //       }
    //     }
    //     // ////////////console.log("cityArea", City + ", " + state);
    //     if (City) {
    //       $("#cityAreai").val(City + ", " + state);
    //     } else if (state) {
    //       $("#cityAreai").val(state);
    //     } else {
    //       $("#cityAreai").val(place.formatted_address);
    //     }
    //     let lat = place.geometry.location.lat();
    //     let lng = place.geometry.location.lng();
    //     $("#lati").val(lat);
    //     $("#lngi").val(lng);
    //   });
    // }
  
    // locationChange() {
    //   setTimeout(() => {
    //     this.offset = 0;
    //     this.searchCategory();
    //   }, 500);
    // }
  
    milesDistance(val) {
      var input = $(".slider_range").val();
      var value = (input - $(".slider_range").attr('min')) / ($(".slider_range").attr('max') - $(".slider_range").attr('min'));
      $(".slider_range").css('background-image',
        '-webkit-gradient(linear, left top, right top, '
        + 'color-stop(' + value + ', #6d3ec1), '
        + 'color-stop(' + value + ', #C5C5C5)'
        + ')'
      );
      this.offset = 0;
      if (val == 50) {
        val = 3000;
      }
      this.distance = val;
      if (val != 0) {
        this.navigateToFoo('dist',val,2);
        // this.searchCategory();
      } else {
        this.allListPosts();
      }
    }
  
    signUpContentToggle() {
      this.signUpContent = !this.signUpContent;
    }
  
    postedWithinList(val, index) {
      this.postedWithin = val;
      ////////////console.log("the postedWithIn", this.postedWithin)
      if (val == 0) {
        $('.radioClass').removeAttr('checked');
        this.categoryVal = [];
        this.catList.forEach(x => {
          x.activeimg = false;
        })
       
        $("html, body").animate({ scrollTop: 0 }, 500);
        if (index == 0) {
          $("#sideDetailsId").removeClass('active');
          $("body").removeClass("hideHidden");
          this.offset = 0;
          // this.pushUrlState(this.queryParams);
          // this.addRemoveQueries();
          this.listGetCategories();
        }
      } else if (val == 10) {
        this.clearAllData();
        this.categoryVal = [];
        this.catList.forEach(x => {
          x.activeimg = false;
        });
        this.subCat.forEach(x => {
          x.activeimg = false;
        });
      } else {
        this.offset = 0;
        this.postedWithin = val;
        this.navigateToFoo('postWithIn', val,5);
      }
      if(this.subCat){
        this.subCat.forEach(x => {
          x.activeimg = false;
        })
      }    
    }
  
  
  
    sortPrice(val) {
      this.offset = 0;
      this.priceOrder = val;
      this.navigateToFoo('sortPrice', val,4)
      ////////////console.log("the priceorder is :", this.priceOrder);
    }
  
    numberDitchit(value) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      var val = value.replace(/([^+0-9]+)/gi, '');
      $(".from1").val(val);
    }
  
    numberDitchit1(value) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      var val = value.replace(/([^+0-9]+)/gi, '');
      $(".to1").val(val);
    }
  
    minimumPrice(val) {
      this.offset = 0;
      this.minPrice = val;
      this.navigateToFoo('minPrice',val,6)
    }
  
    maximumPrice(val) {
      this.offset = 0;
      (val === 0) ? this.maxPrice = '100000' : this.maxPrice = val;
      this.navigateToFoo('maxPrice',val,7)
    }
  
    selectCurrency(val) {
      this.currency = val;
      this.navigateToFoo('currency', val,5 )
    }
  
    categoryVal = [];
    catArray = [];
    selectedQueries:any;
    selectCatList = [];

  filterItemsBy(val, i, filter){
    this.searchCategoryName(val, i, filter);
    this.cancelsubCat();
    this.clearAllData();
    } 
    clearAllData(){
        ////////////console.log("clear all data");
        $('.radioClass').removeAttr('checked');
        $('.inputCheckBox').removeAttr('checked');
        $("#myRange").val(0);
        $(".slider_range").css('background' ,'#d3d3d3');
        $(".froms").val('');
        $(".tos").val('');
        $('#fromToPricingInput').val('');
        $('.fromToPricingInput').val('');
        $('.inputBox').val('');
    }
    searchCategoryName(val, i, filter) {
      $("#nameCategory").val("");
      this.selectCatList = [];
      this.catList.forEach(x => {
        x.activeimg = false
      })
      this.catList[i].activeimg = true;
      this.selectCatList.push(this.catList[i]);
      this.catSelectList = true;
      this.selectedCategoryNodeId = this.catList[i].categoryNodeId;
      this.subCat = [];
      this.catSubFilter = "";
      this.subCategoryName = "";
      this.categoryName = this.catList[i].name;
      this.filterPrice = false;
      this.offset = 0;   
      this._router.navigate([`category/${this.categoryName}`],{
        queryParams: this.queryParams,
        queryParamsHandling: 'merge',
      })
      // this._service.getSubCategoriesList(this.catList[i].name,this.langaugeCode,this.selectedCategoryNodeId)
      //   .subscribe((res) => {
      //     this.subCat = res.data;
      //     // for Query params navigation
      //     this.route.queryParams.subscribe( params =>{
      //       ////////////console.log("the query params are getSubCategoriesList", params);
      //       this.selectedQueries  = this.getQueryParams(params);
      //       this.queryParams = this.getQueryParams(params);
      //       if(params){
      //           this.checkIfQueryParams(params);
      //           this.getFilteredData(filter);
      //       }
      //       else{
      //        this.getFilteredData(filter);
      //       }
      //     });
      //   });
    }
    getFilteredData(filter)
    {
      if (filter == 0) {
        this.searchItem = 1;
        this.searchCategory();
      }
    }
    getQueryParams(params){
      var resultantQuery = {...params};
      //   Object.defineProperties({...resultantQuery}, {
      //     query: {
      //       value: params,
      //       writable: true,
      //       configurable: true
      //   },
      // });
      ////////////console.log("resultantQuery --->", resultantQuery)
      return resultantQuery;
    }
    navigateToFoo(key, value, caseValue){
      // changes the route without moving from the current view or
      // triggering a navigation event,
      let catIsSelected : any;
      let queries : any;
      this.categoryName ? catIsSelected = `./category/${this.categoryName}` : catIsSelected = ``;
      this.route.queryParams.subscribe(params => {
           
          Object.getOwnPropertyNames(params).length > 0 ?  this.queryParams = this.getQueryParams(params):'';
          ////////////console.log("navigate to foo", this.queryParams)
          if(this.queryParams && this.queryParams.hasOwnProperty(key)){
            this.queryParams[key] = value;
          }
          else{
            switch(caseValue){
              case 1: queries = { [key] : value}
              break;
              case 2: queries = { [key] : value}
              // queryStr.set('dist',value );
              break;
              case 3: queries = { [key] : value}
              break;
              case 4: queries = { [key] : value}
              break;
              case 5: queries = { [key] : value}
              break;
              case 6: queries = { [key] : value}
              break;
              case 7: queries = { [key] : value}
              break;
              case 8: queries = { [key] : value}
              break;
            }
            this.queryParams = Object.assign(queries,this.queryParams);
          }
          // this.pushUrlState(queries);
        //   this.location.replaceState(
        //     this._router.createUrlTree([catIsSelected.split('?')[0]], {
        //       queryParams: this.queryParams,
        //      }).toString()
        //   );
        //   this._router.navigate([], {
        //     relativeTo: this.route,
        // });
      });
      ////////////console.log("this.location.path()", this.location.path(),this.queryParams)
    }
    stateParam = 0 ;
    pushUrlState(queries){
      ////////////console.log("pushUrlState --->",this.queryParams)
      this._router.navigate([], {
        queryParams: this.queryParams,
        queryParamsHandling: 'merge',
        relativeTo: this.route,
      });

}
    cancelCat(i) {
      this.catList.forEach(x => {
        x.activeimg = false
      });
      this.param1 ? this.location.replaceState(''): '';
      this.listGetCategories();
      if (!$("#sideDetailsId").hasClass("active")) {
        this.postedWithinList(0, 0);
        this.subCat = [];
      }
    }
  
    subCategoryName: any;
    subCategoryNodeId:any;
    subCategory(i) {
      this.subCategoryName = this.subCat[i].subCategoryName;
      this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
      this.subCat.forEach(x => {
        x.activeimg = false
      })
      this.subCat[i].activeimg = true;
      this.selectCatList && this.selectCatList.length > 1 ? this.selectCatList.splice(1, 1) :'';
      this.selectCatList.push(this.subCat[i]);
      var value = [];
      this.subCat[i].filter.forEach(x => {
        if (x.type == 2 || x.type == 4 || x.type == 6) {
          x.filterData = x.values.split(",");
          x.data = [];
          // x.value = split;
        }
        value.push(x);
      });
      this.catSubFilter = value;
      ////////////console.log("subcatgeory :, this.queryParams",this.queryParams);
      this.navigateToFoo('subCat',this.subCategoryName, 1 );
      // Object.getOwnPropertyNames(this.queryParams).length === 0 ? this.navigateToFoo('subCat',this.subCategoryName, 1 ) :'';
      // this._conf.setItem('SelectCatList', JSON.stringify(selectedList));
    }


    addRemoveQueries(){
      let catIsSelected : any;
      this.categoryName ? catIsSelected = `./category/${this.categoryName}` : catIsSelected = ``;
      ////////////console.log(":this.queryParams addRemoveQueries", this.queryParams)
      this.location.replaceState(
        this._router.createUrlTree([catIsSelected.split('?')[0]], {
          queryParams: this.queryParams,
         }).toString()
      );
      this._router.navigate([], {
        relativeTo: this.route,
    });
    }
  
    cancelsubCat() {
      ////////////console.log('the sub category list is 8:', this.subCat)
      this.subCat.forEach(x => {
        x.activeimg = false
      });
      this.subCategoryName = '';
      this.catSubFilter = "";
      this.selectCatList && this.selectCatList.length > 1 ? this.selectCatList.splice(1, 1) :'';
      if(this.queryParams && this.queryParams.hasOwnProperty('subCat')) Reflect.deleteProperty(this.queryParams,'subCat');
      ////////////console.log("this.queryParams cancelsubCat", this.queryParams);
      this.pushUrlState(this.queryParams);
      // this.addRemoveQueries();

    }
  
    cancelCats(i) {
      $("#nameCategory").val("");
      let len = this.selectCatList.length;
      if (i == 1) {
        this.cancelsubCat();
      } else {
        this.selectCatList.length === 1 ? (this.location.replaceState(''), this._router.navigate([''])): '';
        this.postedWithinList(0, 0);
        this.subCat =[];
      }
    }
  
  
    documentTextList(event, i, range) {
      // ////////////console.log("d", event.target.value.length)
      if (event.target.value.length > 0) {
        if (this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5) {
          if (range == 0) {
            this.catSubFilter[i].from = Number(event.target.value.replace(',','.'));
          } else {
            this.catSubFilter[i].to = Number(event.target.value.replace(',','.'));
          }
          this.catSubFilter[i].types = "range";
          this.catSubFilter[i].checked = true;
        } else if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
          var index = this.catSubFilter[i].data.findIndex(x => x.value == this.catSubFilter[i].filterData[range]);
          if (index > -1) {
            this.catSubFilter[i].data.splice(index, 1)
          } else {
            let list = {
              type: "equlTo",
              fieldName: this.catSubFilter[i].fieldName,
              value: this.catSubFilter[i].filterData[range]
            }
            this.catSubFilter[i].data.push(list);
          }
          if (this.catSubFilter[i].data && this.catSubFilter[i].data.length) {
            this.catSubFilter[i].checked = true;
            this.catSubFilter[i].typed = 4;
          } else {
            this.catSubFilter[i].checked = false;
          }
        } else {
          this.catSubFilter[i].data = event.target.value;
          this.catSubFilter[i].types = "equlTo";
          this.catSubFilter[i].checked = true;
        }
      } else {
        this.catSubFilter[i].checked = false;
      }
      this.navigateToFoo('filter',this.catSubFilter,3 )
    }
  
    distances: any;
    errMsg: any;
    loaderButton: any;
    distanceList(val) {
      this.distances = Number(val);
    }
    filterToggle() {
      $("html, body").animate({ scrollTop: 0 }, 500);
      $("#sideDetailsId").removeClass('active');
      $("body").removeClass("hideHidden");
      ////////////console.log("the selectcateList:",this.selectCatList )
      this.searchItem = 1;
      this.searchCategory();
    }
    
    searchCategory() {
      ////////////console.log("only filers ")
      this.errMsg = false;
      this.LoaderShow = true;
      setTimeout(() => {
        this.loaderButton = false;
        this.errMsg = false;
      }, 3000);
      var filData = [];
      var filDatas = [];
      if (this.catSubFilter) {
        this.catSubFilter.forEach(x => {
          if (x.checked == true && !x.typed) {      
            let list = {
              type: x.types,
              fieldName: x.fieldName,
              value: x.data || '',
              from: "",
              to: ""
            }
            if (x.types == "range") {
              list.from = x.from;
              list.to = x.to;
            }
            filData.push(list);
          } else {
            if (x.checked == true && x.typed == 4) {
              x.data.forEach(y => {
                let lisT = {
                  type: y.type,
                  fieldName: y.fieldName,
                  value: y.value,
                }
                filData.push(lisT);
              });
            }
          }
        })
      }
      this.bannerShow = true;
      // $("html, body").animate({ scrollTop: 150 }, 500);
      let ltlg = {
        lat: $("#lati").val(),
        lng: $("#lngi").val()
      }
      if ($("#lati").val()) {
        this._conf.setItem('latlng', JSON.stringify(ltlg));
      }
      let ciSt = $("#cityAreai").val();
      if (ciSt) {
        this.recentCity = ciSt;
      }
  
      let lists = {
        token: this._conf.getItem('authToken'),
        offset: this.offset,
        limit: this.limit,
        location: "",
        latitude: "",
        longitude: "",
        currency: this.currency,
        postedWithin: this.postedWithin,
        sortBy: this.priceOrder,
        distance: this.distance,
        price:JSON.stringify({
          currency: this.currency,
          from : parseInt(this.minPrice),
          to: parseInt(this.maxPrice),
  
        }),
        // minPrice: this.minPrice,
        // maxPrice: this.maxPrice,
        category: this.categoryName,
        subCategory: this.subCategoryName,
        filter: JSON.stringify(filData),
        langaugeCode: this.langaugeCode,
        categoryId:this.selectedCategoryNodeId,
      }
      if(this.subCategoryName){
        lists['subCategory'] = this.subCategoryName;
        lists['subcategoryId'] = this.subCategoryNodeId;
      } else{
        lists['subCategory'] = "";
      }
      let latlng :any;
      // if (this.latlng) {
        // let latlng = this.latlng;
        // lists.latitude = latlng.lat;
        // lists.longitude = latlng.lng;
        $(".location").val() === this._conf.getItem('currentCity') ?  latlng = JSON.parse(this._conf.getItem('Curlatlng')) : latlng = JSON.parse(this._conf.getItem('latlng'));
        lists.latitude = latlng.lat;
        lists.longitude = latlng.lng;
        ////////console.log("kbkjbnfb",$(".location").val() === this._conf.getItem('currentCity'),this.hasLocation)
      // }
      // else if(this.latlng){
      //   let latlng = this.latlng;
      //   lists.latitude = latlng.lat;
      //   lists.longitude = latlng.lng;
      // }
      if (this.recentCity) {
        lists.location = this.recentCity;
      } else {
        lists.location = this._conf.getItem('recentCity')
      }
      this.loaderButton = true;
      //////////console.log("the search filters value :", lists);
      // if(!this.categoryName && this.queryParams && Object.getOwnPropertyNames(this.queryParams).length  > 0){
      //   this._router.navigate(['filters'], {
      //         queryParams: this.queryParams,
      //         queryParamsHandling: 'merge',
      //   });
      // }
      this._service.searchCategoriesList(lists, this.searchItem,'')
        .subscribe((res) => {
          this.loaderButton = false;
          // this.allListData = true;
          let allList = res.data;
          if (res.code == 200) {
            this.LoaderShow = true;
            ////////////console.log("the allList for descending is 1:",this.allList );
            if(this.offset < 40){
              $("html, body").animate({ scrollTop: 0 }, 500);
            }
            else{
              $("html, body").animate({scrollTop: this.currentScrollPos},500);
            }
            $("#sideDetailsId").removeClass('active');
            $("body").removeClass("hideHidden");
             
            if (this.offset == 0) {
              this.allList = [];
            }
            for (var i = 0; i < allList.length; i++) {
              this.allList.push(res.data[i]);
            }
            this.allList = this.sortPostsBasedOnDates(this.allList);
            if (this.allList && this.allList.length > 0) {
              this.listsLength = false;
              // if(this.priceOrder === "priceDsc")
              // {
              //   this.allList.sort((a, b) =>{return b.price - a.price});
              // }
              // else if(this.priceOrder === "priceAsc"){
              //   this.allList.sort((a, b) =>{return a.price - b.price});
              // }
            } else {
              this.listsLength = true;
            }
            this.listLength = res.data.length;
            this.filterScroll = 1;
          } else {
            this.errMsg = res.message;
            this.listsLength = true;
            // this.showCasablancePosts();
          }
        },error=>{
          this.LoaderShow = false;
        });
        this.searchItem = 1;
        // this.offset = 0;
        this.postedWithin = 0;
    }
  
    setCurrentLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          var lat = position.coords.latitude;
          var lng = position.coords.longitude;
          this.placelatlng = {
            'lat': lat,
            'lng': lng
          }
          this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
          this.checkLocation();
        });
      }
      else{
        this.placelatlng = {
          'lat': "33.5731",
          'lng': "-7.5898"
        }

        $("#cityAreai").val("2, Casablanca 20250, Morocco");
        this._conf.setItem('recentCity',"2, Casablanca 20250, Morocco");
        $("#searchLocation1").val("2, Casablanca 20250, Morocco");
        $(".location").text("2, Casablanca 20250, Morocco");
        $(".location").val("2, Casablanca 20250, Morocco");
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this.checkLocation();
      }
    }
  
  
    checkLocation() {
      let latlng = this._conf.getItem("latlng");
      ////////////console.log(latlng)
      if (latlng) {
        this.latlng = JSON.parse(this._conf.getItem("latlng"));
        $("#locationMap").modal("show");
        this.continueMap();
      }
    }
    getCurrenctLocation(){
      $(".location").text('');
      return  this._conf.getItem('currentCity');
    }
  
    changeLocation(event) {
      var input = document.getElementById('changeLocationId');
      var autocomplete = new google.maps.places.Autocomplete(input);
  
      autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        for (var i = 0; i < place.address_components.length; i += 1) {
          var addressObj = place.address_components[i];
          for (var j = 0; j < addressObj.types.length; j += 1) {
            if (addressObj.types[j] === 'locality') {
              var City = addressObj.long_name;
              // ////////////console.log(addressObj.long_name);
            }
            if (addressObj.types[j] === 'administrative_area_level_1') {
              var state = addressObj.short_name;
              // ////////////console.log(addressObj.short_name);
            }
          }
        }
        // ////////////console.log("cityArea", City + ", " + state);
        if (City) {
          $("#cityAreai").val(City + ", " + state);
        } else if (state) {
          $("#cityAreai").val(state);
        } else {
          $("#cityAreai").val(place.formatted_address);
        }
        let lat = place.geometry.location.lat();
        let lng = place.geometry.location.lng();
        $("#lati").val(lat);
        $("#lngi").val(lng);
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
  
        ////////////console.log("the lat and longitude:",lat,lng )
      });
      event.type === "change" ? this.locationChange() :'';
    }
  
    locationChange1() {
      // ////////console.log("select location",$("#changeLocationId").val(),$("#changeLocationId").text())
      setTimeout(() => {
        this.offset = 0;
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        // this.searchCategory();
      }, 500);
    }
  
  
    locationChange() {
      setTimeout(() => {
        this.continueMap();
        //////////console.log('$("#cityAreai").val()',$("#cityAreai").val())
        let lat = $("#lati").val();
        let lng = $("#lngi").val();
        this.latlng = {
          lat: this.placelatlng.lat,
          lng: this.placelatlng.lng
        }
        ////console.log("latlng", this.latlng)
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this._conf.setItem('recentCity',$("#cityAreai").val())
      }, 500);
    }
  
  
    continueMap() {
      var latlng = this.placelatlng;
      var map = new google.maps.Map(document.getElementById('mapData'), {
        center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
        zoom: 15,
        disableDefaultUI: false,
        mapTypeControl: false
      });
  
      setTimeout(() => {
        google.maps.event.trigger(map, "resize");
        map.setCenter(latlng);
        var marker = new google.maps.Marker({
          map: map,
        });
      }, 300);
  
      this.infowindow = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latlng.lat, latlng.lng),
        map: map,
        anchorPoint: new google.maps.Point(0, -29),
        draggable: true
      });
  
      var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
      var geocoder = geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            // ////////////console.log(results[0].formatted_address)
            $(".addressArea").val(results[0].formatted_address);
            $("#cityAreai").val(results[0].formatted_address);
            $(".location").val(results[0].formatted_address);
            $(".location").text(results[0].formatted_address);
            // $(".areaName").val(results[0].formatted_address);
            // $(".arealat").val(latlng.lat);
            // $(".arealng").val(latlng.lng);
          }
        }
      });
  
  
      var geocoder = new google.maps.Geocoder();
      google.maps.event.addListener(marker, 'dragend', () => {
  
        geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $("#lati").val(marker.getPosition().lat());
              $("#lngi").val(marker.getPosition().lng());
              $(".location").text(results[0].formatted_address);
              $("#cityAreai").val(results[0].formatted_address);
              $(".addressArea").val(results[0].formatted_address);
              this.infowindow.setContent(results[0].formatted_address);
              this.infowindow.open(map, marker);
              this._conf.setItem('latlng',this.latlng);
              this._conf.setItem('recentCity',results[0].formatted_address);
              this.navigateToFoo('location', this.latlng,8);
              // this._conf.setItem('recentCity',results[0].formatted_address);
            }
          }
        });
      });
    }
    toggleEngOrSp: number = 0;
    youTubeLink(caseValue){
      ////////////console.log("the language selected:", caseValue)
      switch(caseValue){
        case 1: this.toggleEngOrSp = 1;
                break;
  
        case 2: this.toggleEngOrSp = 2;
                break;
      }
    }
    // show default  Casablanca, Morocco if current location items are not there

showCasablancePosts(){
  this.bannerShow = false;
  let list = {
    latitude: "33.5731",
    longitude: "-7.5898",
    offset: 0,
    limit: this.limit
  }

  if (this.offset == 0) {
    this.allListData = false;
    setTimeout(() => {
      this.allListData = true;
    }, 3000);
  }
  this._service.homeAllList(list)
    .subscribe((res) => {
      this.allListData = true;
      let allList = res.data;
      if (res.code == 200) {
        if (this.offset == 0) {
          this.allList = [];
        }
        let object = {};
        let result = [];

      this.allList.forEach(function (item,index) {
        let Item =  this.allList[index].postId
        if(!object[Item])
            object[Item] = 0;
          object[Item] += 1;
      })

      for (let prop in object) {
         if(object[prop] <= 2) {
             result.push(prop);
         }
      }
      this.allList = result;

        for (var i = 0; i < allList.length; i++) {
          this.allList.push(res.data[i]);
        }
        if (this.allList && this.allList.length > 0) {
          this.listsLength = false;
        } else {
          this.listsLength = true;
          this.allListData = true;
          // this.getCasablancaPlacePosts();
        }
        this.listLength = this.allList.length;
        this.filterScroll = 0;
      } else {
        this.listsLength = true;
        this.allListData = true;
        // this.getCasablancaPlacePosts();
      }
    });
}
      // format price based on dirham currency (DH)
      getDirhamCurrencyPrice(list){
        let price = parseInt(list.price).toFixed(2);
        price = this._conf.getDirhamCurrencyPrice(list);
        // let price = list.price.toFixed(2);
        // if(list.currency == 'DH' || list.currency == 'Dirham'|| list.currency == 'dirham' || list.currency == 'dh'){
        //     price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //     price = price.replace('.',',') ; 
        // }
        // if(list.currency == 'DH' ){
        //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //   price = price.replace('.',',') ; 
        // }
        // else if(list.currency == 'Dirham' ){
        //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //   price = price.replace('.',',') ; 
        // }
        // else if(list.currency == 'dirham' ){
        //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //   price = price.replace('.',',') ; 
        // }
        // else if(list.currency == 'dh' ){
        //   price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        //   price = price.replace('.',',') ; 
        // }
        // list.currency == 'DH' ? price.replace(',',' ').replace('.',',')  : '';
        return price;
      
      }
      //card#191 ,bug : sort the posts based on dates, fixedBy:sowmya
      sortPostsBasedOnDates(postList){
        //  let sortedList = provideBookingList.sort((a, b) => a.postedOn.unix() - b.postedOn.unix())
        // let  sortedList = provideBookingList.sort(function (a, b) {
        //   return  moment.utc(b.postedOn).diff(moment.utc(a.postedOn))
        // })
        let sortedList = postList.sort(function(a, b){
          const aDate = new Date(a.postedOn)
          const bDate = new Date(b.postedOn)
          return bDate.getTime() - aDate.getTime();
        });
        return sortedList;
      }

}
