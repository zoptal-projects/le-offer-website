import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { TermsService } from './terms.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';

declare var $:any;
@Component({
  selector: 'terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  constructor(
    private _missionService: MissionService, 
    private _serviceHome: HomeService, 
    private _meta: Meta, 
    private _conf: Configuration, 
    private _router: Router, 
    private _service: TermsService,
    private location:Location,
    private route: ActivatedRoute,
    private _lang: LanguageService) { }

  headerOpen: string;
  token:any;
  type = 1;
  terms:any;
  data:any;
  termsSub: any;
  lang:any;
  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    // this.termsSub = this._lang.engTerms;
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.termsSub = this._lang.engTerms1;
                this.lang ="fr";
                break;
        case 2: this.termsSub = this._lang.engTerms;
                this.lang = 'en';
                break;
        case 3: this.termsSub = this._lang.engTerms2;
                this.lang = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.termsSub = this._lang.engTerms;
      this.lang = 'en';
    }
    this.seo();
    this._missionService.confirmheaderOpen(this.headerOpen);
    this.token = this._conf.getItem('authToken');
    this.termsList();
    // this._router.navigate([this.lang,'terms']);   
    this.location.replaceState(`${this.lang}/terms`)       ;   
  }

  seo() {
    let list = {
      type: 5
    }
    this._serviceHome.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.data = res.data;
          // ////console.log("desc", data.description);
          this._meta.addTag({ name: 'title', content: this.data.title })
          this._meta.addTag({ name: 'description', content: this.data.description })
          this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
        }
      });
  }

  termsList(){  
    
    this._service.webContent(this.lang, this.type)
      .subscribe((res) => {
        if (res.code == 200) {
          this.terms = res.data[0].configData;
        }
      });
  }

}
