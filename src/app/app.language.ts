import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';


@Injectable()
export class LanguageService {

    constructor() { }

    //language support

  
    public engHeader =
        {
            selectLang:"Languages",
            // logo: "assets/images/logo.png", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "What are you looking for...", logOrCreateAccount: "Log in or create your account",
            // onTitle: "on Le-Offers", aboutUs: "About Us", trustNSafe: "Trust & Safety", howWorks: "How It Works", needHelp: "Need Help?", logOut: "Logout",
            // login: "Login", sellStuff: "Sell Your Stuff", myProfile: "My Profile", home: "Home", signUp: "Sign Up", backToTitle: "Back to Le-Offers",
            // logoPng: "assets/images/logo.png", postIn30Secs: "Post your items in as little as 30 seconds", downloadTopRated: "Download our top-rated iOS or Android app to post your item",
            // phoneNumber: "Phone Number", emailAddress: "Email Address", enterNumberCountryCode: "Please try entering your full number, including the country code.",
            // send: "Send", continueWebsite: "Continue Browsing Website", thankYou: "Thank you", takeFewMinutes: "it may take a few minutes for you to receive it.",
            // checkInternetConnection: "Please check your internet connection & try again.", ok: "Ok", sessionTimedOut: "Session Timed Out",
            // timeToTitle: "Buy and sell quickly, safely and locally. It’s time to Le-Offers!", titleTag: "Le-Offers", discover:"Discover",
            languageOptions:[{lang:"Français",checked:false,value:1,langCode:'fr'},
            {lang:"عربي",checked:false,value:3,langCode:'ar'},
            {lang:"English",checked:false,value:2,langCode:'en'},
            ],
            language:"Language",
            // Post_Product:"Post Product",addPhotos:"Add photos of your product",minimumPhotos:"At least one photo is mandatory", maximunPhotos:"Add upto more photo(optional)",maximumPhotoLength:"Product Image Maximum five...",what_selling:"What you are selling? ",Location:"Where is the product located?",price:"Price",
            // Negotiable:"Negotiable",exchange:"Willing to exchange",not_found:"No post found",Category:"Category",
            // selectCat:"Select Product Category",subCat:"Sub Category",selectSubCat:"Select Product Sub Category",
            // select:"Select",post:"Post",Help:"Help",connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",
            // continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",Sign_Up:"Sign Up",Log_In:"Log In",
            // byClicking:"By clicking on 'Sign up', you agree to the Yelo",terms:"Terms & Conditions",
            // policy:"Privacy Policy",and:"and",userName:"UserName",password:"PassWord",forgorPassword:"Forgot your password?",
            // haveAccount:"Don't have an account?",email:"Email",Submit:"Submit",verifyCode:"Enter the verification code",
            // enterOtp:"SMS The activation code is ",createAccount:"Create a new account",name:"Name",
            // fullName:"fullname is missing",
            // alreadyHaveAcc:"Already have an account?",
            productPrice:"Product Price",discover:"Discover",
            addTitle:"Add Title",
            description:"Description",whereSellItem:"Where you sell the item",IWant:"I Want...",
            havereceiveOtp:"Haven't received it yet?", resentOtp:'Resend Otp',
            other:"Other(see description)",forParts:"For Parts",used:"Used(normal wear)",
            openBox:"Open Box(never used)",reCon:"Reconditioned/Certified",new:"New(Never Used)",
            selectProductCon:"Select Product Condition",productCon:"Product Condition",from: "From",to:'TO',


            logo: "assets/images/logo.png", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "What are you looking for...", logOrCreateAccount: "Log in or create your account",
            onTitle: "on Le-Offers", aboutUs: "About Us", trustNSafe: "Trust & Safety", howWorks: "How It Works", needHelp: "Need Help?", logOut: "Logout",
            login: "Login", sellStuff: "Sell Your Stuff", myProfile: "My Profile", home: "Home", signUp: "Sign Up", backToTitle: "Back to Le-Offers",
            logoPng: "assets/images/logo.png", postIn30Secs: "Post your items in as little as 30 seconds", 
            downloadTopRated: "Download our top-rated iOS or Android app to post your item",
            phoneNumber: "Phone Number", emailAddress: "Email Address", enterNumberCountryCode: "Please try entering your full number, including the country code.",
            send: "Send", continueWebsite: "Continue Browsing Website", thankYou: "Thank you", 
            takeFewMinutes: "it may take a few minutes for you to receive it.",
            checkInternetConnection: "Please check your internet connection & try again.", ok: "Ok", 
            sessionTimedOut: "Session Timed Out",
            timeToTitle: "Buy and sell quickly, safely and locally. It’s time to Le-Offers!", titleTag: "Le-Offers",
            Post_Product:"Post Product",addPhotos:"Add photos of your product",minimumPhotos:"At least one photo is mandatory", maximunPhotos:"Add upto more photo(optional)",
            maximumPhotoLength:"Product Image Maximum five...",what_selling:"What you are selling? ",Location:"Where is the product located?",price:"Price",
            Negotiable:"Negotiable",exchange:"Willing to exchange",not_found:"No post found",Category:"Category",selectCat:"Select Product Category",subCat:"Sub Category",
            selectSubCat:"Select Product Sub Category",select:"Select",post:"Post",Help:"Help",connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",continueGoogle:"Continue with Google",
            useMail:"OR USE YOUR EMAIL",Sign_Up:"Sign Up",Log_In:"Log In",byClicking:"By clicking on 'Sign up', you agree to the Yelo",terms:"Terms & Conditions",policy:"Privacy Policy",and:"and",userName:"UserName",
            password:"PassWord",forgorPassword:"Forgot your password?",haveAccount:"Don't have an account?",email:"Email",Submit:"Submit",verifyCode:"Enter the verification code",
            enterOtp:"Enter the code that we sent to ",createAccount:"Create a new account",name:"Name",fullName:"fullname is missing",
            alreadyHaveAcc:"Already have an account?",imgLoad:'Uploading in progress...'
            
        }

    public engHome = {
        titleBuySellSafe: "Le-Offers Post, Chat, Buy and Sell.", to:'To',select:'Select', free:'Free',
        // bringCommunities: "The simpler",to:'To',
        // buySellFun: "way to buy and sell locally", cancel: "Cancel", reset: "Reset", location: "LOCATION",
        // categories: "Categories", showMore: "Show More", showLess: "Show Less", distance: "DISTANCE", max: "Max", km: "Km",
        // noCategory: "no category", sortBy: "SORT BY", newestFirst: "Newest First", closestFirst: "Closest First", lowToHighPrice: "Price: low to high",
        // highToLowPrice: "Price: high to low", postedWithin: "POSTED WITHIN", allListings: "All listings", last24Hours: "The last 24 hours",
        // last7Days: "The last 7 days", last30Days: "The last 30 days", filters: "Filters", price: "Price", currency: "Currency", from: "From",
        // saveFilters: "Save Filters", mike: "assets/images/mike.svg", miles: "Km", noOffers: "No offers found", sorryNotFound: "Sorry, we couldn't find what you were looking for",
        // downloadApp: "Download the app", startPostingOffers: "to start posting offers now", trustByMillions: "Trusted by Millions", blythe: "Blythe",
        // robin: "Robin", chris: "Chris", greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "Read Le-Offers Stories", getApp: "Get the App!",
        // joinMillions: "Join millions of loyal customers using the Le-Offers mobile app, the simpler way to buy and sell locally!",
        // googlePlayStore: "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/images/google-play.svg",
        // appStore: "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/images/app-store.svg", loveJob: "Love your Job!",
        // srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
        // seePositions: "See All Positions!", sendAppLink: "Send app link", emailPlaceHolder: "your-email@domain.com",
        // provideValidEmail: "Please provide a valid email address...", howPhone: "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/images/howPhone.png",
        // enterNumberCountryCode: "Please try entering your full number, including the country code.", recentRequestApp: "You have recently requested the app.",
        // minsToReceive: "It may take a few minutes for you to receive it.", wait5Mins: "Please wait about 5 minutes", makeOtherRequest: "to make another request.",
        // thankYou: "Thank you",changeLocation:"Change Location",subCat:"Sub Category",advanceFilter:"Advance Filter",setLocation:"Set Location",
        productCon:"Product Condition",selectProductCon:"Select Product Condition"
        ,other:"Other(see description)",forParts:"For Parts",used:"Used(normal wear)",
        openBox:"Open Box(never used)",reCon:"Reconditioned/Certified",new:"New(Never Used)",backHome:'Home',



        titleYealoSafe: "Le-Offers Post, Chat, Buy and Sell.", bringCommunities: "The simpler",
        YealoFun: "way to buy and sell locally", cancel: "Cancel", reset: "Reset", location: "LOCATION",
        categories: "Categories", showMore: "Show More", showLess: "Show Less", distance: "DISTANCE", max: "Max", km: "Km",
        noCategory: "no category", sortBy: "SORT BY", newestFirst: "Newest First", closestFirst: "Closest First", lowToHighPrice: "Price: low to high",
        highToLowPrice: "Price: high to low", postedWithin: "POSTED WITHIN", allListings: "All listings", last24Hours: "The last 24 hours",
        last7Days: "The last 7 days", last30Days: "The last 30 days", filters: "Filters", price: "Price", currency: "Currency", from: "From",
        saveFilters: "Save Filters", mike: "assets/images/mike.svg", miles: "Km", noOffers: "No offers found", sorryNotFound: "Sorry, we couldn't find what you were looking for",
        downloadApp: "Download the app", startPostingOffers: "to start posting offers now", trustByMillions: "Trusted by Millions", blythe: "Blythe",
        robin: "Robin", chris: "Chris", greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "Read Le-Offers Stories", getApp: "Get the App!",
        joinMillions: "Join millions of loyal customers using the Le-Offers mobile app, the simpler way to buy and sell locally!",
        googlePlayStore: "",
        appStore: "", loveJob: "Love your Job!",
        srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
        seePositions: "See All Positions!", sendAppLink: "Send app link", emailPlaceHolder: "your-email@domain.com",
        provideValidEmail: "Please provide a valid email address...", howPhone: "",
        enterNumberCountryCode: "Please try entering your full number, including the country code.", recentRequestApp: "You have recently requested the app.",
        minsToReceive: "It may take a few minutes for you to receive it.", wait5Mins: "Please wait about 5 minutes", makeOtherRequest: "to make another request.",
        thankYou: "Thank you",selectCurrency:'Select Any Currency',loadMore:'Lore More',
        changeLocation:"Change Location",subCat:"Sub Category",advanceFilter:"Advance Filter",setLocation:"Set Location",
        sorry:'Sorry! No products found.',weSorry:'We are showing stuffs around the world',searchRes:'Search Result'


    }

    public engAbout = {
        aboutUs: "About Us", howWorks: "How It Works", createCommunities: "We Bring People Together",
        comeTogetherSell: "to Discover Value", ourStory: "Our Story", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO",
        betterYealo: "A Better Way to Buy and Sell", largeMarket: "Le-Offers is the largest mobile marketplace in the U.S.",
        topApp: "Top 3 App", shopCategory: "In Shopping Category", appYear: "App of Year", geekWire: "Geekwire", billion14: "$14 Billion",
        transactionYear: "In Transactions this Year", million23: "23+ Million", downloads: "Downloads", titleNews: "Le-Offers In the News",



        // aboutUs: "About Us", howWorks: "How It Works", createCommunities: "We Bring People Together",
        // comeTogetherSell: "to Discover Value", ourStory: "Our Story", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO",
        // betterBuySell: "A Better Way to Buy and Sell", largeMarket: "Le-Offers is the largest mobile marketplace in the U.S.",
        // topApp: "Top 3 App", shopCategory: "In Shopping Category", appYear: "App of Year", geekWire: "Geekwire", billion14: "$14 Billion",
        // transactionYear: "In Transactions this Year", million23: "23+ Million", downloads: "Downloads", titleNews: "Le-Offers In the News",
    }

    public engchangePassword = {
        // changePassTitle: "Change Password - Le-Offers", changePassword: "Change Password", currentPassword: "Current Password",
        // newPasswordAgain: "New Password (again)", newPassword: "New Password", submit: "Submit", cancel:'Cancel',

        changePassTitle: "Change Password - Le-Offers", changePassword: "Change Password", currentPassword: "Current Password",
        newPasswordAgain: "New Password (again)", newPassword: "New Password", submit: "Submit", cancel:'Cancel'
    }

    public engDiscussions = {
        titleBuySellSimple: "Le-Offers - Buy. Sell. Simple.", 
        // myOffers: "My Offers", keyBoard: "Key board", twoHundred: "200", $: "$",
        // Jeffery: "Jeffery", ago: "ago", month: "month", one: "1", hey: "Hey", replyToss: "Reply to ss", send: "Send", report: "Report", sabari: "Sabari",
        // whyreport: "Why do you want to report this item?", prohibited: "It's prohibited on Le-Offers", offensive: "It's offensive to me",
        // notReal: "It's not a real post", duplicate: "It's a duplicate post", wrongCategory: "It's in the wrong category", scam: "It may be a scam",
        // stolen: "It may be stolen", other: "Other", additionalNote: "Additional note (optional)", writeNotehere: "Please write your note here...",
        // done: "Done",

        titleYealoSimple: "Le-Offers - Buy. Sell. Simple.", myOffers: "My Offers", keyBoard: "Key board", twoHundred: "200", $: "$",
        Jeffery: "Jeffery", ago: "ago", month: "month", one: "1", hey: "Hey", replyToss: "Reply to ss", send: "Send", report: "Report", sabari: "Sabari",
        whyreport: "Why do you want to report this item?", prohibited: "It's prohibited on Le-Offers", offensive: "It's offensive to me",
        notReal: "It's not a real post", duplicate: "It's a duplicate post", wrongCategory: "It's in the wrong category", scam: "It may be a scam",
        stolen: "It may be stolen", other: "Other", additionalNote: "Additional note (optional)", writeNotehere: "Please write your note here...",
        done: "Done",

    }

    public engEmailVerify = {
        // verifyEmailTitle: "Verify Email - Le-Offers", emailVerify: "Email Verify",
        // emailProvide: "Please provide a valid email address...",
        // submit: "Submit",
        congratulationsVerified:'Congratulations, you confirmed your email address',

        verifyEmailTitle: "Verify Email - Le-Offers", emailVerify: "Email Verify",
        emailProvide: "Please provide a valid email address...",
        submit: "Submit",


    }

    public engFooter = {
        logoBottom: "assets/images/logo_bottom.png", YealoSafe: "Post, Chat, Buy and Sell", downloadApp: "Download the app",
        footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "About Us", howWorks: "How It Works", terms: "Terms",
        privacy: "Privacy", help: "Help", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",


        // logoBottom: "assets/images/logo.png", buySellSafe: "Post, Chat, Buy and Sell", downloadApp: "Download the app",
        // footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "About Us", howWorks: "How It Works", terms: "Terms",
        // privacy: "Privacy", help: "Help", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",

    }

    public engHowItWorks = {
        // aboutUs: "About Us", howItWorks: "How It Works", mobileCommerce: "Buying and Selling Made Easy",
        // startlessIn30Secs: "Get Started in Less Than 30 Seconds", discoverLocally: "Discover Locally",
        // browseMillionListings: "Browse millions of listings to find amazing items nearby", chatInstantly: "Chat Instantly",
        // messageUsersAnonymously: "Message users anonymously and securely through the Le-Offers app. Our newsfeed allows you to connect with buyers, sellers and other people you know.",
        // sellSimpleVideo: "Sell Quickly With Image", easilyPost: "Easily post your items for sale with just a simple snap chat from your smartphone.",
        // buildTrustCommunity: "Building a Trusted Community", userProfiles: "User Profiles", knowMoreBuyers: "Know more about buyers and seller before you engage",
        // userRatings: "User Ratings", checkBuyer: "Check buyer and seller ratings, and give ratings when you transact", realTimeAlerts: "Real-time Alerts",
        // getNotified: "Get notified instantly on your phone when a buyer or seller contacts you",
        // titleIs: "Le-Offers is", toDownload: "to download", free: "free",
        // downloadTopRated: "Download our top-rated iOS or Android app to get started today!", howItWorksPlayStore: "assets/images/playstore.svg",
        // howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Coming Soon!!!", thankYou: "Thank you",
        // fewMinsReceive: "it may take a few minutes for you to receive it.",



        aboutUs: "About Us", howItWorks: "How It Works", mobileCommerce: "Buying and Selling Made Easy",
        startlessIn30Secs: "Get Started in Less Than 30 Seconds", discoverLocally: "Discover Locally",
        browseMillionListings: "Browse millions of listings to find amazing items nearby", chatInstantly: "Chat Instantly",
        messageUsersAnonymously: "Message users anonymously and securely through the Le-Offers app. Our newsfeed allows you to connect with buyers, sellers and other people you know.",
        sellSimpleVideo: "Sell Quickly With Image", easilyPost: "Easily post your items for sale with just a simple snap chat from your smartphone.",
        buildTrustCommunity: "Building a Trusted Community", userProfiles: "User Profiles", knowMoreBuyers: "Know more about buyers and seller before you engage",
        userRatings: "User Ratings", checkBuyer: "Check buyer and seller ratings, and give ratings when you transact", realTimeAlerts: "Real-time Alerts",
        getNotified: "Get notified instantly on your phone when a buyer or seller contacts you",
        titleIs: "Le-Offers is", toDownload: "to download", free: "free",
        downloadTopRated: "Download our top-rated iOS or Android app to get started today!", howItWorksPlayStore: "assets/images/playstore.svg",
        howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Coming Soon!!!", thankYou: "Thank you",
        fewMinsReceive: "it may take a few minutes for you to receive it.",

    }

    public engItem = {
        // titleSimpleWayToBuy: "Le-Offers is a simpler way to buy and sell locally. Get the free app.", itemAppStore: "assets/images/appstore.svg",
        // itemPlayStore: "assets/images/playstore.svg", previous: "Previous", next: "Next", postRemoved: "post is removed", posted: "Posted",
        // ago: "ago", report: "Report", description: "Description", comments: "Comments", condition: "Condition", viewAll: "view all",
        // typeYourMessage: "Add Review...", youHave: "You have", one120: "120", characterLeft: "character left.",
        // postComment: "Post Comment", myOtherOffers: "Related Post", price: "PRICE", $: "$", makeOffer: "Make Offer", follow: "Follow",
        // following: "Following", unFollow: "Un-Follow", watch: "Favourite", watching: "Favourite", unWatch: "Un-Favourite", seller: "About the seller",
        // approximationProtectPrivacy: "Approximation to protect seller's privacy", followers: "Followers", readyBuyHit: "Ready to buy? Hit the Buy button.",
        // offer: "Offer", notSure: "Not sure?", ask: "Ask", forMoreInfo: "for more info", offerSent: "Offer Sent!", orBetterDownloadApp: "Or better yet download the app. It's faster!",
        // getApp: "Get the app!",
        // itemGooglePlay: "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/images/google-play.svg",
        // itemAppStore2: "https://d2qb2fbt5wuzik.cloudfront.net/yelowebsite/images/app-store.svg",  soldBy: "Sold by", whyReport: "Why do you want to report this item?",
        // additionalNote: "Additional note (optional)", pleaseWriteNote: "Please write your note here...", done: "Done", reported: "Reported",
        // thanksTakingTime: "Thank you for taking time to let us know.", send: "Send", nowFollowing: "You're now following", offers: "Offers",
        // follower: "Follower", comingSoon: "Coming Soon!!!", thankYou: "Thank you", fewMinsReceive: "it may take a few minutes for you to receive it",
        // enterQuestionPlaceHolder: "Enter your question here...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        // settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        // settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        // settingspayOff: "assets/images/paypal_off.svg",Sold:"Sold",follwo:"Followed by",Location:"Location",
        // Negotiable:"Negotiable",notNegatiable:"Not Negotiable",edit:"Edit",delete:"Delete",
        // Details:"Details",addReview:"Add Review",noOfferFound:"No Offer Found",
        // delteListing:"Are you sure want to delete your listing?",Cancel:"Cancel",editProduct:"Edit Product Listing",
        // addPhotos:"Add photos of your product",minimumPhotos:"At least one photo is mandatory", 
        // maximunPhotos:"Add upto more photo(optional)",maximumPhotoLength:"Product Image Maximum five...",
        // what_selling:"What you are selling? ",WhrerLocation:"Where is the product located?",negotiable:"Negotiable",exchange:"Willing to exchange",not_found:"No post found",Category:"Category",
        // selectCat:"Select Product Category",subCat:"Sub Category", emailNotVerify:"Email Not Verified", googleCon:"Google+ Connected", googleDisCon:"Google+ Not Connected", 
        // fbCon:"Facebook Connected", fbNotCon:"Facebook Not Connected",noData:'no data',
        // selectSubCat:"Select Product Sub Category",select:"Select",update:"Update", 
        details:"Details",post:"Post", addTitle:"Add Title",free:'Free',imgLoad:'Uploading in progress...',
        description:"Description",
        other:"Other(see description)",forParts:"For Parts",used:"Used(normal wear)",
        openBox:"Open Box(never used)",reCon:"Reconditioned/Certified",new:"New(Never Used)",
        selectProductCon:"Select Product Condition",  productCon:"Product Condition",from: "From",to:'TO',googleVerify:'Google+ Verified',
        shareLink:'Share Url Here',Swap:'Swap',SwapFor:'Swap For',copyUrl:'Copy Url',UrlCopied:'Url Is Copied Successfully',
        Home:'Home',
        titleSimpleWayToBuy: "Le-Offers is a simpler way to buy and sell locally. Get the free app.", itemAppStore: "assets/images/appstore.svg",
        itemPlayStore: "assets/images/playstore.svg", previous: "Previous", next: "Next", postRemoved: "post is removed", posted: "Posted",
        ago: "ago", report: "Report", comments: "Comments", condition: "Condition", viewAll: "view all",
        typeYourMessage: "Add Review...", youHave: "You have", one120: "120", characterLeft: "character left.",
        postComment: "Post Comment", myOtherOffers: "Related Post", price: "PRICE", $: "$", makeOffer: "Make Offer", follow: "Follow",
        following: "Following", unFollow: "Un-Follow", watch: "Favourite", watching: "Favourite", unWatch: "Un-Favourite", seller: "About the seller",
        approximationProtectPrivacy: "Approximation to protect seller's privacy", followers: "Followers", readyBuyHit: "Ready to buy? Hit the Buy button.",
        offer: "Offer", notSure: "Questions?", ask: "Ask the seller", forMoreInfo: "for more info", offerSent: "Offer Sent!", orBetterDownloadApp: "Or better yet download the app. It's faster!",
        getApp: "Get the app!",
        itemGooglePlay: "",
        itemAppStore2: "", soldBy: "Sold by", whyReport: "Why do you want to report this item?",
        additionalNote: "Additional note (optional)", pleaseWriteNote: "Please write your note here...", done: "Done", reported: "Reported",
        thanksTakingTime: "Thank for you time.", send: "Send", nowFollowing: "You're now following", offers: "Offers",
        follower: "Follower", comingSoon: "Coming Soon!", thankYou: "Thank you", fewMinsReceive: "it may take a few minutes for you to receive it",
        enterQuestionPlaceHolder: "Enter your question here...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        settingspayOff: "assets/images/paypal_off.svg",
        Sold:"Sold",follwo:"Followed by",Location:"Location",
        Negotiable:"Negotiable",notNegatiable:"Not Negotiable",edit:"Edit",delete:"Delete",
        Details:"Details",addReview:"Add Review",noOfferFound:"No Offer Found",
        delteListing:"Are you sure want to delete your listing?",Cancel:"Cancel",
        editProduct:"Edit Product Listing",addPhotos:"Add photos of your product",
        minimumPhotos:"At least one photo is mandatory", maximunPhotos:"Add upto more photo(optional)",
        maximumPhotoLength:"Product Image Maximum five...",what_selling:"What you are selling? ",exchange:"Willing to exchange",
        not_found:"No post found",Category:"Category",selectCat:"Select Product Category",subCat:"Sub Category",selectSubCat:"Select Product Sub Category",select:"Select",update:"Update"
    }

    public engLogin = {
        // formBackground: "assets/images/formBackground.png", loginTo: "Login To", loginLogo: "assets/images/logo.png", userName: "USER NAME",
        // passWord: "PASSWORD", byClicking: "By Clicking on “Login” or ”Connect with Facebook” you shall agree to the Le-Offers",
        // termsService: "Terms of Service", privacyPolicy: "Privacy Policy", login: "Login", notRegistered: "Not registered?",
        // signUp: "Sign up", forgotPassword: "Forgot password?", reset: "Reset", and: "and", registerErrMsg: "fdgfdgfdg",
        // connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",
        // alreadyHaveAcc:"Already have an account?",logFaceBook:"Login With Facebook?",faceBookLog:"Facebook Login",

        formBackground: "assets/images/formBackground.png", loginTo: "Login To", loginLogo: "assets/images/logo.png", userName: "USER NAME",
        passWord: "PASSWORD", byClicking: "By Clicking on “Login” or ”Connect with Facebook” you shall agree to the Le-Offers",
        termsService: "Terms of Service", privacyPolicy: "Privacy Policy", login: "Login", notRegistered: "Not registered?",
        signUp: "Sign up", forgotPassword: "Forgot password?", reset: "Reset", and: "and", registerErrMsg: "fdgfdgfdg",
connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",
alreadyHaveAcc:"Already have an account?",logFaceBook:"Login With Facebook?",faceBookLog:"Facebook Login",
    }

    public engLogout = {
    }

    public engMemberProfile = {
        profile: "Profile:", title: "Le-Offers", offers: "Offers", $: "$", seller: "About the seller", followers: "Followers", follow: "Follow",
        following: "Following", unFollow: "Un-Follow", bangaluru: "Bangaluru", follower: "Follower",
        followBy:"Follwed By",sellerPolicy:"Approximation to protect seller's privacy",

        // profile: "Profil:", title: "Le-Offers", offers: "Offers", $: "$", 
        // seller: "A propos du vendeur", followers: "Suiveurs", follow: "Suivre",
        // following: "Vous Suivez", unFollow: "Se désabonner", bangaluru: "Bangaluru", follower: "Follower",
        // sellerPolicy:"Rapprochement pour protéger la vie privée du vendeur"
    }

    public engneedHelp = {
        support: "Help - Le-Offers",
    }

    public engPasswordReset = {
        forgotPasswordTitle: "Forgot Password - Le-Offers", newPasswordEnter: "ENTER NEW PASSWORD", newPassword: "New Password",
        newPasswordAgain: "New Password (again)", success: "Success...", submit: "Submit",

        // forgotPasswordTitle: "Mot de passe oublié - Le-Offers", newPasswordEnter: "ENTREZ UN NOUVEAU MOT DE PASSE", newPassword: "Nouveau mot de passe",
        // newPasswordAgain: "Répétez le nouveau mot de passe", success: "Succès...", submit: "Soumettre",

    }

    public engPrivacy = {
        privacyPolicyTitle: "Privacy Policy - Le-Offers",
    }

    public engRegister = {
        // formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.png", name: "NAME", fullNameMissing: "fullname is missing",
        // userName: "USER NAME", email: "EMAIL", phoneNumber: "PHONE NUMBER", password: "PASSWORD",
        // connectFBLogin: "By Clicking on “Login” or ”Connect with Facebook” you shall agree to the Le-Offers", termsService: "Terms of Service",
        // privacyPolicy: "Privacy Policy", and: "and", signUp: "SIGN UP", alreadyRegistered: "Already registered?", logIn: "Log In",
        // signUpWith: "Sign Up with",connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",
        // continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",
        // alreadyHaveAcc:"Already have an account?",logFaceBook:"Sign up With Facebook?",faceBookLog:"Facebook Sign up",
        // Signup:"Sign up",createAccount:"Create a new account",


        formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.png", name: "NOM", fullNameMissing: "nom complet manquant",
        userName: "NOM UTILISATEUR", email: "EMAIL", phoneNumber: "NUMÉRO DE TÉLÉPHONE", password: "MOT DE PASSE",
        connectFBLogin: "En cliquant sur “S'identifier” ou ”Connecter avec Facebook”, vous acceptez les conditions générales de Le-Offers.", termsService: "Conditions d'utilisation",
        privacyPolicy: "Politique de confidentialité", and: "et", signUp: "S'INSCRIRE", alreadyRegistered: "Déjà enregistré?", logIn: "S'identifier",
        signUpWith: "Se connecter avec",
        connectWith:"CONNECTER RAPIDEMENT AVEC",continueFaceBook:"Continuer avec Facebook",continueGoogle:"Continuer avec Google",useMail:"OU UTILISEZ VOTRE EMAIL",
        alreadyHaveAcc:"Vous avez déjà un compte?",logFaceBook:"Inscrivez-vous avec Facebook?",faceBookLog:"Inscription Facebook",Signup:"S'inscrire",createAccount:"Create a new account",
    }
    public engResetPassword = {
        passwordResetTitle: "Password reset - Le-Offers", startBuyCrypto: "start buying and selling safe with crypto!",
        formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.png", forgotPassword: "Forgot Password",
        recoverPassword: "Enter your email to recover your password", receiveLink: "You will receive a link to reset your password",
        email: "EMAIL", submit: "SUBMIT", resetPasswordlogoBottom: "assets/images/logo.png",


        // passwordResetTitle: "Réinitialisation du mot de passe - Le-Offers", startBuyCrypto: "commencez à acheter et à vendre en toute sécurité avec crypto!",
        // formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.png", forgotPassword: "Mot de passe oublié",
        // recoverPassword: "Entrez votre email pour récupérer votre mot de passe", receiveLink: "Vous recevrez un lien pour réinitialiser votre mot de passe",
        // email: "EMAIL", submit: "SOUMETTRE", resetPasswordlogoBottom: "assets/images/logo.png",

    }

    public engSell = {
        // sellTitle: "Le-Offers - Buy. Sell. Simple.", browse: "Browse", noCategory: "no category", terms: "Terms", privacy: "Privacy",
        // titleCopyRights: "© 2019 Le-Offers, Inc.", sellStuff: "Sell your stuff",


        sellTitle: "Le-Offers - Acheter. Vendre. Simple.", browse: "Browse", noCategory: "no category", terms: "Terms", privacy: "Privacy",
        titleCopyRights: "© 2019 Le-Offers", sellStuff: "Vendez vos articles",

    }
    public engSettings = {
        // settingsTitle: "Account Settings - Le-Offers", verifiedWith: "Verified With", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        // settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        // settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        // settingspayOff: "assets/images/paypal_off.svg", posts: "Posts", followers: "Followers", following: "Following", selling: "SELLING",
        // sold: "SOLD", favourites: "FAVOURITES", noListYet: "NO LISTINGS (YET!)", $: "$",
        // noAds:"No Ads Yet",snapSell:"Snap and post in 30 second to sell stuff!",
        // noSold:"No Sold Yet",fountFav:"Found Your Favourite Yet",postAds:"Ads you love & make as 'favourite' will appear here",
        // exchange:"you have exchanged your",for:"for",onSale:"on sale",follwing:"No Following Yet",
        // internetErr:"Internet Error",internetSet:"Please check your internet settings", close:"Close",
        // buildTrust:"Build Trust",cancel:"Cancel",payPal:"Connect Paypal",
        // havePaypal:"If you already have a Paypal.me link add it here",Example:"Example",Save:"Save",
        // editProfile:"Edit Profile",tapPhoto:"Tap the photo to change it",
        // imageSize:"Images must be in PNG or JPG format and under 5mb",Name:"Name",userName:"User Name", 
        // bio:"Bio",website:"Website",Location:"Location",privateInfo:"Private Information",emailAdd:"Email Address",
        // phoneNumber:"Phone Number",saveChange:'Save Changes',
        // Gender:"Gender", male:"Male", female:"Female",changePw:"Change Password", Cancel:"Cancel",
        edit:"edit", emailNotVerify:"Email Not Verified", googleCon:"Google+ Connected", 
        googleDisCon:"Google+ Not Connected", fbCon:"Facebook Connected", fbNotCon:"Facebook Not Connected",emailVeirfy:'Email Verified',
        payPalVerify:'paypal Verified', payPalNot:'paypal Not Verified', follower:'Following',saveChange:'Save Changes',


        settingsTitle: "Account Settings - Le-Offers", verifiedWith: "Verified With", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        settingspayOff: "assets/images/paypal_off.svg", posts: "Posts", followers: "Followers", following: "Following", selling: "SELLING",
        sold: "SOLD", favourites: "FAVOURITES", noListYet: "NO LISTINGS (YET!)", $: "$",
        noAds:"No Ads Yet",snapSell:"",
        noSold:"No Sold Yet",fountFav:"Found Your Favourite Yet",postAds:"",exchange:"ou have exchanged your",for:"for",onSale:"on sale",follwing:"No Following Any One Yet",internetErr:"Internet Error",
        internetSet:"Please check your internet settings", close:"Close",buildTrust:"Build Trust",cancel:"Cancel",payPal:"Connect Paypal",
        havePaypal:"If you already have a Paypal.me link add it here",Example:"Example",Save:"Save",
        editProfile:"Edit Profile",tapPhoto:"Tap the photo to change it",imageSize:"Images must be in PNG or JPG format and under 5mb",Name:"Name",
        userName:"User Name", bio:"Bio",website:"Website",Location:"Location",privateInfo:"Private Information",emailAdd:"Email Address",phoneNumber:"Phone Number",
        Gender:"Gender", male:"Male", female:"Female",changePw:"Change Password",


    }

    public engTerms = {
    }

    public engTrust = {
        // trustTitle: "Trust - Le-Offers", trust: "Trust", trustworthiness: "The most valuable currency",
        // communityWorld: "in our marketplace is trust", buildLocalMarket: "We’re building a local marketplace where the",
        // wellBeing: "well-being of buyers and sellers comes first", obsessingText: "We want Le-Offers to be a place where buying and selling can be more rewarding. We'll keep obsessing over every detailof the experience so our buyers and sellers can connect with more confidence. And we'll keep holding ourselvesand our community to high standards",
        // trustNeighbours: "Earn trust from each other", careVigilant: "Get to know other users", userProfiles: "User Profiles",
        // profileOpportunity: "Your profile is your opportunity to introduce yourself to other members of the community",
        // verificationID: "ID Verification", secureIdentity: "We securely validate your identity using your state ID and Facebook profile",
        // userRatings: "User Ratings", seeHowMany: "See how many completed transactions users have and check out their average rating",
        // appMessaging: "In-App Messaging", securelyCommunicate: "Securely communicate with buyers and sellers without giving away personal information",
        // winningGame: "Learn buying and selling best practices", buyingTips: "Buying Tips",
        // coverBasics: "Cover the basics of how to successfully inspect and purchase items from sellers on Le-Offers",
        // sellingTips: "Selling Tips", overBasics: "over the basics of how to successfully engage with buyers on Le-Offers",
        // letUsKnow: "Let us know if we can help", customerExperts: "Customer Care Experts",
        // hereToHelp: "We are here to help solve problems and investigate issues when they arise. Please email for assistance",
        // workClosely: "We work closely with our Law Enforcement Partners to ensure incidents requiring further investigation are handledaccordingly",
        // assistLaw: "To learn more about how we assist Law Enforcement Officers, please visit ourLaw Enforcement Resource Page",
        // haveQuestions: "Still have questions?", visitOur: "Visit our", helpCenter: "Help Center", learnMore: "to learn more",



        trustTitle: "Trust - Le-Offers", trust: "Trust", trustworthiness: "The most valuable currency",
        communityWorld: "in our marketplace is trust", buildLocalMarket: "We’re building a local marketplace where the",
        wellBeing: "well-being of buyers and sellers comes first", obsessingText: "We want Le-Offers to be a place where buying and selling can be more rewarding. We'll keep obsessing over every detailof the experience so our buyers and sellers can connect with more confidence. And we'll keep holding ourselvesand our community to high standards",
        trustNeighbours: "Earn trust from each other", careVigilant: "Get to know other users", userProfiles: "User Profiles",
        profileOpportunity: "Your profile is your opportunity to introduce yourself to other members of the community",
        verificationID: "ID Verification", secureIdentity: "We securely validate your identity using your state ID and Facebook profile",
        userRatings: "User Ratings", seeHowMany: "See how many completed transactions users have and check out their average rating",
        appMessaging: "In-App Messaging", securelyCommunicate: "Securely communicate with buyers and sellers without giving away personal information",
        winningGame: "Learn buying and selling best practices", buyingTips: "Buying Tips",
        coverBasics: "Cover the basics of how to successfully inspect and purchase items from sellers on Le-Offers",
        sellingTips: "Selling Tips", overBasics: "over the basics of how to successfully engage with buyers on Le-Offers",
        letUsKnow: "Let us know if we can help", customerExperts: "Customer Care Experts",
        hereToHelp: "We are here to help solve problems and investigate issues when they arise. Please email for assistance",
        workClosely: "We work closely with our Law Enforcement Partners to ensure incidents requiring further investigation are handledaccordingly",
        assistLaw: "To learn more about how we assist Law Enforcement Officers, please visit ourLaw Enforcement Resource Page",



    }

    public engverifyEmail = {
        verifyEmailTitle: "Verify-email - Le-Offers", congratulationsVerified: "Congratulations, your email Id has been verified",

    }
    



    //language support

    //french language support

    public engHeader1 =
    {
        // logo: "assets/images/logo.png", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "Que cherchez-vous...", logOrCreateAccount: "Connectez-vous ou créez votre compte",
        // onTitle: "sur Le-Offers", aboutUs: "À propos de nous", trustNSafe: "Confiance et sécurité", howWorks: "Comment ça marche", needHelp: "Besoin d'aide?", logOut: "Quittez",
        // login: "S'identifier", sellStuff: "Vendez vos srticles", myProfile: "Mon profil", home: "Accueil", signUp: "S'inscrire", backToTitle: "Retour à Le-Offers",
        // logoPng: "assets/images/logo.png", postIn30Secs: "Postez vos articles en aussi peu que 30 secondes", downloadTopRated: "Téléchargez notre application iOS ou Android la mieux cotée pour publier votre article.",
        // phoneNumber: "Numéro de téléphone", emailAddress: "Adresse électronique", enterNumberCountryCode: "S'il vous plaît essayez d'entrer votre numéro complet, y compris le code du pays.",
        // send: "Envoyer", continueWebsite: "Continuer la navigation sur le site", thankYou: "Merci", takeFewMinutes: "cela peut prendre quelques minutes pour que vous le receviez.",
        // checkInternetConnection: "S'il vous plaît, vérifiez votre connexion internet et réessayez.", ok: "Ok", sessionTimedOut: "Session expirée",
        // timeToTitle: "Achetez et vendez rapidement, en toute sécurité et localement. C’est l'heure de Le-Offers!", titleTag: "Le-Offers",
        // Post_Product:"Publiez article",addPhotos:"Ajouter des photos de votre produit",minimumPhotos:"Au moins une photo est obligatoire", maximunPhotos:"Ajoutez jusqu'à plus de photo (facultatif)",
        // maximumPhotoLength:"Maximum cinq photos...",what_selling:"Qu'est-ce que vous vendez? ",Location:"Où se trouve le produit?",price:"Prix",Negotiable:"Négotiable",exchange:"Prêt à échanger",
        // not_found:"Aucun article trouvé",Category:"catégorie",selectCat:"Sélectionnez une catégorie de produit",subCat:"Sous-catégorie",selectSubCat:"Sélectionner une sous-catégorie de produit",
        // select:"Choisir",post:"Publiez",Help:"Aide",connectWith:"Connectez-vous en utilisant",continueFaceBook:"Facebook",continueGoogle:"Google",useMail:"OU UTILISEZ VOTRE EMAIL",Sign_Up:"S'inscrire",
        // Log_In:"S'identifier",byClicking:"En cliquant sur 'Inscrivez-vous',vous acceptez le Le-Offers",terms:"Termes et conditions",policy:"Politique de confidentialité",and:"et",
        // userName:"Nom d'utilisateur",password:"Mot de passe",forgorPassword:"Mot de passe oublié?",haveAccount:"Vous n'avez pas de compte?",email:"Email",Submit:"Soumettre",
        // verifyCode:"Entrer le code de vérification",enterOtp:"Entrez le code que nous avons envoyé à ",createAccount:"Créer un nouveau compte",name:"prénom",fullName:"nom manquant",
        // alreadyHaveAcc:"Already have an account?",
        
        language:"Langue",
        productPrice:"Prix du produit",discover:"Fouiner",
        addTitle:"Ajouter un titre",
        description:"Description",whereSellItem:"Où vendez vous le produit",IWant:"Je veux...",
        havereceiveOtp:"Vous ne l'avez pas encore reçu?", resentOtp:"Renvoyer l'OTP'",
        other:"Autre (voir description)",forParts:"Pour pièces de rechange",used:"D'occase (usure normale)",
        openBox:"Boîte ouverte (mais jamais utilisé)",reCon:"Remis à neuf/Certifié",new:"Neuf (jamais utilisé)",
        selectProductCon:"Sélectionnez l'état du produit",productCon:"État de l'article",from: "De",to:'À',
        back:'Retour a la page de connexion',

        logo: "assets/images/logo.png", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "Que recherchez-vous...", logOrCreateAccount: "Connectez-vous ou créez votre compte",
        onTitle: "sur Le-Offers", aboutUs: "À propos de nous", trustNSafe: "Confiance et Sécurité", howWorks: "Comment ça marche", needHelp: "Besoin d'aide?", logOut: "Se Déconnecter",
        login: "Se Connecter", sellStuff: "Vendre", myProfile: "Mon Profil", home: "Accueil", signUp: "S'inscrire", backToTitle: "Retour à Le-Offers",
        logoPng: "assets/images/logo.png", postIn30Secs: "Déposez une annonce gratuitement en moins de 30 secondes", 
        downloadTopRated: "Téléchargez notre application iOS ou Android déposez vos annonces en toute simplicité.",
        phoneNumber: "Numéro de téléphone", emailAddress: "Adresse e-mail", enterNumberCountryCode: "Veuillez entrer votre numéro complet, y compris le code du pays.",
        send: "Envoyer", continueWebsite: "Continuez à fouiner Le-Offers", thankYou: "Merci", 
        takeFewMinutes: "cela peut prendre quelques minutes pour le recevoir.",
        checkInternetConnection: "Veuillez vérifier votre connexion Internet et réessayer.", ok: "Ok", 
        sessionTimedOut: "La session a expiré car vous êtes connecté sur Le-Offers via un autre appareil. Une seule connexion par utilisateur est autorisée à la fois.",
        timeToTitle: "Achetez et vendez rapidement, en toute sécurité, et prêt de chez vous. Utilisez Le-Offers!", titleTag: "Le-Offers",
        Post_Product:"Rédigez votre annonce",addPhotos:"Ajouter des photos de votre produit",minimumPhotos:"Au moins une photo est obligatoire", maximunPhotos:"N'hésitez pas à ajouter plus de photos(facultatif mais nécessaire). Maximum disponible ",
        maximumPhotoLength:"Nombre maximum d'images atteint...",what_selling:"Qu'est-ce que vous vendez? ",Location:"Où se trouve votre produit?",price:"Prix",
        Negotiable:"Négotiable",exchange:"Peux aussi troquer",not_found:"Aucune Annonce trouvée",Category:"Catégorie",selectCat:"Sélectionnez la catégorie du produit",subCat:"Sous-catégorie",
        selectSubCat:"Sélectionnez la sous-catégorie du produit",select:"Sélectionner",post:"Publier",Help:"Aide",connectWith:"CONNECTEZ-VOUS RAPIDEMENT AVEC",continueFaceBook:"Se Connecter avec Facebook",continueGoogle:"Se Connecter avec Google",
        useMail:"OU UTILISEZ VOTRE EMAIL",Sign_Up:"S'inscrire",Log_In:"Se Connecter",byClicking:"En cliquant sur 'S'inscrire', vous acceptez nos (Le-Offers)",terms:"Termes et conditions",policy:"Politique de confidentialité",and:"and",userName:"Nom d’utilisateur",
        password:"Mot de passe",forgorPassword:"j'ai oublié mon mot de passe",haveAccount:"Je n'ai pas de compte",email:"Email",Submit:"Soumettre",verifyCode:"Entrez le code de vérification",
        enterOtp:"Entrez le code que nous avons envoyé à ",createAccount:"Retour à la page de connexion",name:"Nom",fullName:"Nom manquant",
        alreadyHaveAcc:"Vous avez déjà un compte?",imgLoad:'En cours de téléchargement...',

        languageOptions:[{lang:"Français",checked:false,value:1,langCode:'fr'},
        {lang:"عربي",checked:false,value:3,langCode:'ar'},
        {lang:"English",checked:false,value:2,langCode:'en'}
        ],
    }

public engHome1 = {
    titleBuySellSafe: "Vos annonces sont toujours gratuites et illimités.", to:'à',select:'Sélectionner', free:'GRATUIT',
    productCon:"L'état du produit",selectProductCon:"Sélectionnez l'état du produit"
    ,other:"Autre (voir description)",forParts:"Pour pièces de rechange",used:"D'occase (usure normale)",
    openBox:"Boîte ouverte (mais jamais utilisé)",reCon:"Remis à neuf/Certifié",new:"Neuf (jamais utilisé)",


    titleYealoSafe: "Le-Offers Publiez, Chattez, Achetez, et Vendez.", bringCommunities: "Facilement",
    YealoFun: "Achetez et Vendez prêt de chez vous", cancel: "Annuler", reset: "Réinitialiser", location: "LOCALISATION",
    categories: "CATEGORIES", showMore: "Afficher plus", showLess: "Afficher moins", distance: "DISTANCE", max: "Max", km: "Km",
    noCategory: "Aucune catégorie", sortBy: "Trier par", newestFirst: "Plus récentes", closestFirst: "Plus proches", lowToHighPrice: "Prix croissants",
    highToLowPrice: "Prix décroissants", postedWithin: "PUBLIÉ DANS", allListings: "Toutes les annonces", last24Hours: "Les dernières 24 heures",
    last7Days: "Les 7 derniers jours", last30Days: "Les 30 derniers jours", filters: "Critères", price: "Prix", currency: "Devise", from: "De",
    saveFilters: "Sauvegarder les critères", mike: "assets/images/mike.svg", miles: "Km", noOffers: "Aucune offre trouvée", sorryNotFound: "Désolé, nous n'avons pas trouvé ce que vous recherchez",
    downloadApp: "Téléchargez l'application", startPostingOffers: "pour commencer à publier des annonces maintenant", trustByMillions: "Des millions nous font confiance", blythe: "Nabil",
    robin: "Imane", chris: "Dina", greg: "Ranya", heidi: "Sonya", krystal: "Amaya", readTitleStories: "Lire les histoires d'Le-Offers", getApp: "Obtenez l'application!",
    joinMillions: "Rejoignez les millions d'utilisateurs fidèles en utilisant l'application mobile Le-Offers, le moyen le plus simple d'acheter et de vendre prêt de chez vous!",
    googlePlayStore: "",
    appStore: "", loveJob: "J'aime votre travail!",
    srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
    seePositions: "See All Positions!", sendAppLink: "Envoyer le lien de l'application", emailPlaceHolder: "support@Le-Offers.ma",
    provideValidEmail: "Please provide a valid email address...", howPhone: "",
    enterNumberCountryCode: "Veuillez entrer votre numéro complet, y compris le code du pays.", recentRequestApp: "Vous avez récemment demandé l'application.",
    minsToReceive: "Cela peut prendre quelques minutes pour le recevoir.", wait5Mins: "Veuillez patienter environ 5 minutes", makeOtherRequest: "avant de faire une autre demande.",
    thankYou: "Merci",selectCurrency:'Sélectionner une devise',loadMore:'Page suivante',
    changeLocation:"Changer de localisation",subCat:"Sous-catégorie",advanceFilter:"Critère avancé",setLocation:"Définir la localisation",
    sorry:'Aucun produit trouvé qui correspond à votre recherche',weSorry:'Nous vous montrons des annonces partout au Maroc',backHome:'Accueil',
    searchRes:'Résultat de la recherche'
     //card# 246, Website - No ads found message , fixed:sowmyaSv
}

public engAbout1 = {
    aboutUs: "À propos de nous", howWorks: "Comment ça marche", createCommunities: "Nous rapprochons les gens",
    comeTogetherSell: "pour découvrir leurs valeurs", ourStory: "Notre histoire", ceo: "Nabil Rami, PDG", cto: "Issam Bouhyane, CTO",
    betterYealo: "Une meilleure façon d'acheter et vendre", largeMarket: "Le-Offers est le meilleur marché mobile au Maroc",
    topApp: "Top 3 App", shopCategory: "Dans la catégorie shopping", appYear: "App de l'année", geekWire: "Geekwire", billion14: "14 milliards de dollars",
    transactionYear: "En transactions cette année", million23: "23+ Million", downloads: "Téléchargements", titleNews: "Le-Offers dans l'actualité",
}

public engchangePassword1 = {
  
    changePassTitle: "Modification du mot de passe - Le-Offers", changePassword: "Modifier le mot de passe", currentPassword: "Mot de passe courant",
    newPasswordAgain: "Ressaisissez le nouveau mot de passe", newPassword: "Nouveau mot de passe", submit: "Soumettre", cancel:'Annuler'
}

public engDiscussions1 = {
    titleBuySellSimple: "Le-Offers - Achetez. Vendez. Facilement.", 
    titleYealoSimple: "Le-Offers - Achetez. Vendez. Facilement.", myOffers: "Mes Offres", keyBoard: "Clavier", twoHundred: "200", $: "USD",
    Jeffery: "Nabil", ago: "il y a", month: "mois", one: "1", hey: "Hé", replyToss: "Répondre à ss", send: "Envoyer", report: "Signaler une annonce", sabari: "Farid",
    whyreport: "Pourquoi voulez-vous signaler cette annonce?", prohibited: "C'est interdit sur Le-Offers", offensive: "C'est publication est choquante",
    notReal: "Ce n'est pas une vrai publication", duplicate: "C'est une publication en double", wrongCategory: "C'est dans la mauvaise catégorie", scam: "C'est peut-être une arnaque",
    stolen: "C'est peut être volé", other: "Autre", additionalNote: "Remarque complémentaire (optionnel)", writeNotehere: "Veuillez écrir votre remarque ici...",
    done: "Accompli",
}

public engEmailVerify1 = {
    congratulationsVerified:'Congratulations, you confirmed your email address',

    verifyEmailTitle: "Vérification Email - Le-Offers", emailVerify: "Vérification Email",
    emailProvide: "Veuillez fournir une adresse email valide...",
    submit: "Soumettre",
}

public engFooter1 = {
    logoBottom: "assets/images/logo.png", YealoSafe: "Publiez, Chattez, Achetez, et Vendez", downloadApp: "Téléchargez l'application",
    footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "À propos de nous", howWorks: "Comment ça marche", terms: "Termes",
    privacy: "confidentialité", help: "Aide", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",
}

public engHowItWorks1 = {
    

    aboutUs: "À propos de nous", howItWorks: "Comment ça marche", mobileCommerce: "Acheter et vendre en toute simplicité",
    startlessIn30Secs: "Déposez une annonce gratuitement en moins de 30 secondes", discoverLocally: "Founier prêt de chez vous",
    browseMillionListings: "Cherchez des milliers d'annonces pour trouver des articles étonnants à votre proximité", chatInstantly: "Chatter instantanément",
    messageUsersAnonymously: "Envoyez des messages anonymes et sécurisés aux utilisateurs via l'application Le-Offers.",
    sellSimpleVideo: "Vendre rapidement avec des photos", easilyPost: "Publiez facilement vos articles à vendre en un simple clic avec votre portable.",
    buildTrustCommunity: "Établir une communauté de confiance", userProfiles: "Profils des utilisateurs", knowMoreBuyers: "Informez-vous sur les acheteurs et les vendeurs avant de s'engager.",
    userRatings: "Évaluations des utilisateurs", checkBuyer: "Consultez les évaluations des acheteurs et des vendeurs, et attribuez des évaluations lors de vos transactions.", realTimeAlerts: "Alertes en temps réel",
    getNotified: "Recevez une notification instantanée sur votre téléphone lorsqu'un acheteur ou un vendeur vous contacte.",
    titleIs: "Le-Offers est", toDownload: "", free: "gratuit",
    downloadTopRated: "Téléchargez nôtre application iOS ou Android pour commencer rapidement!", howItWorksPlayStore: "assets/images/playstore.svg",
    howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Bientôt Disponible!!!", thankYou: "Merci",
    fewMinsReceive: "Cela peut prendre quelques minutes pour le recevoir.",

}

public engItem1 = {
    //Card #380, issues: text Annonce into Ajouter, fixedBy: sowmyaSV, date: 11-1-20
    details:"Détails",post:"Ajouter", addTitle:"Ajouter un titre",free:'Gratuit',
    description:"Description",
    other:"Autre (voir description)",forParts:"Pour pièces de rechange",used:"D'occase (usure normale)",
    openBox:"Boîte ouverte (mais jamais utilisé)",reCon:"Remis à neuf/Certifié",new:"Neuf (jamais utilisé)",
    selectProductCon:"Sélectionnez l'état du produit",  productCon:"État de l'article",from: "De",to:'À',googleVerify:'Google+ Vérifié',
    shareLink:'Partager URL ici',Swap:'Troquer',SwapFor:'Troquer contre',copyUrl:'Copier URL',UrlCopied:'URL est copiée avec succès',
    Home:'Accueil',
    titleSimpleWayToBuy: "Le-Offers est un moyen plus simple d'acheter et de vendre localement. Obtenez l'application gratuite.", itemAppStore: "assets/images/appstore.svg",
    itemPlayStore: "assets/images/playstore.svg", previous: "Précédent", next: "Suivant", postRemoved: "Annonce supprimée", posted: "Publié",
    ago: "ago", report: "Report", comments: "Comments", condition: "Condition", viewAll: "tout voir",
    typeYourMessage: "Postez un commentaire...", youHave: "Il vous reste", one120: "120", characterLeft: "caractères.",
    postComment: "Poster un commentaire", myOtherOffers: "Ces annonces peuvent aussi vous intéresser", price: "PRIX", $: "USD", makeOffer: "Faire une offre", follow: "S'abonner",
    following: "Abonné(e)", unFollow: "Ne plus suivre", watch: "Sauvegarder", watching: "Favoris", unWatch: "Retirer des favoris", seller: "À propos du vendeur",
    approximationProtectPrivacy: "Approximation pour protéger la confidentialité du vendeur", followers: "Abonnés", readyBuyHit: "Prêt à faire une offre? Appuyez sur le bouton Proposer.", 
    offer: "Proposer", notSure: "Des questions?", ask: "Demandez au vendeur", forMoreInfo: "plus d'informations", offerSent: "Offre envoyée!", orBetterDownloadApp: "Ou encore mieux, téléchargez l'application. C'est plus rapide! ",
    getApp: "Obtenez l'application!",
    itemGooglePlay: "",
    itemAppStore2: "", soldBy: "Vendu par", whyReport: "Pourquoi voulez-vous signaler cette annonce?",
    additionalNote: "Note complémentaire (facultatif)", pleaseWriteNote: "Veuillez rédiger votre note ...", done: "Compléter", reported: "Signalé",
    thanksTakingTime: "Merci d'avoir pris le temps.", send: "Envoyer", nowFollowing: "Maintenant, vous suivez", offers: "Offres",
    follower: "Abonné", comingSoon: "À venir!", thankYou: "Merci", fewMinsReceive: "Cela peut prendre quelques minutes pour le recevoir.",
    enterQuestionPlaceHolder: "Saisissez votre question ici ...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg",
    Sold:"Vendu",follwo:"Suivi par",Location:"Localisation",
    Negotiable:"Négociable",notNegatiable:"Non négociable",edit:"Modifier",delete:"Supprimer",
    Details:"Détails",addReview:"Ajouter un commentaire",noOfferFound:"Aucune offre trouvée",
    delteListing:"Voulez-vous vraiment supprimer votre annonce?",Cancel:"Annuler",
    editProduct:"Modifier l'annonce",addPhotos:"Ajouter des photos de votre produit",imgLoad:'En cours de téléchargement...',
    minimumPhotos:"Une photo est obligatoire au minimum", maximunPhotos:"N'hésitez pas à ajouter plus de photos(facultatif). Maximum disponible",
    maximumPhotoLength:"Nombre maximum d'images atteint...",what_selling:"Qu'est-ce que vous vendez? ",exchange:"Peux aussi troquer",
    not_found:"Aucune Annonce trouvée",Category:"Catégorie",selectCat:"Sélectionnez la catégorie du produit",subCat:"Sous-catégorie",selectSubCat:"Sélectionnez la sous-catégorie du produit",select:"Sélectionner",update:"Mettre à jour"
}


public engLogin1 = {
    formBackground: "assets/images/formBackground.png", loginTo: "SE CONNECTER À", loginLogo: "assets/images/logo.png", userName: "NOM D'UTILISATEUR",
    passWord: "MOT DE PASSE", byClicking: "En cliquant sur “S'inscrire” ou ”Connecter avec Facebook” ou ”Connecter avec Google” , vous acceptez nos (Le-Offers)",
    termsService: "Termes et Conditions", privacyPolicy: "Politique de Confidentialité", login: "Se Connecter", notRegistered: "Vous n'avez pas de compte?",
    signUp: "S'inscrire", forgotPassword: "Mot de passe oublié?", reset: "Réinitialiser", and: "et", registerErrMsg: "Erreur d'enregistrement",
    connectWith:"CONNECTEZ-VOUS RAPIDEMENT AVEC",continueFaceBook:"Se connecter avec Facebook",continueGoogle:"Se connecter avec Google",useMail:"OU UTILISEZ VOTRE EMAIL",
    alreadyHaveAcc:"Vous avez déjà un compte?",logFaceBook:"Se connecter avec Facebook?",faceBookLog:"Identifiant Facebook",
}

public engLogout1 = {
}
public engOtpVerify ={
    phoneNumber:"Phone Number",title:'otp Verification', verifyOtp:'Verify Otp',resentOtp:"Resend Otp",havereceiveOtp:"Haven't received it yet?", enterOtp:'Enter OTP Here'
}
public engMemberProfile1 = {
    profile: "Profil:", title: "Le-Offers", offers: "met en vente: ", $: "USD", seller: "À propos du vendeur", followers: "Abonnés", 
    //Card #361, issue: Website - translation issue, fixedBy:sowmyaSV, date: 2-1-20
    follow: "Suivi par",
    following: "Abonné(e)", unFollow: "Ne plus suivre", bangaluru: "Casablanca", follower: "Abonnés",
    followBy:"S'Abonner",sellerPolicy:"Approximation pour protéger la confidentialité du vendeur",
}

public engneedHelp1 = {
    support: "Aide - Le-Offers",
}

public engPasswordReset1 = {
    forgotPasswordTitle: "Mot de passe oublié - Le-Offers", newPasswordEnter: "NOUVEAU MOT DE PASSE", newPassword: "Saisissez un nouveau mot de passe",
    newPasswordAgain: "Répétez le nouveau mot de passe:", success: "Vous avez changé votre mot de passe avec succès...", submit: "Soumettre",

}

public engPrivacy1 = {
    privacyPolicyTitle: "Politique de confidentialité - Le-Offers",
}

public engRegister1 = {
    formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.png", name: "NOM", fullNameMissing: "nom complet manquant",
        userName: "NOM UTILISATEUR", email: "EMAIL", phoneNumber: "NUMÉRO DE TÉLÉPHONE", password: "MOT DE PASSE",
        connectFBLogin: "En cliquant sur “Se connecter” ou ”Se connecter avec Facebook” ou ”Se connecter avec Google”, vous acceptez les conditions générales de Le-Offers.", termsService: "Conditions d'utilisation",
        privacyPolicy: "Politique de Confidentialité", and: "et", signUp: "S'INSCRIRE", alreadyRegistered: "Déjà enregistré?", logIn: "Se connecter",
        signUpWith: "Se connecter avec",
        connectWith:"CONNECTER RAPIDEMENT AVEC",continueFaceBook:"Se connecter avec Facebook",continueGoogle:"Se connecter avec Google",useMail:"OU UTILISEZ VOTRE EMAIL",
        alreadyHaveAcc:"Vous avez déjà un compte?",logFaceBook:"Se connecter avec Facebook?",faceBookLog:"Identifiant Facebook",Signup:"S'inscrire",createAccount:"Créer un nouveau compte",
}
public engResetPassword1 = {
    passwordResetTitle: "Réinitialisation du mot de passe - Le-Offers", startBuyCrypto: "commencez à acheter et à vendre en toute sécurité avec crypto! ",
        formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.png", forgotPassword: "Mot de passe oublié",
        recoverPassword: "Entrez votre email pour recouvrir votre mot de passe", receiveLink: "Vous allez recevoir un lien pour réinitialiser votre mot de passe",
        email: "EMAIL", submit: "SOUMETTRE", resetPasswordlogoBottom: "assets/images/logo.png",
}

public engSell1 = {
    sellTitle: "Le-Offers - Acheter et Vendre en toute simplicité.", browse: "Fouiner", noCategory: "Aucune catégorie", terms: "Termes", privacy: "Confidentialité",
    titleCopyRights: "© 2019 Le-Offers", sellStuff: "Déposer une Annonce",
}
public engSettings1 = {
    edit:"Modifier", emailNotVerify:"Email non-vérifié", googleCon:"Google+ connecté", 
    googleDisCon:"Google+ non-connecté", fbCon:"Facebook connecté", fbNotCon:"Facebook non-connecté",emailVeirfy:'Email vérifié',
    payPalVerify:'Paypal vérifié', payPalNot:'Paypal non-vérifié', follower:'Abonné(e)',saveChange:'Sauvegarder les modifications',


    settingsTitle: "Paramètres du compte - Le-Offers", verifiedWith: "Vérifié avec", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg", posts: "Annonces", followers: "Abonnés", following: "Abonnements", selling: "VOS ANNONCES",
    sold: "VENDUS", favourites: "FAVORIS", noListYet: "AUCUNE ANNONCE (POUR LE MOMENT!)", $: "USD",
    noAds:"Aucune annonce pour le moment",snapSell:"",
    noSold:"Aucune vente pour le moment",fountFav:"Enregistrer vos favoris pour les trouver ici",postAds:"",exchange:"ou troquer votre",for:"contre",onSale:"En Vente",follwing:"Abonné(e)",internetErr:"Erreur de connection",
    internetSet:"Veuillez vérifier vos paramètres Internet", close:"Fermer",buildTrust:"Build Trust",cancel:"Annuler",payPal:"Connecter Paypal",
    havePaypal:"Si vous avez déjà un lien Paypal.me, ajoutez-le ici",Example:"Exemple",Save:"Sauvegarder",
    editProfile:"Modifier le Profil",tapPhoto:"Cliquez sur la photo pour la modifier",imageSize:"La photo doit être en format PNG ou JPG et moins de 5 Mo.",Name:"Nom",
    userName:"Nom d'utilisateur", bio:"Biographie",website:"Site Web",Location:"Localisation",privateInfo:"Information Privée",emailAdd:"Adresse E-mail",phoneNumber:"Numéro de téléphone",
    Gender:"Le sexe", male:"Mâle", female:"Femelle",changePw:"Modifier le mot de passe",
}

public engTerms1 = {
}

public engTrust1 = {
    trustTitle: "Faire Confiance - Le-Offers", trust: "Confiance", trustworthiness: "Nous attachons une grande valeur",
        communityWorld: "à la confiance dans notre marché ", buildLocalMarket: "Nous sommes entrain d'établir un marché local où le",
        wellBeing: "bien-être des acheteurs et des vendeurs viens avant tout.", obsessingText: "Nous voulons qu'Le-Offers soit un lieu où acheter et vendre soient plus récompensants. Nous continuerons à examiner chaque détail de l'expérience afin que nos acheteurs et vendeurs peuvent se connecter avec plus de confiance. Et nous continuerons à nous tenir, ainsi que notre communauté, aux normes les plus élevées.",
        trustNeighbours: "Gagner la confiance des uns et des autres", careVigilant: "Faire connaissance d'autres membres de votre communauté", userProfiles: "Profils des Utilisateurs",
        profileOpportunity: "Votre profil vous donne l'occasion de vous présenter aux autres membres de la communauté",
        verificationID: "Vérification d'Identité", secureIdentity: "Nous validons en toute sécurité votre identité en utilisant votre numéro de portable et autres identifiants comme votre profil Facebook ou Google.",
        userRatings: "Évaluations des Utilisateurs", seeHowMany: "Consultez le nombre de transactions complétées par les utilisateurs et consulter leurs évaluations.",
        appMessaging: "Messagerie Instantanée", securelyCommunicate: "Communiquez en toute sécurité avec les acheteurs et les vendeurs sans divulguer vos informations personnelles",
        winningGame: "Acquérez les meilleures pratiques d'acheter et de vendre", buyingTips: "Conseils d'Achat",
        coverBasics: "Fournit des conseils sur la façon d'inspecter et d'acheter avec succès des articles auprès des vendeurs sur Le-Offers",
        sellingTips: "Conseils de Vente", overBasics: "Fournit des conseils sur  la façon de s'engager avec succès avec les acheteurs sur Le-Offers",
        letUsKnow: "Faites-nous savoir comment vous aider", customerExperts: "Experts en service à la clientèle",
        hereToHelp: "Nous sommes ici pour aider à résoudre les problèmes quand ils se présentent.",
        workClosely: "Nous travaillons en étroite collaboration avec nos partenaires pour les incidents nécessitant une enquête plus approfondie sont traités en conséquence",
        assistLaw: "",
}

public engverifyEmail1 = {
    verifyEmailTitle: "Vérification d'E-mail - Le-Offers", congratulationsVerified: "Félicitations, votre e-mail a été vérifié",
}
public engOtpVerify1 ={
    title:'otp Vérification', verifyOtp:'Vérifier Otp',resentOtp:'Renvoyer Otp',havereceiveOtp:"Vous ne l'avez pas encore reçu?",
    enterOtp:'Entrez OTP ici',phoneNumber:"Numéro de téléphone",
}

public engHeader2 =
{
    productPrice:"ثمن المنتج",discover:"البحث",
    addTitle:"إضافة عنوان",
    description:"Description",whereSellItem:"أين تبيع المنتج؟",IWant:"أريد...",
    havereceiveOtp:"لم تستلمه بعد؟", resentOtp:"Renvoyer l'OTP'",
    other:"أخرى (انظر الوصف)",forParts:"لقطع الغيار",used:"مستخدمة (استعمال عادي)",
    openBox:"منتج مفتوح (لم يتم استخدامه)",reCon:"مجدد / معتمد",new:"جديد (لم يستخدم قط)",
    selectProductCon:"حدد حالة المنتج",productCon:"حالة المنتج ",from: "من",to: "إلى",


    logo: "assets/images/logo.png", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "عن مذا تبحث؟...", logOrCreateAccount: "تسجيل الدخول أو إنشاء حسابك",
    onTitle: "على Le-Offers", aboutUs: "في ما يخصنا", trustNSafe: "الثقة والأمن ", howWorks: "كيف يعمل؟ ", needHelp: "بحاجة الى مساعدة؟", logOut: "تسجيل الخروج",
    login: "تسجيل الدخول", sellStuff: "بيع", myProfile: "ملفي الشخصي", home: "الصفحة الرئيسية", signUp: "التسجيل", backToTitle: "العودة إلى Le-Offers",
    logoPng: "assets/images/logo.png", postIn30Secs: "انشر إعلان مجاني في أقل من 30 ثانية", 
    downloadTopRated: "قم بتنزيل تطبيق iOS أو Android لنشر إعلاناتك بسهولة.",
    phoneNumber: "رقم الهاتف", emailAddress: "البريد الإلكتروني", enterNumberCountryCode: "يرجى إدخال رقمك بالكامل ، بما في ذلك رمز البلد.",
    send: "تأكيد", continueWebsite: "متابعة البحث في Le-Offers", thankYou: "شكرا", 
    takeFewMinutes: "قد يستغرق الأمر بضع دقائق لاستلامه.",
    checkInternetConnection: "يرجى التحقق من اتصالك بالإنترنت والمحاولة مرة أخرى.", ok: "حسنا", 
    sessionTimedOut: "انتهت الحصة لأنك متصل بـ Le-Offers عبر جهاز آخر. يُسمح بتسجيل دخول واحد فقط لكل مستخدم في نفس الوقت.",
    timeToTitle: "شراء وبيع بسرعة ، بشكل آمن ، وبالقرب منك. استخدام Le-Offers!", titleTag: "Le-Offers",
    Post_Product:"اكتب اعلانك ",addPhotos:"أضف صورًا لمنتجك ",minimumPhotos:"مطلوب صورة واحدة على الأقل", maximunPhotos:"لا تتردد في إضافة المزيد من الصور (اختياري ولكن ضروري). ",
    maximumPhotoLength:"تم الوصول إلى الحد الأقصى لعدد الصور ...",what_selling:"ماذا تبيع؟",Location:"أين هو منتجك؟",price:"الثمن",
    Negotiable:"قابل للتفاوض",exchange:"يمكن أيضا المبادلة",not_found:"لم يتم العثور على إعلانات",Category:"الفئة",selectCat:"اختر فئة المنتج",subCat:"الفئة الفرعية",
    selectSubCat:"حدد فئة المنتج الفرعية",select:"حدد",post:"نشر",Help:"مساعدة",connectWith:"الاتصال بسرعة مع",continueFaceBook:"المتابعة عن طريق الفيسبوك",continueGoogle:"المتابعة عن طريق جوجل",
    useMail:"أو باستخدام بريدك الإلكتروني",Sign_Up:"تسجيل",Log_In:"تسجيل الدخول",byClicking:"بالضغط على 'تسيل'، أنت تقبل (Le-Offers)",terms:"الشروط والأحكام",policy:"سياسة الخصوصية",and:"و",userName:"إسم المستخدم",
    password:"كلمة المرور",forgorPassword:"نسيت كلمة المرور",haveAccount:"ليس لدي حساب",email:"البريد الإلكتروني",Submit:"ارسال",verifyCode:"أدخل رمز التحقق",
    enterOtp:"أدخل الرمز الذي أرسلناه إلى",createAccount:"العودة إلى صفحة تسجيل الدخول",name:"الاسم",fullName:"الاسم الكامل",
    alreadyHaveAcc:"لديك حساب من قبل؟", language:'اختر اللغات',imgLoad:'تحميل في التقدم...',


   languageOptions:[{lang:"Français",checked:false,value:1,langCode:'fr'},
   {lang:"عربي",checked:false,value:3,langCode:'ar'},
   {lang:"English",checked:false,value:2,langCode:'en'}
   ],
}

public engHome2 = {
    titleBuySellSafe: "إعلاناتك مجانية دائمًا وغير محدودة.", to:'à',select:'Sélectionner', free:'GRATUIT',
    productCon:"حالة المنتج",selectProductCon:"حدد حالة المنتج"
    ,other:"آخر (انظر الوصف)",forParts:"لقطع الغيار",used:"مستخدمة (استعمال عادي)",
    openBox:"منتج مفتوح (لم يتم استخدامه)",reCon:"مجدد / معتمد",new:"جديد (لم يستخدم قط)",
    sorry:'لا توجد منتجات تلبي عن بحثك',weSorry:'نعرض لك إعلانات من جميع أنحاء المغرب',backHome:'الصفحة الرئيسية',


    titleYealoSafe: "Le-Offers المشاركة والدردشة والشراء والبيع.", bringCommunities: "بسهولة",
    YealoFun: "البيع والشراء بالقرب منك", cancel: "إلغاء", reset: "إعادة التعيين", location: "الموقع",
    categories: "الفئات", showMore: "عرض المزيد", showLess: "عرض أقل", distance: "المسافة", max: "الحد الأقصى", km: "كم",
    noCategory: "بدون فئة", sortBy: "الترتيب حسب", newestFirst: "الأحدث", closestFirst: "الأقرب منك", lowToHighPrice: "ثمن تصاعدي",
    highToLowPrice: "ثمن تنازلي", postedWithin: "نشر في", allListings: "جميع الإعلانات", last24Hours: "آخر 24 ساعة",
    last7Days: "آخر 7 أيام", last30Days: "آخر 30 يومًا", filters: "المعايير", price: "الثمن", currency: "العملة", from: "من",
    saveFilters: "حفظ المعايير", mike: "assets/images/mike.svg", miles: "كيلومتر", noOffers: "لا يوجد عروض", sorryNotFound: "عذرا ، لم نجد ما تبحث عنه",
    downloadApp: "قم بتنزيل التطبيق", startPostingOffers: "لبدء نشر الإعلانات الآن", trustByMillions: "الملايين يثقون بنا", blythe:"نبيل",
    robin: "إيمان", chris: "دينا", greg: "رنيا", heidi: "صونيا", krystal: "أماية", readTitleStories: "قراءة قصص Le-Offers", getApp: "الحصول على التطبيق!",
    joinMillions: "انضم إلى ملايين المستخدمين المخلصين باستخدام تطبيق Le-Offers للهاتف المحمول ، كونه أسهل طريقة للشراء والبيع من المنزل!",
    googlePlayStore: "",
    appStore: "", loveJob: "أحب عملك!",
    srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
    seePositions: "شاهد جميع المنشورات!", sendAppLink: "إرسال رابط التطبيق", emailPlaceHolder: "support@Le-Offers.ma",
    provideValidEmail: "يرجى تقديم عنوان بريد إلكتروني صالح ...", howPhone: "",
    enterNumberCountryCode: "يرجى إدخال رقمك بالكامل ، بما في ذلك رمز البلد.", recentRequestApp: "لقد قمت بطلب التطبيق مؤخرًا",
    minsToReceive: "قد يستغرق الأمر بضع دقائق لاستلامه.", wait5Mins: "يرجى الانتظار حوالي 5 دقائق", makeOtherRequest: "قبل تقديم طلب آخر.",
    thankYou: "شكرا",selectCurrency:" اختر عملة",loadMore:"التالى",
    changeLocation:"تغيير الموقع",subCat:"فئة فرعية",advanceFilter:"معيار متقدم",setLocation:"تحديد الموقع",searchRes:'نتيجة البحث'
}

public engAbout2 = {
    aboutUs: "في ما يخصنا", howWorks: "كيف يعمل؟", createCommunities: "نقرب الناس",
    comeTogetherSell: "لاكتشاف قيمهم", ourStory: "قصتنا", ceo: "نبيل رامي ، الرئيس التنفيذي", cto: "عصام بوهيان، الرئيس التقني",
    betterYealo: "طريقة أفضل للشراء والبيع", largeMarket: "Le-Offers هو أفضل سوق ديناميكي في المغرب",
    topApp: "أفضل 3 تطبيقات", shopCategory: "في فئة التسوق", appYear: "تطبيق السنة", geekWire: "Geekwire", billion14: "14 مليار دولار",
    transactionYear: "في المعاملات هذا العام", million23: "23+ مليون", downloads: "تحميلات", titleNews: "Le-Offers في الأخبار",
}

public engchangePassword2 = {
    changePassTitle: "تغيير كلمة المرور - Le-Offers", changePassword: "تغيير كلمة المرور", currentPassword: "كلمة المرور الحالية",
    newPasswordAgain: "Ressaisissez le nouveau mot de passe", newPassword: "كلمة مرور جديدة", submit: "إرسال", cancel: "إلغاء"
}

public engDiscussions2 = {
    titleBuySellSimple: "Le-Offers - شراء. بيع. بسهولة.", 
    titleYealoSimple: "Le-Offers - شراء. بيع. بسهولة.", myOffers: "عروضي", keyBoard: "لوحة المفاتيح", twoHundred: "200", $: "USD",
    Jeffery: "نبيل", ago: "منذ", month: "شهر", one: "1", hey: "مرحبا", replyToss: "الجواب على ss", send: "إرسال", report: "الإبلاغ عن إعلان", sabari: "فريد",
    whyreport: "لماذا تريد الإبلاغ عن هذا الإعلان؟", prohibited: "ممنوع على Le-Offers", offensive: "هذا المنشور جارح",
    notReal: "هذا ليس منشورًا حقيقيًا", duplicate: "هذا هو منشور مكرر", wrongCategory: "انها في الفئة الخطأ", scam: "قد يكون احتيالا",
    stolen: "قد يكون مسروقا", other: "آخر", additionalNote: "ملاحظة إضافية (اختياري)", writeNotehere: "يرجى كتابة ملاحظتك هنا ...",
    done: "إنتهاء",
}

public engEmailVerify2 = {
    congratulationsVerified:'Congratulations, you confirmed your email address',

    verifyEmailTitle: "التحقق من البريد الإلكتروني - Le-Offers", emailVerify: "التحقق من البريد الإلكتروني",
    emailProvide: "يرجى تقديم عنوان بريد إلكتروني صالح ...",
    submit: "إرسال",

}

public engFooter2 = {
    logoBottom: "assets/images/logo.png", YealoSafe: "النشر والدردشة والشراء والبيع", downloadApp: "قم بتنزيل التطبيق",
    footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "في ما يخصنا", howWorks: "كيف يعمل؟", terms: "الشروط",
    privacy: "الخصوصية", help: "مساعدة", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",



}

public engHowItWorks2 = {

    aboutUs: "في ما يخصنا", howItWorks: "كيف يعمل؟", mobileCommerce: "شراء وبيع بسهولة",
    startlessIn30Secs: "نشر إعلان مجاني في أقل من 30 ثانية", discoverLocally: "البحث بالقرب منك",
    browseMillionListings: "ابحث عن آلاف المنشورات للعثور على منتجات رائعة بالقرب منك", chatInstantly: "الدردشة على الفور",
    messageUsersAnonymously: "أرسل رسائل مجهولة وآمنة للمستخدمين من خلال تطبيق Le-Offers.",
    sellSimpleVideo: "بيع بسرعة مع الصور", easilyPost: "انشر منتجاتك بسهولة للبيع بنقرة بسيطة على هاتفك المحمول.",
    buildTrustCommunity: "إنشاء مجموعة ثقة", userProfiles: "ملفات تعريف المستخدمين", knowMoreBuyers: "اسأل عن المشترين والبائعين قبل الالتزام.",
    userRatings: "تقييم المستخدم", checkBuyer: "عرض تقييمات المشترين والبائعين ، وتعيين تقييمات للمعاملات الخاصة بك.", realTimeAlerts: "تنبيهات في الوقت الحقيقي",
    getNotified: "تلقي نتبيها فوريًا على هاتفك عندما يتصل بك المشتري أو البائع.",
    titleIs: "Le-Offers هو", toDownload: "للتحميل", free: "مجانا",
    downloadTopRated: "قم بتنزيل تطبيق iOS أو Android للبدء بسرعة!", howItWorksPlayStore: "assets/images/playstore.svg",
    howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "متوفر قريبا!!!", thankYou: "شكرا",
    fewMinsReceive: "Cela peut يستغرق بضع دقائق لاستلامه.",

}

public engItem2 = {
    details:"التفاصيل",post:"الإعلان", addTitle:"إضافة عنوان",free: "مجانا",
    description:"الوصف",
    other:"أخرى (انظر الوصف)",forParts:"لقطع الغيار",used:"مستخدمة (استعمال عادي)",
    openBox:"منتج مفتوح (لم يتم استخدامه)",reCon:"مجدد / معتمد",new:"جديد (لم يستخدم قط)",
    selectProductCon:"حدد حالة المنتج",  productCon:"حالة المنتج",from: "من",to: "إلى",googleVerify:'Google+ Vérifié',
    shareLink:'Partager URL ici',Swap:'Troquer',SwapFor:'Troquer contre',copyUrl:'Copier URL',UrlCopied:'URL est copiée avec succès',
    Home:'Accueil',
    titleSimpleWayToBuy: "Le-Offers هي طريقة أبسط للشراء والبيع محليًا. الحصول على التطبيق المجاني.", itemAppStore: "assets/images/appstore.svg",
    itemPlayStore: "assets/images/playstore.svg", previous: "السابق", next: "التالي", postRemoved: "تم حذف الإعلان", posted: "منشور",
    ago: "منذ", report: "الإبلاغ", comments: "التعليقات", condition: "الشروط", viewAll: "رؤية الكل",
    typeYourMessage: "نشر تعليق ...", youHave: "تبقى لكم", one120: "120", characterLeft: "أحرف.",
    postComment: "نشر تعليق", myOtherOffers: "قد تهمك هذه الإعلانات أيضًا", price: "ثمن", $: "USD", makeOffer: "تقديم عرض", follow: "متابعة",
    following: "مشترك (ة)", unFollow: "التوقف عن المتابعة", watch: "حفظ", watching: "المفضلة", unWatch: "إزالة من المفضلة", seller: "حول البائع",
    approximationProtectPrivacy: "تقريب لحماية سرية البائع", followers: "متابع", readyBuyHit: "هل أنت مستعد لتقديم عرض؟ اضغط على زر إرسال.", 
    offer: "اقتراح", notSure: "أسئلة؟", ask: "اسأل البائع", forMoreInfo: "مزيد من المعلومات", offerSent: "تم إرسال العرض!", orBetterDownloadApp: "أو أفضل ، قم بتنزيل التطبيق. فهذا أسرع!",
    getApp: "الحصول على التطبيق!",
    itemGooglePlay: "",
    itemAppStore2: "", soldBy: "بيعت بواسطة", whyReport: "لماذا تريد الإبلاغ عن هذا الإعلان؟",
    additionalNote: "ملاحظة إضافية (اختياري)", pleaseWriteNote: "يرجى كتابة ملاحظتك ...", done: "إرسال", reported: "إبلاغ",
    thanksTakingTime: "شكرا لوقتك.", send: "إرسال", nowFollowing: "أنت الآن تتبع", offers: "العروض",
    follower: "متتبع", comingSoon: "قريبا!", thankYou: "شكرا", fewMinsReceive: "قد يستغرق الأمر بضع دقائق لاستلامه.",
    enterQuestionPlaceHolder: "أدخل سؤالك هنا ...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg",
    Sold:"تم بيعه",follwo:"متابع من طرف",Location:"الموقع",
    Negotiable:"قابل للتفاوض",notNegatiable:"غير قابل للتفاوض",edit:"تعديل",delete:"مسح",
    Details:"التفاصيل",addReview:"إضافة تعليق",noOfferFound:"لا يوجد عروض",
    delteListing:"هل أنت متأكد من أنك تريد حذف إعلانك؟",Cancel:"إلغاء",
    editProduct:"تعديل الإعلان",addPhotos:"أضف صورًا لمنتجك",imgLoad:'تحميل في التقدم...',
    minimumPhotos:"مطلوب صورة على الأقل", maximunPhotos:"لا تتردد في إضافة المزيد من الصور (اختياري).",
    maximumPhotoLength:"تم الوصول إلى الحد الأقصى لعدد الصور ...",what_selling:"ماذا تبيع؟",exchange:"يمكن أيضا المبادلة",
    not_found:"لم يتم العثور على إعلانات",Category:"الفئة",selectCat:"اختر فئة المنتج",subCat:"الفئة الفرعية",selectSubCat:"حدد فئة المنتج الفرعية",select:"تحديد",update:"تحديث"
}

public engLogin2 = {

    formBackground: "assets/images/formBackground.png", loginTo: "الاتصال ب", loginLogo: "assets/images/logo.png", userName: "إسم المستخدم",
    passWord: "كلمة السر", byClicking: "من خلال النقر على 'التسجيل'أو 'التسجيل عبر Facebookأو التسجيل عبر Google, فأنت تقبل (Le-Offers)",
    termsService: "الشروط والأحكام", privacyPolicy: "سياسة الخصوصية", login: "الدخول", notRegistered: "ليس لديك حساب؟",
    signUp: "التسجيل", forgotPassword: "نسيت كلمة المرور؟", reset: "إعادة التعيين", and: "و", registerErrMsg: "خطأ في التسجيل",
    connectWith:"الاتصال بسرعة مع",continueFaceBook:"الدخول عن طريق فايسوك",continueGoogle:"الدخول عن طريق غوغل",useMail:"أو استخدم بريدك الإلكتروني",
    alreadyHaveAcc:"لديك بالفعل حساب؟",logFaceBook:"الدخول عن طريق فايسوك",faceBookLog:"الدخول بالفيسبوك"
}
    

public engLogout2 = {
}

public engMemberProfile2 = {
profile: "البيانات الشخصية:", title: "Le-Offers", offers: "ضع للبيع:", $: "USD", seller: "عن البائع", followers: "المتابعون", follow: "تابع",
following: "مشترك (ة)", unFollow: "التوقع عن المتابعة", bangaluru: "الدار البيضاء", follower: "المتابعون",
followBy:"متابع من طرف",sellerPolicy:"التقريب لحماية سرية البائع",

}

public engneedHelp2 = {
    support: "مساعدة - Le-Offers",
}

public engPasswordReset2 = {
    forgotPasswordTitle: "نسيت كلمة المرور - Le-Offers", newPasswordEnter: "كلمة مرور جديدة", newPassword: "أدخل كلمة مرور جديدة",
    newPasswordAgain: "كرر كلمة المرور الجديدة:", success: "لقد نجحت في تغيير كلمة المرور الخاصة بك ...", submit: "إرسال",

}

public engPrivacy2 = {
    privacyPolicyTitle: "سياسة الخصوصية - Le-Offers",
}

public engRegister2 = {
    formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.png", name: "الإسم", fullNameMissing: "الاسم الكامل ناقص",
    userName: "اسم المستخدم", email: "البريد الإلكتروني", phoneNumber: "رقم الهاتف", password: "كلمة المرور",
    connectFBLogin: "من خلال النقر على دخول” ou ” المتابعة عن طريق الفيسبوك” ou ” المتابعة عن طريق جوجل”, vous acceptez les conditions générales de Le-Offers.", termsService: "شروط الاستعمال",
    privacyPolicy: "سياسة الخصوصية", and: "و", signUp: "التسجيل", alreadyRegistered: "مسجل من قبل؟", logIn: "الدخول",
    signUpWith: "الدخول عن طريق",
    connectWith:"الدخول بسرعة عن طريق",continueFaceBook:"المتابعة عن طريق الفيسبوك",continueGoogle:"المتابعة عن طريق جوجل",useMail:"أو استخدم بريدك الإلكتروني",
    alreadyHaveAcc:"لديك حساب من قبل؟",logFaceBook:"المتابعة عن طريق الفيسبوك",faceBookLog:"اسم مستخدم الفيسبوك",Signup:"التسجيل",createAccount:"انشاء حساب جديد",
}
public engResetPassword2 = {
    passwordResetTitle: "إعادة تعيين كلمة المرور- Le-Offers", startBuyCrypto: "البدء في بيع وشراء آمن مع كريبتو!",
    formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.png", forgotPassword: "نسيت كلمة المرور",
    recoverPassword: "أدخل بريدك الإلكتروني لاستعادة كلمة المرور الخاصة بك", receiveLink: "ستتلقى رابطًا لإعادة تعيين كلمة المرور الخاصة بك",
    email: "البريد الإلكتروني", submit: "إرسال", resetPasswordlogoBottom: "assets/images/logo.png",
}

public engSell2 = {
    sellTitle: "Le-Offers - شراء وبيع بكل سهولة.", browse: "بحث", noCategory: "بدون فئة", terms: "الشروط", privacy: "الخصوصية",
    titleCopyRights: "© 2019 Le-Offers", sellStuff: "انشر إعلانا",
}
public engSettings2 = {
    edit:"Modifier", emailNotVerify:"البريد الإلكتروني لم يتم التحقق منه", googleCon:"اتصال + جوجل", 
    googleDisCon:"غير متصل+ جوجل", fbCon:"الفيسبوك متصل", fbNotCon:"الفيسبوك غير متصل",emailVeirfy:'Email vérifié',
    payPalVerify:'Paypal vérifié', payPalNot:'Paypal non-vérifié', follower:'Abonné(e)',saveChange:'Sauvegarder les modifications',


    settingsTitle: "إعدادات الحساب- Le-Offers", verifiedWith: "التحقق مع", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg", posts: "الإعلانات", followers: "المتابعون", following: "الاشتراكات", selling: "إعلاناتك",
    sold: "تم البيع", favourites: "المفضلة", noListYet: "لا إعلان (لحد الآن)", $: "USD",
    noAds:"لا إعلان لحد الآن",snapSell:"",
    noSold:"لا مبيعات في الوقت الراهن",fountFav:"حفظ المفضلة لديك للعثور عليهم هنا",postAds:"",تبديل:"أو مقايضة",for:"بـ",onSale:"للبيع",follwing:"مشترك (ة)",internetErr:"خطأ في الاتصال",
    internetSet:"يرجى التحقق من إعدادات الإنترنت الخاصة بك", close:"إغلاق",buildTrust:"بناء الثقة",cancel:"إلغاء",payPal:"ربط باي بال",
    havePaypal:"إذا كان لديك بالفعل رابط Paypal.me ، فأضفه هنا",Example:"مثال",Save:"حفظ",
    editProfile:"تعديل الملف الشخصي",tapPhoto:"انقر على الصورة لتعديلها",imageSize:"يجب أن تكون الصورة بتنسيق PNG أو JPG وأقل من 5 ميغابايت.",Name:"الإسم",
    userName:"إسم المستخدم", bio:"السيرة الذاتية",website:"موقع إلكتروني",Location:"الموقع",privateInfo:"معلومات خاصة",emailAdd:"اليريد الإلكتروني",phoneNumber:"رقم الهاتف",
    Gender:"الجنس", male:"ذكر", female:"أنثى",changePw:"تغيير كلمة المرور",

}

public engTerms2 = {
}

public engTrust2 = {

    trustTitle: "يثق في- Le-Offers", trust: "الثقة", trustworthiness: "نحن نعطي قيمة كبيرة",
    communityWorld: "للثقة في السوق لدينا", buildLocalMarket: "نحن نقوم بإنشاء السوق المحلية حيث",
    wellBeing: "رفاهية المشترين والبائعين تأتي في المرتبة الأولى.", obsessingText: "نحن نريد أن تكون Le-Offers مكانًا يكون فيه البيع والشراء أكثر فائدة. سنستمر في فحص كل تفاصيل التجربة حتى يتمكن المشترون والبائعون لدينا من التواصل بثقة أكبر. كما سنستمر في الإلتزام، وكذلك مستخدمونا، بأعلى المستويات.",
    trustNeighbours: "كسب ثقة بعضنا البعض", careVigilant: "تعرف على أعضاء آخرين هنا", userProfiles: "ملفات تعريف المستخدمين",
    profileOpportunity: "يمنحك ملف التعريف الخاص بك الفرصة لتقديم نفسك لأفراد آخرين هنا",
    verificationID: "التحقق من الهوية", secureIdentity: "نحن نقوم بالتحقق من هويتك بأمان باستخدام رقم هاتفك المحمول ومعلومات أخرى مثل ملفك الشخصي على الفيسبوك أو جوجل.",
    userRatings: "تقييمات المستخدم", seeHowMany: "عرض عدد المعاملات المكتملة من قبل المستخدمين وعرض تصنيفاتها.",
    appMessaging: "التراسل الفوري", securelyCommunicate: "التواصل بشكل آمن مع المشترين والبائعين دون الكشف عن معلوماتك الشخصية",
    winningGame: "الحصول على أفضل الممارسات لشراء وبيع", buyingTips: "نصائح للشراء",
    coverBasics: "ابحث عن نصائح حول كيفية فحص العناصر وشرائها بنجاح من البائعين عل Le-Offers",
    sellingTips: "نصائح للبيع", overBasics: "قم بتقديم نصائح حول كيفية الانخراط بنجاح مع المشترين عل Le-Offers",
    letUsKnow: "شارك معنا كيفية مساعدتك", customerExperts: "خبراء خدمة العملاء",
    hereToHelp: "نحن هنا للمساعدة في حل المشاكل التي تعترضك",
    workClosely: "نحن نعمل عن كثب مع شركائنا للمشاكل التي تتطلب مزيدًا من التحقيق ليتم معالجتها",
    assistLaw: "",

}

public engverifyEmail2 = {
    verifyEmailTitle: "التحقق من البريد الإلكتروني- Le-Offers", congratulationsVerified: "تهانينا ، لقد تم التحقق من بريدك الإلكتروني",
}
public engOtpVerify2 ={
    title:'otp التحقق', verifyOtp:'تحقق من Otp',resentOtp:'إعادة إرسال Otp',havereceiveOtp:"لم تستلمها بعد؟",enterOtp:'أدخل مكتب المدعي العام هنا',
    phoneNumber:"رقم الهاتف",
}



}