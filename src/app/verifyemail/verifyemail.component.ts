import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { RegisterService } from '../register/register.service';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';
import { Configuration } from '../app.constants';
declare var $:any;
@Component({
  selector: 'verifyemail',
  templateUrl: './verifyemail.component.html',
  styleUrls: ['./verifyemail.component.css']
})
export class VerifyemailComponent implements OnInit {

  constructor(
    private _missionService: MissionService, 
    private _router: Router, 
    private route: ActivatedRoute, 
    private location:Location,
    private _service: RegisterService,
    private _conf: Configuration,
    private _lang: LanguageService) { }

  headerHelpClose: string;
  verificationToken: any;
  token: any;
  emailverify = false;
  verifyEmail: any;
  languageCode:any;
  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'2') :
    //     params['language'] == 'fr' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'3'); 
    //   }
    // });
    // this.verifyEmail = this._lang.engverifyEmail;
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.verifyEmail = this._lang.engverifyEmail1;
                this.languageCode = 'fr';
                break;
        case 2: this.verifyEmail = this._lang.engverifyEmail;
                this.languageCode = 'en';
                break;
        case 3: this.verifyEmail = this._lang.engverifyEmail2;
                this.languageCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;

        // case 3: this.item = this._lang.engItem;
        //         this.languageCode= 'en';
        //         break;
      }
    }
    else{
      this.verifyEmail = this._lang.engverifyEmail;
      this.languageCode = 'en';
    }
    this._missionService.confirmheaderHelpClose(this.headerHelpClose);    
    
      this.route.params.subscribe(params => {
        this.token = params['id'];
        this.verificationToken = params['vtoken'];
        let list = {
          authToken : this.token,
          verificationToken: this.verificationToken
        };

        this._service.verifyEmail(list)
          .subscribe((res) => {
            if (res.code == 200) {
              this.emailverify = true;
              setTimeout(() => {
                // this._router.navigate([this.languageCode]);
                this._router.navigate(['']);                
              }, 5000);

            } else {
              // this._router.navigate([this.languageCode]);
              this._router.navigate(['']);              
            }
          });
      });
      // this._router.navigate([this.languageCode,]);  
    
  }

}

