import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AboutComponent } from './about/about.component';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { TrustComponent } from './trust/trust.component';
import { ItemComponent } from './item/item.component';
import { MemberprofileComponent } from './memberprofile/memberprofile.component';
import { SettingsComponent } from './settings/settings.component';
import { NeedhelpComponent } from './needhelp/needhelp.component';
import { LogoutComponent } from './logout/logout.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { VerifyemailComponent } from './verifyemail/verifyemail.component';
import { EmailverifyComponent } from './emailverify/emailverify.component';
import { OptpVerificationComponent } from './optp-verification/optp-verification.component';
import { AuthGuard } from './auth.guard';
import { CategoryComponent } from './category/category.component';
import { FiltersComponent } from './filters/filters.component';
import { SearchproductsComponent } from './searchproducts/searchproducts.component';
const appRoutes: Routes = [
  { path: ':language/', component: HomeComponent },
  { path: ':lang/category/:name', component: CategoryComponent },
  { path:'',component: HomeComponent},
  { path: ':language', component: HomeComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: ':lang/reset', component: ResetpasswordComponent },
  { path: ':lang/password', component: ChangepasswordComponent },
  { path: ':lang/searchProduct/:search', component: SearchproductsComponent },
  { path: ':lang/p/:name', component: MemberprofileComponent },
  { path: ':lang/about', component: AboutComponent },
  { path: ':lang/howitworks', component: HowitworksComponent },
  { path: ':lang/trust', component: TrustComponent }, 
  { path: ':lang/emailVerify', component: EmailverifyComponent }, 
  { path: ':lang/settings', component: SettingsComponent },
  { path: ':lang/help', component: NeedhelpComponent },
  { path: ':lang/terms', component: TermsComponent },
  { path: ':lang/privacy', component: PrivacyComponent }, 
  { path: ':lang/reset/:id', component: PasswordresetComponent },
  { path: 'reset/:id', component: PasswordresetComponent },
  { path: ':lang/verify-email/:id/:vtoken', component: VerifyemailComponent },
  { path: 'verify-email/:id/:vtoken', component: VerifyemailComponent },
  { path: ':language/:name/:id', component: ItemComponent },
  { path: ':language/filters', component: FiltersComponent },
  { path: ':lang/otpVerification' , component:OptpVerificationComponent},
];

export const routing = RouterModule.forRoot(appRoutes,{onSameUrlNavigation: 'reload'});