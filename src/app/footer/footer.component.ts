import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { LanguageService } from '../app.language';
import { Configuration } from '../app.constants';
declare var $: any;

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  footerClosed = false;
  succesSeller = false;
  footer:any;
  currentScrollPos:any;
  showFooterAfterScroll:any;
  //Card #115, Website - The bottom bar should not show unless the user scrolls down. fixedBy: sowmyaSV
  // @HostListener('window:scroll', ['$event'])
  // onWindowScroll($event) {
  //   const number = window.scrollY;
  //   // //console.log("number",number);
  //   // (number > 50) ? this.showFooterAfterScroll = true :this.showFooterAfterScroll = false;
  //   ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) ? this.showFooterAfterScroll = true :this.showFooterAfterScroll = false;
  //     // you're at the bottom of the page
  // }

  constructor(private missionService: MissionService, private router: Router, private _lang: LanguageService,private route: ActivatedRoute, private _conf:Configuration) {
    missionService.missionheaderOpen$.subscribe(
      headerOpen => {
        this.footerClosed = true;
      });
    missionService.missionheaderClose$.subscribe(
      headerClose => {
        this.footerClosed = true;
      });
    missionService.missionheaderClosed$.subscribe(
      headerClosed => {
        this.footerClosed = false;
      });
    missionService.missionheaderHelpClose$.subscribe(
      headerHelpClose => {
        this.footerClosed = true;
      });
      // missionService.missionScroll$.subscribe(
      //   footerShow =>{
      //     //console.log("footerShow",typeof footerShow)
      //     this.showFooterAfterScroll = footerShow;
      //   }
      // )
    
  }
  languagecode:any;
  ngOnInit() {
    // this.footer = this._lang.engFooter;
    // this.route.params.subscribe(params => {
    //   if(params['language']){
    //     params['language'] == 'en' ? sessionStorage.setItem("Language",'1') :
    //     sessionStorage.setItem("Language",'2'); 
    //   }
    // });
    let selectedLang  = Number(this._conf.getItem("Language"));;
    if(selectedLang){
      switch(selectedLang){

        case 1: this.footer = this._lang.engFooter1;
                this.languagecode = 'fr';
                break;
        case 2: this.footer = this._lang.engFooter;
                this.languagecode = 'en';
                break;

        case 3: this.footer = this._lang.engFooter2;
                this.languagecode= 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
      }
    }
    else{
      this.footer = this._lang.engFooter;
      this.languagecode = 'en';
    }
  }
  // card#292, issues : footer links are not redirecting into respective page, fixedBY: sowmyaSV, date: 10-1-20
  goToRespectivePage(caseValue){
    switch(caseValue){
      case 1: this.router.navigate([`${this.languagecode}/about`]);
               break;
      case 2: this.router.navigate([`${this.languagecode}/howitworks`]);
              break;
      case 3: this.router.navigate([`${this.languagecode}/terms`]);
              break;
      case 4: this.router.navigate([`${this.languagecode}/privacy`]);
              break;
      case 5: this.router.navigate([`${this.languagecode}/help`]);
              break;
        
    }
  }

}
