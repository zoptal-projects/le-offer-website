import { Component, OnInit, HostListener } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { HomeService } from './home.service';
import { Configuration } from '../app.constants';
import { Meta } from '@angular/platform-browser';
import { LanguageService } from '../app.language';
import{ Location } from '@angular/common';
// import { NgxMasonryOptions } from 'ngx-masonry';
declare var $: any;
declare var google: any;
declare var moment: any;

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  innerWidth:any;
  @HostListener('window:size', ['$event'])
  onSize(event) {
    this.innerWidth = window.innerWidth;
  }
  // public masonryOptions: NgxMasonryOptions = {
	// 	transitionDuration: '0.2s',
	// 	gutter: 20,
	// 	resize: true,
	// 	initLayout: true,
  //   fitWidth: true,
  //   horizontalOrder:true
  // };
  activeQueryParams:any;
  masonryImages;
	dummyPictures = [
		{
			picture: 'https://source.unsplash.com/433x649/?Uruguay'
		},
		{
			picture: 'https://source.unsplash.com/530x572/?Jamaica'
		},
		{
			picture: 'https://source.unsplash.com/531x430/?Kuwait'
		},
		{
			picture: 'https://source.unsplash.com/586x1073/?Bermuda'
		},
		{
			picture: 'https://source.unsplash.com/500x571/?Ecuador'
		},
		{
			picture: 'https://source.unsplash.com/579x518/?Virgin Islands (British)'
		}
	];

  constructor(
    private _missionService: MissionService,
    private _meta: Meta,
    private _conf: Configuration,
    private _router: Router,
    private _service: HomeService,
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private location : Location,
  ) {
    // to get query params in the url
    this.getSearchProduct('');
  }
  currentScrollPos:any;
  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    const number = window.scrollY;
    if (number > 500) {
        this.currentScrollPos = number;
    }
  }
  headerClose: string;
  token: string;
  offset = 0;
  limit = 40;
  langaugeCode:any;
  distances: any;
  errMsg: any;
  loaderButton: any;
  resetToggle:any
  allList: any = [];
  allListEmpty: any = [];

  prevSelectedCat:any;
  previousCategoryIndex:any;
  catList: any;
  catSelectList = false;
  subCat: any;
  catSubFilter: any;
  loadMoreList = false;
  loadMoreListArrow = false;
  allListData = false;
  placelatlng: any;
  recentCity: any;
  listLength: any;
  listsLength = false;
  offsetImg = 100;
  defaultImage = 'assets/images/lazyLoad.png';
  sellEmail: string;
  sellPhone: string;
  selectedCategoryNodeId:any;
  email_Error = false;
  phone_Error = false;
  registerSave = false;
  PEerror: any;
  queryParams:any;
  succesSeller = false;
  howPopupSuccess = false;
  listSell: any;
  signUpContent: boolean = true;

  filterPrice = false;
  categoryName: any;
  latlng: any;
  priceOrder = "lowToHigh";
  timeOrder = 0;
  distance = 30;
  minPrice: any;
  maxPrice: any;
  filterScroll: any;
  postedWithin: any;
  currency: any;
  DefualtCurrency:any;
  bannerShow = false;
  loadCateg = false;

  home: any;
  data: any;

  infowindow: any;
  map: any;
  place: any;

  shimmer: any = [];
  searchItem = 0;
  ipinfoData:any;
  ipApiResponse:any;
  SelectedCat :any;
  SelectSubCat:any;
  ngOnInit() {
    this._conf.removeItem("CountryCode");
   this.resetToggle = 0;
    let selectedLang  = Number(this._conf.getItem("Language"));;
     if(selectedLang){
      switch(selectedLang){
 
        case 1: this.home = this._lang.engHome1;
                 this.langaugeCode = 'fr';
                 break; 
        case 2:this.home = this._lang.engHome;
                this.langaugeCode = 'en';
                break;
        case 3:this.home = this._lang.engHome2;
                this.langaugeCode = 'ar';
                $("html, body").css({'direction':'rtl', "font-size": "18px !important"});
                break;
          
       }
     }
     else{
       this.home = this._lang.engHome;
       this.langaugeCode = 'en';
     }
      this.seo();
      this.placeHolder();

      $(window).on('popstate', function () {
        $('.modal').modal('hide');
      });

      this._missionService.confirmheaderClose(this.headerClose);
      this.token = this._conf.getItem('authToken');
      let latlng = this._conf.getItem('latlng');
      let cityArea = this._conf.getItem('recentCity');
      (cityArea) ? $("#cityAreai").val(cityArea):'';
      (!latlng) ? this.currentLocation() :this.allListPosts();
       this.listGetCategories();
      setTimeout(() => {
        let latlng1 = this._conf.getItem('latlng');
          if (!latlng1 || !cityArea) {
          this.latLngNull();
        }
      }, 1000);
      this._conf.setItem("CountryCode", 'MA');
      this.currency = 'USD';
      this.DefualtCurrency = 'USD';

    // }
    // setTimeout(()=>{
    //   this.updatePresentUrl(0,'');
    //   this.closeDiscoverPage();
    // },800);
  }

  placeHolder() {
    for (var i = 0; i < 12; i++) {
      this.shimmer.push(i);
    }
  }
  public currencyList = [
    { value: "ARP", text: "Argentina Pesos" },
    { value: "ATS", text: "Austria Schillings" },
    { value: "AUD", text: "Australia Dollars" },
    { value: "BSD", text: "Bahamas Dollars" },
    { value: "BBD", text: "Barbados Dollars" },
    { value: "BEF", text: "Belgium Francs" },
    { value: "BMD", text: "Bermuda Dollars" },
    { value: "BRR", text: "Brazil Real" },
    { value: "BGL", text: "Bulgaria Lev" },
    { value: "CAD", text: "Canada Dollars" },
    { value: "CHF", text: "Switzerland Francs" },
    { value: "CLP", text: "Chile Pesos" },
    { value: "CNY", text: "China Yuan Renmimbi" },
    { value: "CYP", text: "Cyprus Pounds" },
    { value: "CSK", text: "Czech Republic Koruna" },
    { value: "DEM", text: "Germany Deutsche Marks" },
    { value: "DKK", text: "Denmark Kroner" },
    { value: 'DH' , text: 'Dirham'},
    { value: "DZD", text: "Algeria Dinars" },
    { value: "EGP", text: "Egypt Pounds" },
    { value: "ESP", text: "Spain Pesetas" },
    { value: "EUR", text: "Euro" },
    { value: "FJD", text: "Fiji Dollars" },
    { value: "FIM", text: "Finland Markka" },
    { value: "FRF", text: "France Francs" },
    { value: "GRD", text: "Greece Drachmas" },
    { value: "HKD", text: "Hong Kong Dollars" },
    { value: "HUF", text: "Hungary Forint" },
    { value: "ISK", text: "Iceland Krona" },
    { value: "INR", text: "India Rupees" },
    { value: "IDR", text: "Indonesia Rupiah" },
    { value: "IEP", text: "Ireland Punt" },
    { value: "ILS", text: "Israel New Shekels" },
    { value: "ITL", text: "Italy Lira" },
    { value: "JMD", text: "Jamaica Dollars" },
    { value: "JPY", text: "Japan Yen" },
    { value: "JOD", text: "Jordan Dinar" },
    { value: "KRW", text: "Korea(South) Won" },
    { value: "LBP", text: "Lebanon Pounds" },
    { value: "LUF", text: "Luxembourg Francs" },
    { value: "MYR", text: "Malaysia Ringgit" },
    { value: 'MAD', text: 'Moroccan dirham'},
    { value: "MXP", text: "Mexico Pesos" },
    { value: "NLG", text: "Netherlands Guilders" },
    { value: "NZD", text: "New Zealand Dollars" },
    { value: "NOK", text: "Norway Kroner" },
    { value: "PKR", text: "Pakistan Rupees" },
    { value: "PHP", text: "Philippines Pesos" },
    { value: "PLZ", text: "Poland Zloty" },
    { value: "PTE", text: "Portugal Escudo" },
    { value: "ROL", text: "Romania Leu" },
    { value: "RUR", text: "Russia Rubles" },
    { value: "SAR", text: "Saudi Arabia Riyal" },
    { value: "SGD", text: "Singapore Dollars" },
    { value: "SKK", text: "Slovakia Koruna" },
    { value: "SDD", text: "Sudan Dinar" },
    { value: "SEK", text: "Sweden Krona" },
    { value: "TWD", text: "Taiwan Dollars" },
    { value: "THB", text: "Thailand Baht" },
    { value: "TTD", text: "Trinidad and Tobago Dollars" },
    { value: "TRL", text: "Turkey Lira" },
    { value: "USD", text: "United States Dollars" },
    { value: "VEB", text: "Venezuela Bolivar" },
    { value: "ZMK", text: "Zambia Kwacha" },
    { value: "XAG", text: "Silver Ounces" },
    { value: "XAU", text: "Gold Ounces" },
    { value: "XCD", text: "Eastern Caribbean Dollars" },
    { value: "XDR", text: "Special Drawing Right(IMF)" },
    { value: "XPD", text: "Palladium Ounces" },
    { value: "XPT", text: "Platinum Ounces" },
    { value: "ZAR", text: "South Africa Rand" },
    { value: 'RSD', text:'Serbian dinar'}
  ]


  latLngNull() {
    sessionStorage.setItem("CountryCode",'MA');
    $.get("https://ipapi.co/json/").done(response=>{
      this.placelatlng = {
        'lat': response.latitude,
        'lng': response.longitude
      }
      this.ipinfoData  = response;
      sessionStorage.setItem('IpInfo',JSON.stringify(response) );
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this._conf.setItem('Currentlatlng', JSON.stringify(this.placelatlng));
        this.allListPosts();
      this.recentCity = response.city + ", " + response.country;
      this._conf.setItem('recentCity', this.recentCity);
      this._conf.setItem('currentCity', this.recentCity);
      $("#cityAreai").val(this.recentCity);
      $(".location").text(this.recentCity);
  }).fail(error=>{
    this.placelatlng = {
      'lat': "33.5731",
       'lng': "-7.5898"
     }
     this._conf.setItem('latlng',JSON.stringify(this.placelatlng));
     this._conf.setItem('Currentlatlng', JSON.stringify(this.placelatlng));
     $("#cityAreai").val("2, Casablanca 20250, Morocco");
     $(".location").text("2, Casablanca 20250, Morocco");
     $(".location").val("2, Casablanca 20250, Morocco");
     this._conf.setItem('recentCity', "2, Casablanca 20250, Morocco");
     this._conf.setItem('currentCity', "2, Casablanca 20250, Morocco");
     this.recentCity = "2, Casablanca 20250, Morocco";
    //console.log("error ipapi");
    this.allListPosts();
  });


  }

  seo() {
    let list = {
      type: 1
    }
    this._service.seoList(list)
      .subscribe((res) => {
        if (res.code == 200) {
          this.data = res.data;
          if (this.data) {
            this._meta.addTag({ name: 'title', content: this.data.title })
            this._meta.addTag({ name: 'description', content: this.data.description })
            this._meta.addTag({ name: 'keywords', content: this.data.keyword.toString() })
          }
        }
      });
  }

  emptyFigure() {
    for (var i = 0; i < 20; i++) {
      this.allListEmpty.push(i);
    }
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        ////console.log("HomeLatLng", this.placelatlng);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this._conf.setItem('Currentlatlng', JSON.stringify(this.placelatlng));
        let latlng = this._conf.getItem('latlng');
        if (latlng) {
          this.allListPosts();
        }
        var latilng = new google.maps.LatLng(lat, lng);
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latilng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (var i = 0; i < results[1].address_components.length; i += 1) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    var city = addressObj.long_name;    
                  }
                  if (addressObj.types[j] === 'country') {
                    var country = addressObj.short_name;   
                  }
                }
              }
              $("#cityAreai").val(city + ", " + country);
              $(".location").text(city + ", " + country);
              this._conf.setItem('recentCity', city + ", " + country);
              this._conf.setItem('currentCity', city + ", " + country);
            }
          }
        });
      }, this.geoError);
    } else {
    }
  }

  geoError() {
  }

  itemDetails(postId, name) {
    let replace = name.split(' ').join('-');
    this._router.navigate([this.langaugeCode,replace, postId]);
  }

  public bgcolor = [
    { 'bgcolor': 'rgb(134, 176, 222)' },
    { 'bgcolor': 'rgb(255, 63, 85)' },
    { 'bgcolor': 'rgb(115, 189, 197)' },
    { 'bgcolor': 'rgb(232, 111, 91)' },
    { 'bgcolor': 'rgb(166, 196, 136)' },
    { 'bgcolor': 'rgb(245, 205, 119)' },
    { 'bgcolor': 'rgb(190, 168, 210)' },
    { 'bgcolor': 'rgb(252, 145, 157)' },
    { 'bgcolor': 'rgb(83, 143, 209)' },
    { 'bgcolor': 'rgb(209, 169, 96)' },
    { 'bgcolor': 'rgb(227, 74, 107)' }
  ]

  listGetCategories() {
    this.catSelectList = false;
    this.selectCatList = [];
    this._service.getCategoriesList(this.langaugeCode)
      .subscribe((res) => {
        if (res.code == 200) {
          this.catList = res.data;
          let index = this.catList.findIndex((item)=> item.name.toLowerCase() == this.SelectedCat);  
          if(index > -1){
            this.searchCategoryName(this.SelectedCat,index,0);
          }
          Object.keys(this.activeQueryParams).length > 0 ? (this.updatePresentUrl(0,''),this.closeDiscoverPage()) : '';
        }
        // setTimeout(()=>{
        //   if(this.activeQueryParams){
        //     this.updatePresentUrl(0,'');
        //     this.closeDiscoverPage();
        //   }
        // },800);
      });
  }

  loadCat() {
    this.loadCateg = !this.loadCateg;
  }

  loadMore: boolean = false;
  getBannerList:any;
  allListPosts() {
    this.bannerShow = false;
    this.offset >= 40 ? this.loadMore =  true : this.loadMore = false;
    let list = {
      latitude: "",
      longitude: "",
      offset: this.offset,
      limit: this.limit
    }
    this.placelatlng = this._conf.getItem('latlng');
    if (this.placelatlng) {
      this.placelatlng = JSON.parse(this.placelatlng);
      list.latitude = this.placelatlng.lat;
      list.longitude = this.placelatlng.lng;
    }

    if (this.offset == 0) {
      this.allListData = false;
      setTimeout(() => {
        this.allListData = true;
      }, 3000);
    }
    this._service.homeAllList(list)
      .subscribe((res) => {
        this.allListData = true;
        let allList = res.data;
        if (res['code'] == 200) {
          this.offset > 40 ?    $("html, body").animate({scrollTop: this.currentScrollPos},500) :'';
            // $("html, body").animate({ scrollTop: 0 }, 500);
          res.banner && res.banner.length <= 3 ? this.getBannerList = res.banner : res.banner && res.banner.length > 3 ?
          this.getBannerList = res.banner.slice(0,3) : this.getBannerList = [{url:'',imageUrl:'assets/images/homepage-cover1.jpg',description: "Bondvenda Post, Chat, Buy and Sell.", }];
          allList =  this.sortPostsBasedOnDates(allList);
         (this.offset == 0) ?  this.allList = [] :'';
        allList.map((item, index)=>{
          this.allList.push(item);
        });
          if (this.allList && this.allList.length > 0) {
            this.listsLength = false;
          } else {
            this.listsLength = true;
            this.allListData = true;
          }
          this.listLength = this.allList.length;
          this.filterScroll = 0;
        } else {
          this.allList && this.allList.length == 0 ? 
          (this.listsLength = true,
          this.allListData = true):'';
          // this.showCasablancePosts();
        }
      });
  }
  getCasablancaPlacePosts(){
    let CasablancaLatLng = {
      'lat':'33.5731',
      'lng':'-7.5898'
    }
    this._conf.setItem('latlng',JSON.stringify(CasablancaLatLng));
    this.recentCity = "2, Casablanca 20250, Morocco";
    $(".location").text('');
    $("#cityAreai").val(this.recentCity);
    $("#lati").val(CasablancaLatLng.lat);
    $("#lngi").val(CasablancaLatLng.lat);
    $(".location").text(this.recentCity);
    this.categoryName ? this.searchCategory() : this.allListPosts();

  }
  onScroll() {
    console.log("first scroll",this.listLength)
    if (this.listLength == 40 && this.filterScroll == 0) {
      console.log("first scroll")
      this.offset += 40;
      this.allListPosts();
    } else if (this.listLength == 40 && this.filterScroll == 1) {
      console.log("second scroll")
      this.offset += 40;
      this.searchItem = 1;
      this.searchCategory();
    } 
  }
  // loadMore item after scroll down
  loadMoreItems(){
    if ($('.filterRecent').scrollTop() + $('.filterRecent').innerHeight() >= $('.filterRecent')[0].scrollHeight && this.listLength == 40 && this.filterScroll == 0) {
      this.offset += 40;
      this.allListPosts();
    }
    else if ($('.filterRecent').scrollTop() + $('.filterRecent').innerHeight() >= $('.filterRecent')[0].scrollHeight && this.listLength == 40 && this.filterScroll == 1) {
      this.offset += 40;
      this.searchCategory();
    }
  }

  emailValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      if (regexEmail.test(value)) {
        this.email_Error = false;
        this.registerSave = true;
      } else {
        this.email_Error = true;
        this.registerSave = false;
      }
    } else {
      this.email_Error = false;
    }
  }

  mobileValidation(value) {
    this.PEerror = false;
    if (value.length > 5) {
      var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
      if (value.match(regexPhone)) {
        this.phone_Error = false;
        this.registerSave = true;
      } else {
        var val = value.replace(/([^+0-9]+)/gi, '')
        this.sellPhone = val;
        this.phone_Error = true;
        this.registerSave = false;
      }
      if (value.length == 10) {
        this.phone_Error = true;
        this.registerSave = false;
      }
    } else {
      this.phone_Error = false;
    }
  }

  sellHowSend() {
    if (this.sellEmail && this.sellPhone) {
      this.howPopupSuccess = false;
      this.phone_Error = false;
      this.email_Error = false;
      this.langaugeCode === "en" ? this.PEerror = "Please try anyone" : this.langaugeCode === "fr" ?
      this.PEerror = "S'il vous plaît essayer quelqu'un" : this.PEerror = "يرجى محاولة أي شخص";
      setTimeout(() => {
        this.PEerror = false;
      }, 3000);
    } else if (this.sellEmail || this.sellPhone) {
      if (this.sellEmail) {
        this.sellSend(1);
      } else {
        this.sellSend(2);
      }
      this.PEerror = false;
    }
  }

  emptySell() {
    this.sellPhone = "";
    this.sellEmail = "";
    this.email_Error = false;
    this.phone_Error = false;
  }

  sellSend(val) {
    this.email_Error = false;
    this.phone_Error = false;
    if (val == 1) {
      this.listSell = {
        type: "1",
        emailId: this.sellEmail
      }
    } else {
      this.listSell = {
        type: "2",
        phoneNumber: this.sellPhone
      }
    }
    this._service.sellList(this.listSell)
      .subscribe((res) => {
        if (res.code == 200) {
          this.howPopupSuccess = true;
          this.emptySell();
        } else {
          this.PEerror = res.message;
        }
      });
  }
  milesDistance(val) {
    var input = $(".slider_range").val();
    var value = (input - $(".slider_range").attr('min')) / ($(".slider_range").attr('max') - $(".slider_range").attr('min'));
    $(".slider_range").css('background-image',
      '-webkit-gradient(linear, left top, right top, '
      + 'color-stop(' + value + ', #008B9A), '
      + 'color-stop(' + value + ', #C5C5C5)'
      + ')'
    );
    this.offset = 0;
    if (val == 50) {
      val = 30;
    }
    this.distance = val;
    if (val != 0) {
    } else {
      this.allListPosts();
    }
  }

  signUpContentToggle() {
    this.signUpContent = !this.signUpContent;
  }
  postedWithinList(val, index) {
    this.postedWithin = val;
    if (val == 0) {
      $("html, body").animate({ scrollTop: 0 }, 500);
      if (index == 0) {
        this.offset = 0;
      }
      else if(index == 11){
        this.offset = 0;
        if(this.prevSelectedCat && this.prevSelectedCat.length > 1){
          let index = this.prevSelectedCat.findIndex((item => item.index == this.previousCategoryIndex));
          if(index > -1 && this.prevSelectedCat && this.prevSelectedCat.length > 0){
            this.prevSelectedCat.splice(index,1);
              this.prevSelectedCat.splice(index,1);
              this.searchCategoryName(this.prevSelectedCat[this.prevSelectedCat.length-1].value,this.prevSelectedCat[this.prevSelectedCat.length-1].index,0);
          }
          else{
              this.allListPosts();
              this.listGetCategories();
          }
          this.prevSelectedCat =[];
          this.previousCategoryIndex  = "-1";
        }
        else{
          if(this.prevSelectedCat && this.prevSelectedCat.length > 0){
          this.searchCategoryName(this.prevSelectedCat[this.prevSelectedCat.length-1].value,this.prevSelectedCat[this.prevSelectedCat.length-1].index,0);
        }
       }
      this.categoryVal = [];
      this.selectCatList = [];
      this.catSelectList = false;
      this.allListPosts();
      this.catList.forEach(x => {
        x.activeimg = false;
      });
       $("#sideDetailsId").removeClass('active');
       $("body").removeClass("hideHidden");
      }
      // this.allListPosts();
    } else if (val == 10) {
      $('.radioClass').removeAttr('checked');
      $('.inputCheckBox').removeAttr('checked');
      $("#myRange").val(0);
      $(".slider_range").css('background' ,'#d3d3d3');
      $(".froms").val('');
      $(".tos").val('');
      $('#fromToPricingInput').val('');
      $('.fromToPricingInput').val('');
      $('.inputBox').val('');
      $('.currency').text('Moroccan dirham');
      $(".location").text(this._conf.getItem('currentCity'));
      this._conf.setItem('recentCity', this._conf.getItem('currentCity'));
      this.resetToggle = val;
      this.currency = this.DefualtCurrency;
      this.categoryVal = [];
      this.catSubFilter = [];
      this.currency = '';
      this.selectCatList =[];
      this.offset = 0;
      this.subCat =[];
      this.postedWithin = val;
      this.catSelectList = false;
      this.catList.forEach(x => {
        x.activeimg = false;
      });
      this.currency = this.DefualtCurrency;
      this._conf.setItem('latlng', (this._conf.getItem('Currentlatlng')));
      this.placelatlng = JSON.parse(this._conf.getItem('Currentlatlng'));
      $("#cityAreai").val(this._conf.getItem('currentCity'));
      this._conf.setItem('recentCity', this._conf.getItem('currentCity'));
      $("#lati").val(this.placelatlng.lat),
      $("#lngi").val(this.placelatlng.lng)
    } else {
      this.offset = 0;
      this.postedWithin = val;
    }
    if(this.subCat){
      this.subCat.forEach(x => {
        x.activeimg = false;
      })
    }    
  }



  sortPrice(val) {
    this.offset = 0;
    this.priceOrder = val;
  }

  numberDitchit(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".from1").val(val);
  }

  numberDitchit1(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;
    var val = value.replace(/([^+0-9]+)/gi, '');
    $(".to1").val(val);
  }

  minimumPrice(val) {
    this.offset = 0;
    this.minPrice = val;
  }

  maximumPrice(val) {
    this.offset = 0;
    (val === 0) ? this.maxPrice = '100000' : this.maxPrice = val;
  }

  selectCurrency(val) {
    this.currency = val;
  }
  categoryVal = [];
  catArray = [];
  selectCatList = [];
  searchCategoryName(val, i, filter) {
    $("#nameCategory").val("");
    this.selectCatList = [];
    this.catList.forEach(x => {
      x.activeimg = false
    })
    this.catList[i].activeimg = true;
    this.selectCatList.push(this.catList[i]);
    this.catSelectList = true;
    this.subCat = "";
    this.catSubFilter = "";
    filter == 0  ? this.subCategoryName = "" :'';
    this.categoryName = this.catList[i].name;
    this.selectedCategoryNodeId = this.catList[i].categoryNodeId;
    this.filterPrice = false;
    this.offset = 0;    
    if (filter === 0) {
          this.searchItem = 1;
          this._router.navigate([`${this.langaugeCode}/category/${this.categoryName}`])
    }
    else{
    this._service.getSubCategoriesList(this.catList[i].name,this.langaugeCode,this.selectedCategoryNodeId)
      .subscribe((res) => {
        this.subCat = res.data;
        if(this.subCategoryName){
          if(this.subCat){
            let index = this.subCat.findIndex((item)=> item.subCategoryName == this.subCategoryName);
            index > -1 ? this.subCategory(index,1) : '';
            this.searchCategory();
          }
        }
        else{
          filter == 0 ? this.updatePresentUrl(1,'subCat') :'';
        }
      });
      filter == 0 ? this.searchCategory() :'';
      // !this.isSmallScreen &&  filter == 0 ? this.searchCategory() :  !this.subCategoryName && filter == 1 ? this.searchCategory() : '';
      (this.categoryName == "") ? this.postedWithinList(0, 1) :'';
      filter == 1  ? this.navigateToFoo('cat',this.categoryName,9) :'';
    }
  }
  // searchCategoryName(val, i, filter) {
  //   $("#nameCategory").val("");
  //   this.selectCatList = [];
  //   this.catList.forEach(x => {
  //     x.activeimg = false
  //   })
  //   this.catList[i].activeimg = true;
  //   this.selectCatList.push(this.catList[i]);
  //   this.catSelectList = true;
  //   this.subCat = "";
  //   this.catSubFilter = "";
  //   filter !=1 ?  this.subCategoryName:'';
  //   this.categoryName = this.catList[i].name;
  //   this.selectedCategoryNodeId = this.catList[i].categoryNodeId;
  //   this.filterPrice = false;
  //   this.offset = 0;    
  //   if (filter === 0) {
  //     this.searchItem = 1;
  //     this._router.navigate([`${this.langaugeCode}/category/${this.categoryName}`])
  //   }
  //   else{
  //     this._service.getSubCategoriesList(this.catList[i].name,this.langaugeCode,this.selectedCategoryNodeId)
  //     .subscribe((res) => {
  //       this.subCat = res.data;
  //     });
  //   (this.categoryName == "") ? this.postedWithinList(0, 1) : "";
  //   filter == 1 && !this.subCategoryName ? this.searchCategory():'';
  //   this.navigateToFoo('cat',this.categoryName,9);
  // }
  // }
  // to close discover popup
  closeDiscoverPage(){
    $("html, body").animate({ scrollTop: 0 }, 500);
    $("#sideDetailsId").hasClass('active') ? $("#sideDetailsId").toggleClass('active') : '';
    $("body").hasClass('hideHidden') ? $("body").toggleClass("hideHidden") : '';
  }
  
  cancelCat(i) {
    this.catList.forEach(x => {
      x.activeimg = false
    })
    this.subCat = "";
    this.categoryName = "";
    this.listGetCategories();
    if (!$("#sideDetailsId").hasClass("active")) {
      this.postedWithinList(0, 0);
    }
    this.updatePresentUrl(2,'cat');
  }
  
  subCategoryName: any;
  subCategoryNodeId: any;
  subCategory(i,val) {
    this.subCategoryName = this.subCat[i].subCategoryName;
    this.SelectSubCat = this.subCategoryName;
    this.subCategoryNodeId = this.subCat[i].subCategoryNodeId;
    this.subCat.forEach(x => {
      x.activeimg = false
    })
    this.subCat[i].activeimg = true;
    this.selectCatList.splice(1, 1);
    this.selectCatList.push(this.subCat[i]);
    var value = [];
    this.subCat[i].filter.forEach(x => {
      if (x.type == 2 || x.type == 4 || x.type == 6) {
        x.filterData = x.values.split(",");
        // x.value = split;
      }
      x.data = [];
      x.checked = false;
      value.push(x);
    });
    ////console.log("the cat filter is:", value);
    this.catSubFilter = value;
    val == 1 ? this.searchCategory():'';
    this.navigateToFoo('subCat',this.subCategoryName, 1 );
  }

  cancelsubCat() {
    this.SelectSubCat = '';
    this.subCat.forEach(x => {
      x.activeimg = false
    })
    this.catSubFilter = "";
    this.subCategoryName = '';
    this.selectCatList.splice(1, 1);
    this.updatePresentUrl(1,'subCat');
    // this.updatePresentUrl(1,'cat');
  }

  cancelCats(i) {
    $("#nameCategory").val("");
    let len = this.selectCatList.length;
    if (i == 1) {
      this.cancelsubCat();
      let index = this.catList.findIndex((item)=> item.categoryNodeId == this.selectCatList[0].categoryNodeId );
      this.searchCategoryName(this.selectCatList[0].name, index,0);

    } else {
      this.postedWithinList(0, 0);
      this.SelectedCat ='';
      this.allListPosts();
      this.listGetCategories();
      this.categoryVal = [];
      this.catList.forEach(x => {
        x.activeimg = false;
      });
      this.subCat.forEach(x => {
        x.activeimg = false;
      });
      this.subCat = [];
      this.updatePresentUrl(2,'subCat');
    }
  }

  // latest code for Advaced filter
  checkedFilter = [];
  documentTextList(event, i, range) {
    if (event.target.value.length > 0) {
      if (this.catSubFilter[i].type == 2 || this.catSubFilter[i].type == 4 || this.catSubFilter[i].type == 6) {
        var index = -1;
        if(this.catSubFilter[i].data && this.catSubFilter[i].data.length >= 1 && this.catSubFilter[i].type == 4){
          index = this.catSubFilter[i].data.findIndex(x => x.value == this.catSubFilter[i].filterData[range]);
        }
        if (index > -1) {
           this.catSubFilter[i].data.splice(index, 1);
         
        } else {
          let list = {
            type: "equlTo",
            fieldName: this.catSubFilter[i].fieldName,
          }
          if( this.catSubFilter[i].type == 4   && this.catSubFilter[i].data.length >=  1){
            this.catSubFilter[i].data[0].value = this.catSubFilter[i].filterData[range];
          }
          else if(this.catSubFilter[i].type == 4  && this.catSubFilter[i].data.length == 0){
            list['value'] = this.catSubFilter[i].filterData[range];
            this.catSubFilter[i].data.push(list);
          }
          else if( this.catSubFilter[i].type == 6   && this.catSubFilter[i].data.length >=  1){
            this.catSubFilter[i].data[0].value = range;
          }
          else if(this.catSubFilter[i].type == 6  && this.catSubFilter[i].data.length == 0){
            list['value'] = range;
            this.catSubFilter[i].data.push(list);
          }
          else if(this.catSubFilter[i].type == 2){
            const checkbox = event.target as HTMLInputElement;
            if(checkbox.checked){
              this.checkedFilter.push({
                checked: checkbox.checked,
                name: event.target.name
              })
            }
            else{
              let index = this.checkedFilter.findIndex((x)=>x.name === event.target.name);
              if(index > -1){
                this.checkedFilter.splice(index,1);
              } 
            }
          }
          let finalValue = '';
          if( this.checkedFilter.length > 0  && this.catSubFilter[i].type == 2){
            this.checkedFilter.forEach((x,index)=> {
              (index != this.checkedFilter.length - 1)? finalValue += (x.name)+',' :
              finalValue += (x.name);
            });
            list['value'] = finalValue; 
            if(this.catSubFilter[i].data.length == 0){
              this.catSubFilter[i].data.push(list);
            }
            else{
              let index = this.catSubFilter[i].data.findIndex(x => x.fieldName == this.catSubFilter[i].fieldName);
              this.catSubFilter[i].data[index].value = finalValue;
            }
          }
          else if(this.checkedFilter.length == 0 && this.catSubFilter[i].type == 2){
            this.catSubFilter[i].data = [];
          }
        }
        if (this.catSubFilter[i].data && this.catSubFilter[i].data.length >= 1) {
          this.catSubFilter[i].checked = true;
          this.catSubFilter[i].typed = 4;
        } else {
          this.catSubFilter[i].checked = false;
        }
      } 
      else {
        this.catSubFilter[i].data = event.target.value.replace(',','.');
        this.catSubFilter[i].checked = true;
        ( this.catSubFilter[i].type == 3 || this.catSubFilter[i].type == 5 ) ?
        this.catSubFilter[i].types = "range" : this.catSubFilter[i].types = "equlTo";
      }
    } 
    else {
          this.catSubFilter[i].checked = false;
    }
  }
  distanceList(val) {
    this.distances = Number(val);
  }
  filterIsApplied : boolean = false;
  filterToggle() {
    // $("html, body").animate({ scrollTop: 0 }, 500);
    // $("#sideDetailsId").toggleClass('active');
    // $("body").toggleClass("hideHidden");
    if(this.resetToggle == 10 && this.selectCatList.length <= 0 && this.selectCatList){ 
      this.allListPosts();
      this.listGetCategories();
    }
    else{
      this.searchItem = 1;
      this.searchCategory();
    }
    this.pushUrlState();
  }

  searchCategory() {
    this.errMsg = false;
    if(this.currency.length <= 0){
      this.currency = '';
    }
    setTimeout(() => {
      this.loaderButton = false;
      this.errMsg = false;
    }, 3000);
    var filData = [];
    var filDatas = [];
    if (this.catSubFilter) {
      this.catSubFilter.forEach(x => {
        if (x.checked == true && !x.typed) {     
          let list = {
            type: x.types,
            fieldName: x.fieldName,
            value: x.data || '',
            from: "",
            to: ""
          }
          if (x.types == "range") {
            list.from = x.from;
            list.to = x.to;
          }
          filData.push(list);
        } else {
          if (x.checked == true && x.typed == 4) {
            x.data.forEach(y => {
              let lisT = {
                type: y.type,
                fieldName: y.fieldName,
                value: y.value,
              }
              filData.push(lisT);
            });
          }
        }
      })
    }
    this.bannerShow = true;
    let ltlg = {
      lat: $("#lati").val(),
      lng: $("#lngi").val()
    }
    if ($("#lati").val()) {
      this._conf.setItem('latlng', JSON.stringify(ltlg));
    }
    let ciSt = $("#cityAreai").val();
    if (ciSt) {
      this.recentCity = ciSt;
    }

    let lists = {
      token: this._conf.getItem('authToken'),
      offset: this.offset,
      limit: this.limit,
      location: "",
      latitude: "",
      longitude: "",
      currency: this.currency,
      postedWithin: this.postedWithin ||0,
      sortBy: this.priceOrder,
      distance: this.distance,
      langaugeCode: this.langaugeCode,
      categoryId : this.selectedCategoryNodeId ,
      price:JSON.stringify({
        currency: this.currency,
        from : parseInt(this.minPrice),
        to: parseInt(this.maxPrice),

      }),
      category: this.categoryName,
      subCategory: this.subCategoryName || '',
      subCategoryId: this.subCategoryNodeId || '',
      filter: JSON.stringify(filData),
    }
    let latlng = this._conf.getItem('latlng');
    if (latlng) {
      let latlng = JSON.parse(this._conf.getItem('latlng'));
      lists['latitude'] = latlng.lat;
      lists['longitude'] = latlng.lng;
    }
    if (this.recentCity) {
      lists.location = this.recentCity;
    } else {
      lists.location = this._conf.getItem('recentCity')
    }
    this.loaderButton = true;
    this._service.searchCategoriesList(lists, this.searchItem,'')
      .subscribe((res) => {
        this.loaderButton = false;
        let allList = res.data;
        if (res.code == 200) {
          //card#251, inifinate scroll issues , fixed by:sowmyaDV
          // allList = this.sortPostsBasedOnDates(allList);
          this.offset == 0 ? this.allList = [] :'';
          allList.map((item)=>{
            this.allList.push(item);
          });
          // console.log("search data", res.data, this.allList)
          this.allList && this.allList.length > 0 ? this.listsLength = false : this.listsLength = true;
          this.listLength = this.allList.length;
          this.filterScroll = 1;
        } else {
          this.errMsg = res.message;
          this.listsLength = true;
          if(!this.currency && !this.postedWithin &&  !this.priceOrder && !this.priceOrder &&  !this.distance && !this.selectedCategoryNodeId
            && !this.minPrice &&  !this.maxPrice && !this.categoryName && !this.subCategoryName && !filData){
            this.allListPosts();
          }
          
          this.filterIsApplied = false;
         setTimeout(()=>{
          this.closeDiscoverPage();
         },1000)
        }
      },error =>{
      });
      this.searchItem = 0;
    }

  setCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          'lat': lat,
          'lng': lng
        }
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        this.checkLocation();
      });
    }
  }


  checkLocation() {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      $("#locationMap").modal("show");
      this.continueMap();
    }
  }
  currentSelectedLoc:any;
  changeLocation(event) {
    var input = document.getElementById('changeLocationId');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
          }
        }
      }
      if (City) {
        $("#cityAreai").val(City + ", " + state);
      } else if (state) {
        $("#cityAreai").val(state);
      } else {
        $("#cityAreai").val(place.formatted_address);
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      $("#lati").val(lat);
      $("#lngi").val(lng);
      this.placelatlng = {
        'lat': lat,
        'lng': lng
      }
      this.latlng = {
        lat: lat,
        lng: lng
      };
    });
    event.type === "change" ? this.locationChange() :'';
  }

  locationChange1() {
    setTimeout(()=>{
      this.currentSelectedLoc ={
        lat: $("#lati").val(),
        lng : $("#lngi").val()
      }
      this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this._conf.setItem('recentCity',$("#cityAreai").val());
    },200)
  }


  locationChange() {
    setTimeout(() => {
      let lat = $("#lati").val();
      let lng = $("#lngi").val();
      this.latlng = {
        lat: lat,
        lng: lng
      };
      this.continueMap();
    }, 500);
  }


  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById('mapData'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: false,
      mapTypeControl: false
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map,
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // //console.log("results[0].formatted_address",results[0].formatted_address)
          $(".addressArea").val(results[0].formatted_address);
          $("#cityAreai").val(results[0].formatted_address);
          $(".location").text(results[0].formatted_address);
        }
      }
    });


    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $("#lati").val(marker.getPosition().lat());
            $("#lngi").val(marker.getPosition().lng());
            $(".location").text(results[0].formatted_address);
            $("#cityAreai").val(results[0].formatted_address);
            $(".addressArea").val(results[0].formatted_address);
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }
// show default  Casablanca, Morocco if current location items are not there

showCasablancePosts(){
  this.bannerShow = false;
  let list = {
    latitude: "33.5731",
    longitude: "-7.5898",
    offset: 0,
    limit: this.limit
  }
  this.placelatlng = this._conf.getItem('latlng');
  if (this.offset == 0) {
    this.allListData = false;
    setTimeout(() => {
      this.allListData = true;
    }, 3000);
  }
  this._service.homeAllList(list)
    .subscribe((res) => {
      this.allListData = true;
      let allList = res.data;
      if (res.code == 200) {
        if (this.offset == 0) {
          this.allList = [];
        }
        let object = {};
        let result = [];

      this.allList.forEach(function (item,index) {
        let Item =  this.allList[index].postId
        if(!object[Item])
            object[Item] = 0;
          object[Item] += 1;
      })

      for (let prop in object) {
         if(object[prop] <= 2) {
             result.push(prop);
         }
      }
      this.allList = result;

        for (var i = 0; i < allList.length; i++) {
          this.allList.push(res.data[i]);
        }
        if (this.allList && this.allList.length > 0) {
          this.listsLength = false;
        } else {
          this.listsLength = true;
          this.allListData = true;
          // this.getCasablancaPlacePosts();
        }
        this.listLength = this.allList.length;
        this.filterScroll = 0;
      } else {
        this.listsLength = true;
        this.allListData = true;
        // this.allList = [];
        // this.getCasablancaPlacePosts();
      }
    });
}
// format price based on dirham currency (DH)
getDirhamCurrencyPrice(list){
  let price = list.price.toString();
  price = parseInt(list.price).toFixed(2);
  price = this._conf.getDirhamCurrencyPrice(list);
  return price;

}
 //card#191 ,bug : sort the posts based on dates, fixedBy:sowmya
 sortPostsBasedOnDates(postList){

  let sortedList = postList.sort(function(a, b){
    return moment.utc(a.postedOn * 1000 , '"h:mm:ss').diff(moment.utc(b.postedOn * 1000 , '"h:mm:ss'))
  });
  return sortedList;
}


// for performance purpose us trackBy for *ngFor directive
trackByIndex(index, item) {
  return index;
}

//check if query are there 
checkIfQueryParams(queryParams){
  console.log("qiery ", queryParams,queryParams.hasOwnProperty('cat'));
   // distance filter
  // if(queryParams['dist'] != undefined && queryParams.hasOwnProperty('dist')){
  //    this.distance = queryParams['dist'];
  //    this.milesDistance(this.distance);
  //   //  background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(0.8, rgb(92, 52, 163)), color-stop(0.8, rgb(197, 197, 197)));
  // }
  //  // advanced Filter
  // (queryParams['filter'] != undefined && queryParams.hasOwnProperty('filter'))?this.catSubFilter = queryParams['filter'] :'';
  // // sort based onPrice
  // (queryParams['sortPrice'] != undefined && queryParams.hasOwnProperty('sortPrice'))?this.priceOrder = queryParams['sortPrice'] :'';

  //  // filter based PostWithin
  // (queryParams['postWithIn'] != undefined && queryParams.hasOwnProperty('postWithIn')) ? this.postedWithin = queryParams['postWithIn'] :'';
  // // filter based minPrice
  // (queryParams['minPrice'] != undefined && queryParams.hasOwnProperty('minPrice')) ? this.minPrice = queryParams['minPrice'] : '';
  // // filter based maxPrice
  // (queryParams['maxPrice'] != undefined && queryParams.hasOwnProperty('maxPrice'))? this.maxPrice = queryParams['maxPrice'] : '';
  // // filter based on location 
  // (queryParams['location'] != undefined && queryParams.hasOwnProperty('location'))?this.latlng = queryParams['location'] : '';
  // // filter based on currency 
  // (queryParams['currency'] != undefined && queryParams.hasOwnProperty('currency')) ? this.currency = queryParams['currency'] :'';
  //filters based on category
  (queryParams.hasOwnProperty('cat')) ? this.categoryName = queryParams['cat'] :'';
  //filters based on category
  (queryParams.hasOwnProperty('subCat')) ? this.subCategoryName = queryParams['subCat'] :'';
  // auto select cat and subcat 
  // setTimeout(()=>{
    if(this.categoryName){
      if(this.catList){
        let index = this.catList.findIndex((item)=> item.name === this.categoryName);
        console.log("index for hoem category", index)
        index > -1 ? this.searchCategoryName(this.categoryName,index,1) : '';
        !queryParams.hasOwnProperty('subCat') ? this.searchCategory() :'';
      }
    }
  // },300)
}
// create new params for query params of url
getQueryParams(params){
  var resultantQuery = {...params};
  return resultantQuery;
}
// get queryparams 
navigateToFoo(key, value, caseValue){
  // changes the route without moving from the current view or
  // triggering a navigation event,
  let catIsSelected : any;
  let queries : any;
  // this.categoryName ? catIsSelected = `./category/${this.categoryName}` : catIsSelected = ``;
  this.route.queryParams.subscribe(params => {
       
    Object.getOwnPropertyNames(params).length > 0 ?  this.queryParams = this.getQueryParams(params):'';
    if(this.queryParams && this.queryParams.hasOwnProperty(key)){
      this.queryParams[key] = value;
    }
    else{
      switch(caseValue){
          case 1: queries = { [key] : value}
          break;
          case 2: queries = { [key] : value}
          // queryStr.set('dist',value );
          break;
          case 3: queries = { [key] : value}
          break;
          case 4: queries = { [key] : value}
          break;
          case 5: queries = { [key] : value}
          break;
          case 6: queries = { [key] : value}
          break;
          case 7: queries = { [key] : value}
          break;
          case 8: queries = { [key] : value}
          break;
          case 9: queries = { [key] : value}
          break;
        }
        this.queryParams = Object.assign(queries,this.queryParams);
      }
  });
  // this.pushUrlState();
}
stateParam = 0 ;
// set query params for present url
pushUrlState(){
  //console.log("push state")

    $("html, body").animate({ scrollTop: 0 }, 500);
    $("#sideDetailsId").hasClass('active') ? $("#sideDetailsId").removeClass('active') : '';
    $("body").removeClass("hideHidden");
  this._router.navigate([], {
    queryParams: this.queryParams,
    queryParamsHandling: 'merge',
    relativeTo: this.route
  });
}
// if any subcategory or category de-seelcted then remove them from url 
updatePresentUrl(val,caseVal){
  let result1 =  Object.assign({}, this.activeQueryParams);
  val === 1 && caseVal == 'cat' ? result1.hasOwnProperty('cat') ? Reflect.deleteProperty(result1,'cat') : '' : '';
  val === 1 && caseVal == 'subCat' ? result1.hasOwnProperty('subCat') ? Reflect.deleteProperty(result1,'subCat') : '' : '';
  // if cancel the category
  if(val == 2){
    result1.hasOwnProperty('subCat') ? Reflect.deleteProperty(result1,'subCat') : '' ;
    result1.hasOwnProperty('cat') ? Reflect.deleteProperty(result1,'cat') : '';
  }
  console.log("result",result1)
   if(Object.keys(result1).length > 0){
    this.location.replaceState(
      this._router.createUrlTree(
        [`${this.langaugeCode}`], // Get uri
        {queryParams: result1} // Pass all parameters inside queryParamsObj
      ).toString()
    );
    this.searchItem = 1;
    //card#303, issues: Website - Sidepanel issue, fixed by:sowmyasv
    this.checkIfQueryParams(result1);
   }
   else{
    //  this.searchProduct = this.seachedProduct;
    //  this.searchItem = 0;
    this.location.replaceState(`${this.langaugeCode}`);
    this.searchCategory();
   }
}
seachedProduct:any;
getSearchProduct(val){
  this.route.queryParams.subscribe((queryParams)=>{
    this.activeQueryParams = queryParams;
  })
  
}
}
